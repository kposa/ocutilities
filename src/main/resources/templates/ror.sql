CREATE TABLE public.ror_inserts
(
    subjectid character varying(255) COLLATE pg_catalog."default",
    timepointoid character varying(255) COLLATE pg_catalog."default",
    status character varying(255) COLLATE pg_catalog."default",
    create_date character varying(255) COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE public.ror_inserts
    OWNER to kposa;

	insert into ror_inserts (subjectid, timepointoid, importxml, status, create_date) 
	values ('EP_030', 'SE_INTERREGIMEN', null, 'N', current_timestamp)

CREATE TABLE public.rorimportbatch
(
    subjectidlist text COLLATE pg_catalog."default",
    status character varying(255) COLLATE pg_catalog."default",
    create_date character varying(255) COLLATE pg_catalog."default",
    importxml text COLLATE pg_catalog."default", 
    jobId text COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE public.rorimportbatch
    OWNER to kposa;
