package com.quantumleaphealth.oc.model;

import java.io.StringWriter;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="ClinicalData")
@XmlAccessorType(XmlAccessType.FIELD)
public class ClinicalData {
	
	public ClinicalData() {
		
	}
	public ClinicalData(String studyOid) {
		this.studyOID = studyOid;
		this.metaDataVersionOID = "null";
		//this.subjectData = new SubjectData("SS_57273", "EAR0001014");
		
	}
	 @XmlAttribute(name = "StudyOID")
	 String studyOID;


	 @XmlElement(name = "SubjectData")
	 SubjectData subjectData;
	 
	 @XmlAttribute(name = "MetaDataVersionOID")
	 String metaDataVersionOID;


	public String getStudyOID() {
		return studyOID;
	}


	public void setStudyOID(String studyOID) {
		this.studyOID = studyOID;
	}


	public SubjectData getSubjectData() {
		return subjectData;
	}


	public void setSubjectData(SubjectData subjectData) {
		this.subjectData = subjectData;
	}


	@Override
	public String toString() {
		//return "ClinicalData [studyOID=" + studyOID + ", subjectData=" + subjectData + ", metaDataVersionOID="
		//		+ metaDataVersionOID + "]";
		
		String xmlString =  null;
		try {
		
			JAXBContext jaxbContext = org.eclipse.persistence.jaxb.JAXBContextFactory
	                 .createContext(new Class[]{Odm.class}, null);
	         Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	
	         jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	         StringWriter sw = new StringWriter();
	         jaxbMarshaller.marshal(this,sw);
	         xmlString = sw.toString();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return xmlString;
	}


	public String getMetaDataVersionOID() {
		return metaDataVersionOID;
	}


	public void setMetaDataVersionOID(String metaDataVersionOID) {
		this.metaDataVersionOID = metaDataVersionOID;
	}
	
	public void addSubjectData(SubjectData sd) {
		this.subjectData  = sd;
	}


}
