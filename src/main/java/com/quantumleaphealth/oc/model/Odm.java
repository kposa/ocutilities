package com.quantumleaphealth.oc.model;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import com.quantumleaphealth.oc.utils.OCUtil;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.UnmarshallerHandler;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="ODM")
@XmlAccessorType(XmlAccessType.FIELD)
public class Odm {
	
	public static DateFormat creationDf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+00:00");

	public Odm () {
		
	}
	
	public Odm (String fileOid ) {
		this.fileOID  = fileOid;
		this.description = "Study Metadata";
        String creationDate =   creationDf.format(new Date());
        this.creationDateTime = creationDate;
        this.fileType = "Snapshot";
        this.odmVersion = "1.3";
        
        this.clinicalData = new ClinicalData("S_ISPY(TEST)");

		
	}
	@XmlAttribute(name="xsi:schemaLocation")
	protected final String xsi_schemaLocation="http://www.cdisc.org/ns/odm/v1.3 OpenClinica-ODM1-3-0-OC3-0.xsd"; 

	 @XmlAttribute(name = "FileOID")
	 String fileOID;
	 
	 @XmlAttribute(name = "CreationDateTime")
	 String creationDateTime;
	 
	 
	@XmlAttribute(name = "Description")
	 String description;


	 @XmlAttribute(name = "FileType")
	 String fileType;

	 @XmlAttribute(name ="ODMVersion")
	 String odmVersion;


	 @XmlElement(name = "ClinicalData", required = true)
	 ClinicalData clinicalData;


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getFileType() {
		return fileType;
	}


	public void setFileType(String fileType) {
		this.fileType = fileType;
	}


	public String getOdmVersion() {
		return odmVersion;
	}


	public void setOdmVersion(String odmVersion) {
		this.odmVersion = odmVersion;
	}


	




	public String getFileOID() {
		return fileOID;
	}


	public void setFileOID(String fileOID) {
		this.fileOID = fileOID;
	}


	public String getCreationDateTime() {
		return creationDateTime;
	}


	public void setCreationDateTime(String creationDateTime) {
		this.creationDateTime = creationDateTime;
	}


	public ClinicalData getClinicalData() {
		return clinicalData;
	}


	public void setClinicalData(ClinicalData clinicalData) {
		this.clinicalData = clinicalData;
	}


	@Override
	public String toString() {
		//return "Odm [fileOID=" + fileOID + ", creationDateTime=" + creationDateTime + ", description=" + description
				//+ ", fileType=" + fileType + ", odmVersion=" + odmVersion + ", clinicalData=" + clinicalData + "]";
		String xmlString =  null;
		try {
		
			JAXBContext jaxbContext = org.eclipse.persistence.jaxb.JAXBContextFactory
	                 .createContext(new Class[]{Odm.class}, null);
	         Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	
	         jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	         StringWriter sw = new StringWriter();
	         jaxbMarshaller.marshal(this,sw);
	         xmlString = sw.toString();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return xmlString;
	
	}

	public static Odm loadFromXml(String xml) {

		Odm data = null;
		try
		{
		 	SAXParserFactory spf = SAXParserFactory.newInstance();
	        SAXParser sp = spf.newSAXParser();
	        XMLReader xmlReader = sp.getXMLReader();
	        OCXmlFilter xmlFilter = new OCXmlFilter(xmlReader);

	        JAXBContext context = JAXBContext.newInstance(Odm.class);
	        Unmarshaller um = context.createUnmarshaller();
	        UnmarshallerHandler unmarshallerHandler = um.getUnmarshallerHandler();
	        xmlFilter.setContentHandler(unmarshallerHandler);

	        InputSource inputSource = new InputSource(new StringReader(xml));
	              
	        xmlFilter.parse(inputSource);

	        data = (Odm) unmarshallerHandler.getResult();
		}
		catch (Exception e) 
		{
		    e.printStackTrace();
		}
		return data;
	}
	
	public FormData getFormData(String formOid) {
		FormData returnFormData = null;
		ClinicalData cd = this.getClinicalData();
		SubjectData sd = cd.getSubjectData();
		outerloop:
		for(StudyEventData sed: sd.getStudyEventDataList()) {
			if(sed!=null && sed.getFormList() != null) {
				for(FormData fd: sed.getFormList()) {
					if(fd!= null && fd.getFormOID()!=null && fd.getFormOID().equals(formOid)) {
						returnFormData = fd;
						break outerloop;
					}
				}
			}
		}
		return returnFormData;
	}
	
	public StudyEventData getStudyEventData(String sedOid) {
		StudyEventData returnSeData = null;
		ClinicalData cd = this.getClinicalData();
		SubjectData sd = cd.getSubjectData();
		for(StudyEventData sed: sd.getStudyEventDataList()) {
				if(sed.getStudyEventOID().equals(sedOid)) {
					returnSeData = sed;
					break;
				}
		}
		return returnSeData;
	}

	public SubjectData getSubjectData() {
		return this.getClinicalData().getSubjectData();
	}
	
	public static Odm getOdmForSubjectKey(String subKey) {
		String subId = SubjectData.getSubjectIdForKey(subKey);
		System.out.println("key:" + subKey + ", subjectId:" + subId);
		OCUtil util = new OCUtil();
		
		String username = util.getAppProperties().getProperty("username");
		String password = util.getAppProperties().getProperty("password");
		
		String accessToken = OCUtil.getAccessToken("qlhc2.build.openclinica.io", username, password); //username/password need to be for OC data manager, vandhana may already have them in a properties file
		String url = util.getAppProperties().getProperty("odmUrlWithSubjectId");
		String urlODM = String.format(url, subId);
		
		Odm odmData = null;
		try {
			Document documentODM = OCUtil.getXML(urlODM, accessToken);		
			String xml = OCUtil.convertDocToString(documentODM);	
			odmData = Odm.loadFromXml(xml); 
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return odmData;
	}
}
