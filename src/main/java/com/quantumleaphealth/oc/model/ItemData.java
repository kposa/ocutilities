package com.quantumleaphealth.oc.model;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

//@XmlRootElement(name="ItemData")
@XmlAccessorType(XmlAccessType.FIELD)
public class ItemData {
	
	public ItemData() {
		
	}
	
	public ItemData(String itemOid, String itemName, String value) {
		this.itemOID = itemOid;
		this.itemName = itemName;
		this.value = value;
	}
	
	 @XmlAttribute(name = "ItemOID")
	 String itemOID;
	 
	 @XmlAttribute(name = "OpenClinica:ItemName")
	 String itemName;

	 @XmlAttribute(name = "Value")
	 String value;

	public String getItemOID() {
		return itemOID;
	}

	public void setItemOID(String itemOID) {
		this.itemOID = itemOID;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		//return "ItemData [itemOID=" + itemOID + ", itemName=" + itemName + ", value=" + value + "]";
	
		String xmlString =  null;
		try {
		
			JAXBContext jaxbContext = org.eclipse.persistence.jaxb.JAXBContextFactory
	                 .createContext(new Class[]{Odm.class}, null);
	         Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	
	         jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	         StringWriter sw = new StringWriter();
	         jaxbMarshaller.marshal(this,sw);
	         xmlString = sw.toString();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return xmlString;
	}

}
