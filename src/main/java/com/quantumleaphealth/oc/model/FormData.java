package com.quantumleaphealth.oc.model;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.UnmarshallerHandler;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="FormData")
@XmlAccessorType(XmlAccessType.FIELD)
public class FormData {
	
	public static DateFormat creationDf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+00:00");
	
	public FormData() {
		
	}
	
	public FormData(String formOid, 
			String formName,
			String layoutOid,
			String status,
			String workflowStatus) {
		this.formOID = formOid;
		this.formName = formName;
		this.layoutId = layoutOid;
		this.status = status;
		this.workflowStatus = workflowStatus;
		
        String creationDate =   creationDf.format(new Date());
        this.createdDate = creationDate;
        itemGroupDataList = new ArrayList<ItemGroupData>();
	}
	
	 @XmlAttribute(name = "FormOID")
	 String formOID;

	 @XmlAttribute(name = "OpenClinica:FormName")
	 String formName;

	 @XmlAttribute(name = "OpenClinica:FormLayoutOID")
	 String layoutId;
	 
	 @XmlAttribute(name = "OpenClinica:CreatedDate")
	 String createdDate;
	 
	 @XmlAttribute(name = "OpenClinica:CreatedB")
	 String createdBy;
	 
	 @XmlAttribute(name = "OpenClinica:UpdatedDate")
	 String updatedDate;
	 
	 @XmlAttribute(name = "OpenClinica:UpdatedBy")
	 String updatedBy;
	 
	 @XmlAttribute(name = "OpenClinica:Status")
	 String status;
	 
	 @XmlAttribute(name = "OpenClinica:WorkflowStatus")
	 String workflowStatus;

	 @XmlAttribute(name = "OpenClinica:SdvStatus")
	 String sdvStatus;
	 
	 @XmlAttribute(name = "OpenClinica:Required")
	 String required;
	 
	 @XmlAttribute(name = "OpenClinica:Visible")
	 String visible;

	 @XmlAttribute(name = "OpenClinica:Editable")
	 String editable;


	 
	 @XmlElement(name = "ItemGroupData")
	  List<ItemGroupData> itemGroupDataList;



	public String getFormOID() {
		return formOID;
	}



	public void setFormOID(String formOID) {
		this.formOID = formOID;
	}



	public String getFormName() {
		return formName;
	}



	public void setFormName(String formName) {
		this.formName = formName;
	}



	public String getLayoutId() {
		return layoutId;
	}



	public void setLayoutId(String layoutId) {
		this.layoutId = layoutId;
	}



	public String getCreatedDate() {
		return createdDate;
	}



	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}



	public String getCreatedBy() {
		return createdBy;
	}



	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}



	public String getUpdatedDate() {
		return updatedDate;
	}



	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}



	public String getUpdatedBy() {
		return updatedBy;
	}



	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public String getWorkflowStatus() {
		return workflowStatus;
	}



	public void setWorkflowStatus(String workflowStatus) {
		this.workflowStatus = workflowStatus;
	}



	public String getSdvStatus() {
		return sdvStatus;
	}



	public void setSdvStatus(String sdvStatus) {
		this.sdvStatus = sdvStatus;
	}



	public String getRequired() {
		return required;
	}



	public void setRequired(String required) {
		this.required = required;
	}



	public String getVisible() {
		return visible;
	}



	public void setVisible(String visible) {
		this.visible = visible;
	}



	public String getEditable() {
		return editable;
	}



	public void setEditable(String editable) {
		this.editable = editable;
	}



	public List<ItemGroupData> getItemGroupDataList() {
		return itemGroupDataList;
	}



	public void setItemGroupDataList(List<ItemGroupData> itemGroupDataList) {
		this.itemGroupDataList = itemGroupDataList;
	}



	@Override
	public String toString() {
		
		/*return "FormData [formOID=" + formOID + ", formName=" + formName + ", layoutId=" + layoutId + ", createdDate="
				+ createdDate + ", createdBy=" + createdBy + ", updatedDate=" + updatedDate + ", updatedBy=" + updatedBy
				+ ", status=" + status + ", workflowStatus=" + workflowStatus + ", sdvStatus=" + sdvStatus
				+ ", required=" + required + ", visible=" + visible + ", editable=" + editable + ", itemGroupDataList="
				+ itemGroupDataList + "]";
		*/
		
		String xmlString =  null;
		try {
		
			JAXBContext jaxbContext = org.eclipse.persistence.jaxb.JAXBContextFactory
	                 .createContext(new Class[]{Odm.class}, null);
	         Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	
	         jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	         StringWriter sw = new StringWriter();
	         jaxbMarshaller.marshal(this,sw);
	         xmlString = sw.toString();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return xmlString;
	}
	
	public void addItemGroupData(ItemGroupData igd) {
		this.itemGroupDataList.add(igd);
	}
	
	public static FormData loadFromXml(String xml) {
		FormData formData = null;
		
		try
		{
		   
			 	SAXParserFactory spf = SAXParserFactory.newInstance();
		        SAXParser sp = spf.newSAXParser();
		        XMLReader xmlReader = sp.getXMLReader();
		        OCXmlFilter xmlFilter = new OCXmlFilter(xmlReader);

		        JAXBContext context = JAXBContext.newInstance(FormData.class);
		        Unmarshaller um = context.createUnmarshaller();
		        UnmarshallerHandler unmarshallerHandler = um.getUnmarshallerHandler();
		        xmlFilter.setContentHandler(unmarshallerHandler);

		        InputSource inputSource = new InputSource(new StringReader(xml));
		        		      
		        xmlFilter.parse(inputSource);

		        formData = (FormData) unmarshallerHandler.getResult();
		}
		catch (Exception e) 
		{
		    e.printStackTrace();
		}
		return formData;
	}

	public void clearFormData() {	
		for(ItemGroupData igData: this.getItemGroupDataList()) {
			for (ItemData id: igData.getItemDataList()) 
				id.setValue("");			
		}	
}

}
