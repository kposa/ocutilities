package com.quantumleaphealth.oc.model;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="ItemGroupData")
@XmlAccessorType(XmlAccessType.FIELD)
public class ItemGroupData {
	
	public ItemGroupData() {
		
	}
	
	public ItemGroupData(String oid, String repeatKey, 
			String itemGroupName, String transactionType) {
		this.itemGroupOID = oid;
		this.itemGroupRepeatKey = repeatKey;
		this.itemGroupName = itemGroupName;
		this.transactionType = transactionType;
		
		ArrayList<ItemData> dataList = new ArrayList<ItemData>();
		this.itemDataList = dataList;
	}
	
	 @XmlAttribute(name = "ItemGroupOID")
	 String itemGroupOID;

	 @XmlAttribute(name = "ItemGroupRepeatKey")
	 String itemGroupRepeatKey;

	 
	 @XmlAttribute(name = "OpenClinica:ItemGroupName")
	 String itemGroupName;

	 @XmlAttribute(name = "TransactionType")
	 String transactionType;
	 
	 @XmlElement(name = "ItemData")
	  List<ItemData> itemDataList;

	public String getItemGroupOID() {
		return itemGroupOID;
	}

	public void setItemGroupOID(String itemGroupOID) {
		this.itemGroupOID = itemGroupOID;
	}

	public String getItemGroupRepeatKey() {
		return itemGroupRepeatKey;
	}

	public void setItemGroupRepeatKey(String itemGroupRepeatKey) {
		this.itemGroupRepeatKey = itemGroupRepeatKey;
	}

	public String getItemGroupName() {
		return itemGroupName;
	}

	public void setItemGroupName(String itemGroupName) {
		this.itemGroupName = itemGroupName;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public List<ItemData> getItemDataList() {
		return itemDataList;
	}

	public void setItemDataList(List<ItemData> itemDataList) {
		this.itemDataList = itemDataList;
	}

	@Override
	public String toString() {
		/*
		return "ItemGroupData [itemGroupOID=" + itemGroupOID + ", itemGroupRepeatKey=" + itemGroupRepeatKey
				+ ", itemGroupName=" + itemGroupName + ", transactionType=" + transactionType + ", itemDataList="
				+ itemDataList + "]";
				*/
		String xmlString =  null;
		try {
		
			JAXBContext jaxbContext = org.eclipse.persistence.jaxb.JAXBContextFactory
	                 .createContext(new Class[]{Odm.class}, null);
	         Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	
	         jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	         StringWriter sw = new StringWriter();
	         jaxbMarshaller.marshal(this,sw);
	         xmlString = sw.toString();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return xmlString;
	}
	
	public void addItemData(ItemData itemD) {
		this.itemDataList.add(itemD);
	}



}
