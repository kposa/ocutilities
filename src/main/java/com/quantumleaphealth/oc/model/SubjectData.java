package com.quantumleaphealth.oc.model;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.quantumleaphealth.oc.utils.OCUtil;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;


@XmlAccessorType(XmlAccessType.FIELD)
public class SubjectData {
	
	public SubjectData() {
		
	}
	
	public SubjectData(String subjectKey) {
		this.subjectKey = subjectKey;
		this.status = "Available";
		this.studySubjectId = getSubjectIdForKey(subjectKey);
		ArrayList<StudyEventData> sedList = new ArrayList<StudyEventData>();
		this.studyEventDataList = sedList;
	}
	
	public SubjectData(String subjectKey, String subId) {
		this.subjectKey = subjectKey;
		this.studySubjectId = subId;
		this.status = "Available";
		
		ArrayList<StudyEventData> sedList = new ArrayList<StudyEventData>();
		this.studyEventDataList = sedList;		
	}
	 @XmlAttribute(name = "SubjectKey")
	 String subjectKey;


	 
	 @XmlAttribute(name = "OpenClinica:Status")
	 String status;
	 
	 @XmlAttribute(name = "OpenClinica:StudySubjectID")
	 String studySubjectId;
	 
	 @XmlElement(name = "StudyEventData")
	 List<StudyEventData> studyEventDataList;

	public String getSubjectKey() {
		return subjectKey;
	}

	public void setSubjectKey(String subjectKey) {
		this.subjectKey = subjectKey;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStudySubjectId() {
		return studySubjectId;
	}

	public void setStudySubjectId(String studySubjectId) {
		this.studySubjectId = studySubjectId;
	}

	public List<StudyEventData> getStudyEventDataList() {
		return studyEventDataList;
	}

	public void setStudyEventDataList(List<StudyEventData> studyEventDataList) {
		this.studyEventDataList = studyEventDataList;
	}

	@Override
	public String toString() {
		//return "SubjectData [subjectKey=" + subjectKey + ", status=" + status + ", studySubjectId=" + studySubjectId
		//		+ ", studyEventDataList=" + studyEventDataList + "]";
		
		String xmlString =  null;
		try {
		
			JAXBContext jaxbContext = org.eclipse.persistence.jaxb.JAXBContextFactory
	                 .createContext(new Class[]{Odm.class}, null);
	         Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	
	         jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	         StringWriter sw = new StringWriter();
	         jaxbMarshaller.marshal(this,sw);
	         xmlString = sw.toString();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return xmlString;
	}
	
	public void addStudyEventData(StudyEventData sde) {
		this.studyEventDataList.add(sde);
	}

	public static String  getSubjectIdForKey(String subkey) {
		String oid = null;
		OCUtil util = new OCUtil();
		String url = util.getAppProperties().getProperty("getSubjectUrl")+subkey;
		String username = util.getAppProperties().getProperty("username");
		String password = util.getAppProperties().getProperty("password");
		String accessToken = OCUtil.getAccessToken("qlhc2.build.openclinica.io", username, password); //username/password need to be for OC data manager, vandhana may already have them in a properties file
		
		try {
			JsonNode jNode = OCUtil.getJson(url, accessToken);
			oid = jNode.get("subjectOid").asText();
			System.out.println(oid);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return oid;
		
	}

}
