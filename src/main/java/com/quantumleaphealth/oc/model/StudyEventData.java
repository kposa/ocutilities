package com.quantumleaphealth.oc.model;

import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.quantumleaphealth.oc.data.OCData;
import com.quantumleaphealth.oc.data.OCData.FormEnum;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;


@XmlAccessorType(XmlAccessType.FIELD)
public class StudyEventData {
	
	public static DateFormat startDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0");
	


	
	public StudyEventData() {
		
	}
	
	public StudyEventData(String eventOID) {
		this.studyEventOID = eventOID;
		switch(eventOID) {
			case "SE_SCREENING":
				this.eventName = "Screening";
			case "SE_MAMMAPRINTTEST_9177":
				this.eventName = "MammaPrint Test (Agendia)";
			case "SE_RANDOMIZATION":
				this.eventName = "Randomization";
		}
		
		this.status = "data entry started";
		this.workflowStatus = "data entry started";
		
        String startDate =   startDf.format(new Date());
        this.startDate = startDate;
	}
	
	public StudyEventData(String eventOID, FormData formData) {
		this(eventOID);
        this.addFormData(formData);
	}
	
	 @XmlAttribute(name ="StudyEventOID")
	 String studyEventOID;

	 @XmlAttribute(name = "OpenClinica:EventName")
	 String eventName;

	 @XmlAttribute(name = "OpenClinica:StartDate")
	 //2021-05-20 00:00:00.0
	 String startDate;
	 
	 @XmlAttribute(name = "OpenClinica:Status")
	 String status;
	 
	 @XmlAttribute(name = "OpenClinica:WorkflowStatus")
	 String workflowStatus;
	 
	 @XmlAttribute(name = "OpenClinica:Signed")
	 String signed;
	 
	 @XmlElement(name = "FormData")
	 List<FormData> formList;

	public String getStudyEventOID() {
		return studyEventOID;
	}

	public void setStudyEventOID(String studyEventOID) {
		this.studyEventOID = studyEventOID;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getWorkflowStatus() {
		return workflowStatus;
	}

	public void setWorkflowStatus(String workflowStatus) {
		this.workflowStatus = workflowStatus;
	}

	public List<FormData> getFormList() {
		return formList;
	}

	public void setFormList(List<FormData> formList) {
		this.formList = formList;
	}

	@Override
	public String toString() {
		/*
		return "StudyEventData [studyEventOID=" + studyEventOID + ", eventName=" + eventName + ", startDate="
				+ startDate + ", status=" + status + ", workflowStatus=" + workflowStatus + ", formList=" + formList
				+ "]";
				*/
		String xmlString =  null;
		try {
		
			JAXBContext jaxbContext = org.eclipse.persistence.jaxb.JAXBContextFactory
	                 .createContext(new Class[]{Odm.class}, null);
	         Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	
	         jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	         StringWriter sw = new StringWriter();
	         jaxbMarshaller.marshal(this,sw);
	         xmlString = sw.toString();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return xmlString;
	}
	
	public void addFormData(FormData fData) {

		if(formList == null)
			formList = new ArrayList<FormData>();
			
		formList.add(fData);
		
	}

	public String getSigned() {
		return signed;
	}

	public void setSigned(String signed) {
		this.signed = signed;
	}

	

	


}
