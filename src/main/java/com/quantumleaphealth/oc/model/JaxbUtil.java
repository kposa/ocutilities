package com.quantumleaphealth.oc.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.UnmarshallerHandler;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class JaxbUtil {

	public static void main(String[] args) {

		//createXmlFromODM();
		createODMFromXml();
	}
	
	public static void  createODMFromXml(){
		File xmlFile = new File("/Users/krishnaposa/Documents/work/odmSample.xml");
		 
		try
		{
		   
			 	SAXParserFactory spf = SAXParserFactory.newInstance();
		        SAXParser sp = spf.newSAXParser();
		        XMLReader xmlReader = sp.getXMLReader();
		        OCXmlFilter xmlFilter = new OCXmlFilter(xmlReader);

		        JAXBContext context = JAXBContext.newInstance(Odm.class);
		        Unmarshaller um = context.createUnmarshaller();
		        UnmarshallerHandler unmarshallerHandler = um.getUnmarshallerHandler();
		        xmlFilter.setContentHandler(unmarshallerHandler);

		        
		        InputStream in = new FileInputStream(xmlFile);
		        InputSource inputSource = new InputSource(new InputStreamReader(in));
		        
		      
		        xmlFilter.parse(inputSource);

		        Odm odm = (Odm) unmarshallerHandler.getResult();
		     
		    System.out.println(odm);
		}
		catch (Exception e) 
		{
		    e.printStackTrace();
		}
	}
	


	public static void getXMLString(Node node, boolean withoutNamespaces, StringBuffer buff, boolean endTag) {
	    buff.append("<")
	        .append(namespace(node.getNodeName(), withoutNamespaces));

	    if (node.hasAttributes()) {
	        buff.append(" ");

	        NamedNodeMap attr = node.getAttributes();
	        int attrLenth = attr.getLength();
	        for (int i = 0; i < attrLenth; i++) {
	            Node attrItem = attr.item(i);
	            String name = namespace(attrItem.getNodeName(), withoutNamespaces);
	            String value = attrItem.getNodeValue();

	            buff.append(name)
	                .append("=")
	                .append("\"")
	                .append(value)
	                .append("\"");

	            if (i < attrLenth - 1) {
	                buff.append(" ");
	            }
	        }
	    }

	    if (node.hasChildNodes()) {
	        buff.append(">");

	        NodeList children = node.getChildNodes();
	        int childrenCount = children.getLength();

	        if (childrenCount == 1) {
	            Node item = children.item(0);
	            int itemType = item.getNodeType();
	            if (itemType == Node.TEXT_NODE) {
	                if (item.getNodeValue() == null) {
	                    buff.append("/>");
	                } else {
	                    buff.append(item.getNodeValue());
	                    buff.append("</")
	                        .append(namespace(node.getNodeName(), withoutNamespaces))
	                        .append(">");
	                }

	                endTag = false;
	            }
	        }

	        for (int i = 0; i < childrenCount; i++) {
	            Node item = children.item(i);
	            int itemType = item.getNodeType();
	            if (itemType == Node.DOCUMENT_NODE || itemType == Node.ELEMENT_NODE) {
	                getXMLString(item, withoutNamespaces, buff, endTag);
	            }
	        }
	    } else {
	        if (node.getNodeValue() == null) {
	            buff.append("/>");
	        } else {
	            buff.append(node.getNodeValue());
	            buff.append("</")
	                .append(namespace(node.getNodeName(), withoutNamespaces))
	                .append(">");
	        }

	        endTag = false;
	    }

	    if (endTag) {
	        buff.append("</")
	            .append(namespace(node.getNodeName(), withoutNamespaces))
	            .append(">");
	    }
	}

	private static String namespace(String str, boolean withoutNamespace) {
	    if (withoutNamespace && str.contains(":")) {
	        return str.substring(str.indexOf(":") + 1);
	    }

	    return str;
	}
	
	public static void createXmlFromODM(){
		  JAXBContext jaxbContext = null;
	        try {


	            jaxbContext = org.eclipse.persistence.jaxb.JAXBContextFactory
	                    .createContext(new Class[]{Odm.class}, null);

	            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

	            // output pretty printed
	            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

	            Odm odm = new Odm();
	            odm.setDescription("Study Metadata");
	            
	            DateFormat df = new SimpleDateFormat("YYYYMMDD'T'hhmmss+0000");
	            String fileOid =   "Study-MetaD" + df.format(new Date());
	            
	            
	            DateFormat creationDf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+00:00");
	            String creationDate =   creationDf.format(new Date());
	            odm.setCreationDateTime(creationDate);
	            odm.setFileOID(fileOid);
	            odm.setOdmVersion("1.3");
	            odm.setFileType("Snapshot");
	            
	            ClinicalData cd = new ClinicalData();
	            cd.setStudyOID("S_ISPY(TEST)");
	            cd.setMetaDataVersionOID("null");
	            
	            odm.setClinicalData(cd);
	            
	            SubjectData sd = new SubjectData();
	            sd.setStatus("Available");
	            sd.setStudySubjectId("EAR0002001");
	            sd.setStudySubjectId("SS_EAR0002001");
	            
	            StudyEventData sed = new StudyEventData();
	            sed.setStudyEventOID("SE_SCREENING");
	            sed.setEventName("Screening");
	            
	            DateFormat startDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0");
	            String startDateStr =   startDate.format(new Date());
	            sed.setStartDate(startDateStr);
	            sed.setStatus("data entry started");
	            sed.setWorkflowStatus("data entry started");
	            
	            ArrayList<StudyEventData> sedList = new ArrayList<StudyEventData>() ;
	            sedList.add(sed);
	            sd.setStudyEventDataList(sedList);
	            
	            cd.setSubjectData(sd);
	            
	            FormData fd = new FormData();
	            fd.setFormOID("F_ONSTUDYELIGI");
	            fd.setFormName("On-Study Eligibility");
	            fd.setLayoutId("16");
	            fd.setCreatedDate(creationDf.format(new Date()));
	            fd.setUpdatedDate(creationDf.format(new Date()));
	            fd.setCreatedBy("k.posa");
	            fd.setUpdatedBy("k.posa");
	            fd.setStatus("data entry started");            
	            fd.setWorkflowStatus("data entry started");   
	            
	            ArrayList<FormData> fdList = new ArrayList<FormData>();
	            fdList.add(fd);
	            
	            sed.setFormList(fdList);
	            
	            ArrayList<ItemGroupData> igdList = new ArrayList<ItemGroupData>();
	            ItemGroupData igd = new ItemGroupData();
	            igd.setItemGroupName("S_METHOD_OF_DETECTION");
	            igd.setItemGroupOID("IG_ONSTU_S_METHOD_OF_DETECTION");
	            igd.setItemGroupRepeatKey("1");
	            igd.setTransactionType("Insert");
	            
	            igdList.add(igd);
	            
	            fd.setItemGroupDataList(igdList);
	            
	            
	            ArrayList<ItemData> idList = new ArrayList<ItemData>();
	            ItemData id1 = new ItemData();
	            id1.setItemName("MASS_IDENTIFICATION_DATE");
	            id1.setItemOID("I_ONSTU_MASS_IDENTIFICATION_DATE");
	            id1.setValue("2021-05-18");
	            idList.add(id1);
	            
	            
	            igd.setItemDataList(idList);
	            
	            // output to a xml file
	            
	            StringWriter sw = new StringWriter();
	            jaxbMarshaller.marshal(odm,sw);
	            String xmlString = sw.toString();
	            
	            System.out.println(xmlString);
	            
	            //jaxbMarshaller.marshal(odm, new File("/tmp/odmTest.xml"));
	            

	            // output to console
	             //jaxbMarshaller.marshal(odm, System.out);

	        } catch (JAXBException e) {
	            e.printStackTrace();
	        }
	}
	
	
	

}
