@XmlSchema(
    namespace = "http://www.cdisc.org/ns/odm/v1.3",
    elementFormDefault = XmlNsForm.QUALIFIED,
    xmlns = {
        @XmlNs(prefix="", namespaceURI="http://www.cdisc.org/ns/odm/v1.3"),
         @XmlNs(prefix="OpenClinica", namespaceURI="http://www.openclinica.org/ns/odm_ext_v130/v3.1"),
        @XmlNs(prefix="xsi", namespaceURI="http://www.w3.org/2001/XMLSchema-instance"),
          @XmlNs(prefix="OpenClinicaRules", namespaceURI="http://www.openclinica.org/ns/rules/v3.1")
        
    }
)  

package com.quantumleaphealth.oc.model;

import jakarta.xml.bind.annotation.XmlNs;
import jakarta.xml.bind.annotation.XmlNsForm;
import jakarta.xml.bind.annotation.XmlSchema;
