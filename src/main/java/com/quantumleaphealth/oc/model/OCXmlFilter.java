package com.quantumleaphealth.oc.model;


import org.xml.sax.*;
import org.xml.sax.helpers.XMLFilterImpl;

public class OCXmlFilter extends XMLFilterImpl{
	
	 public OCXmlFilter(XMLReader xmlReader) {
         super(xmlReader);
     }

     @Override
     public void startElement(String uri, String localName, String qName,
             Attributes attributes) throws SAXException {
         int colonIndex = qName.indexOf(':');
         if(colonIndex >= 0) {
             qName = qName.substring(colonIndex + 1);
         }
         uri = "http://www.cdisc.org/ns/odm/v1.3"; //to prevent unknown XML element exception, we have to specify the namespace here         
         
         super.startElement(uri, localName, qName, attributes);
     }

     @Override
     public void endElement(String uri, String localName, String qName)
             throws SAXException {
         int colonIndex = qName.indexOf(':');
         if(colonIndex >= 0) {
             qName = qName.substring(colonIndex + 1);
         }
         super.endElement(uri, localName, qName);
     }

     
 

}
