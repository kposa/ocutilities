package com.quantumleaphealth.oc.data;

public class OCData {
	
	private String oid;
	
	public enum StudyEventsEnum {
		
		SE_SCREENING("SE_SCREENING"), SE_MAMMAPRINTTEST_9177("SE_MAMMAPRINTTEST_9177"),
		SE_RANDOMIZATION("SE_RANDOMIZATION");
		
		private final String text;

		StudyEventsEnum(final String text) {
			this.text = text;
		}
		@Override
	    public String toString() {
	        return text;
	    }
	}
	
	public enum FormEnum {
		F_ONSTUDYELIGI("F_ONSTUDYELIGI"), 
		F_TISSUEPRE("F_TISSUEPRE"), 
		F_ONSTUDYPATHO("F_ONSTUDYPATHO"), F_DISEASEASSES("F_DISEASEASSES"), 
		F_MRIVOLUME("F_MRIVOLUME"), F_PATIENTDEMOG("F_PATIENTDEMOG"),
		F_MAMMAPRINT("F_MAMMAPRINT"), F_RANDOMIZATIO("F_RANDOMIZATIO");
		
		private final String text;

		FormEnum(final String text) {
			this.text = text;
		}
		@Override
	    public String toString() {
	        return text;
	    }
	}

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}
	

}
