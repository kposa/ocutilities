package com.quantumleaphealth.oc.data;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.quantumleaphealth.oc.model.FormData;
import com.quantumleaphealth.oc.model.ItemData;
import com.quantumleaphealth.oc.model.ItemGroupData;

public class OnStudyEligibility extends OCData{

	private FormData onStudyFormData;
	public OnStudyEligibility(FormData studyForm) {
		this.onStudyFormData = studyForm;
	}
	
	public OnStudyEligibility() {
		this.setOid("F_ONSTUDYELIGI" );
		onStudyFormData = new FormData("F_ONSTUDYELIGI",
				"On-Study Eligibility",
				"16", "data entry started", "data entry started");
		
		ItemGroupData methodOfDetection = new ItemGroupData("IG_ONSTU_S_METHOD_OF_DETECTION",
				"1", "S_METHOD_OF_DETECTION", "Insert");
		methodOfDetection.addItemData(new ItemData("I_ONSTU_MASS_IDENTIFICATION_DATE",
				"MASS_IDENTIFICATION_DATE", "2021-05-18"));
		methodOfDetection.addItemData(new ItemData("I_ONSTU_METHOD_OF_DETECTION",
				"METHOD_OF_DETECTION", "Self exam"));
		methodOfDetection.addItemData(new ItemData("I_ONSTU_SCREENING_PRIOR_TO_DETECT",
				"SCREENING_PRIOR_TO_DETECT", "N"));
		
		ItemGroupData oseInfo = new ItemGroupData("IG_ONSTU_S_OSE_INFORMATION",
				"1", "S_OSE_INFORMATION", "Insert");
		oseInfo.addItemData(new ItemData("I_ONSTU_ACCEPT_COMORBID",
				"ACCEPT_COMORBID", "yes"));
		oseInfo.addItemData(new ItemData("I_ONSTU_ACCEPT_COMORBID_ELIG_CALC2",
				"ACCEPT_COMORBID_ELIG_CALC2", "1"));
		oseInfo.addItemData(new ItemData("I_ONSTU_ACCEPT_LABS",
				"ACCEPT_LABS", "yes"));
		oseInfo.addItemData(new ItemData("I_ONSTU_ACCEPT_STUDY_ARM",
				"ACCEPT_STUDY_ARM", "yes"));
		oseInfo.addItemData(new ItemData("I_ONSTU_ARM_ELIG_CALC2",
				"ARM_ELIG_CALC2", "1"));
		oseInfo.addItemData(new ItemData("I_ONSTU_COVID_ELIG_CALC1",
				"COVID_ELIG_CALC1", "0"));
		oseInfo.addItemData(new ItemData("I_ONSTU_COVID_ELIG_CALC2",
				"COVID_ELIG_CALC2", "1"));
		oseInfo.addItemData(new ItemData("I_ONSTU_DIAGNOSED_YES",
				"DIAGNOSED_YES", "N"));
		oseInfo.addItemData(new ItemData("I_ONSTU_EOP_ARM_ELIG_CALC2",
				"EOP_ARM_ELIG_CALC2", "0"));
		oseInfo.addItemData(new ItemData("I_ONSTU_EXCLUSION_OS_COMORBID",
				"EXCLUSION_OS_COMORBID", "yes"));
		oseInfo.addItemData(new ItemData("I_ONSTU_EXCLUSION_OS_COMORBID_ELIG_6672",
				"EXCLUSION_OS_COMORBID_ELIG_CALC2", "1"));
		oseInfo.addItemData(new ItemData("I_ONSTU_LAB_ELIG_CALC2",
				"LAB_ELIG_CALC2", "1"));
		oseInfo.addItemData(new ItemData("I_ONSTU_OSE_ELIGIBILITY_CRITERIA_ME",
				"OSE_ELIGIBILITY_CRITERIA_MET", "yes"));
		oseInfo.addItemData(new ItemData("I_ONSTU_OSE_ELIG_CALC2",
				"OSE_ELIG_CALC2", "1"));
		oseInfo.addItemData(new ItemData("I_ONSTU_PROCEED_RANDO",
				"PROCEED_RANDO", "Y"));

		
		ItemGroupData preeligEcog = new ItemGroupData("IG_ONSTU_S_PREELIG_ECOG",
				"1", "S_PREELIG_ECOG", "Insert");
		preeligEcog.addItemData(new ItemData("I_ONSTU_ECOG_ELIG_CALC2",
				"ECOG_ELIG_CALC2", "1"));
		preeligEcog.addItemData(new ItemData("I_ONSTU_PREELIG_ECOG",
				"PREELIG_ECOG", "0"));

		
		
		ItemGroupData preeligElig = new ItemGroupData("IG_ONSTU_S_PREELIG_ELIG", 
				"1", "S_PREELIG_ELIG", "Insert");
		preeligElig.addItemData(new ItemData("I_ONSTU_COMORBID_ELIG_CALC2",
				"COMORBID_ELIG_CALC2", "1"));
		preeligElig.addItemData(new ItemData("I_ONSTU_ELIGIBILITY_CRITERIA_MET",
				"ELIGIBILITY_CRITERIA_MET", "yes"));
		preeligElig.addItemData(new ItemData("I_ONSTU_ELIGIBLE_CRITERIA_MET",
				"ELIGIBLE_CRITERIA_MET", "Y"));
		preeligElig.addItemData(new ItemData("I_ONSTU_ELIGIBLE_CRITERIA_MET_CALC",
				"ELIGIBLE_CRITERIA_MET_CALC", "Y"));
		preeligElig.addItemData(new ItemData("I_ONSTU_ELIG_CALC2",
				"ELIG_CALC2", "1"));
		preeligElig.addItemData(new ItemData("I_ONSTU_EXCLUSION_COMORBID",
				"EXCLUSION_COMORBID", "yes"));
		preeligElig.addItemData(new ItemData("I_ONSTU_EXCLUSION_PRIOR_TREATMENT",
				"EXCLUSION_PRIOR_TREATMENT", "yes"));
		preeligElig.addItemData(new ItemData("I_ONSTU_EXCLUSION_SYMPTOMS_MET",
				"EXCLUSION_SYMPTOMS_MET", "yes"));
		preeligElig.addItemData(new ItemData("I_ONSTU_PRIOR_TREAT_ELIG_CALC2",
				"PRIOR_TREAT_ELIG_CALC2", "1"));
		preeligElig.addItemData(new ItemData("I_ONSTU_STUDY_INFO",
				"STUDY_INFO", "MAIN"));
		preeligElig.addItemData(new ItemData("I_ONSTU_SYMPTOMS_ELIG_CALC2",
				"SYMPTOMS_ELIG_CALC2", "1"));
		
		
		onStudyFormData.addItemGroupData(methodOfDetection);
		onStudyFormData.addItemGroupData(oseInfo);
		onStudyFormData.addItemGroupData(preeligEcog);
		onStudyFormData.addItemGroupData(preeligElig);
			
	}
	
	public FormData getFormData() {
		return this.onStudyFormData;
	}
	
	public static Map<String,String> oseData1(){
		Map<String, String> oseContent = new HashMap<String, String>();
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("/Users/krishnaposa/eclipse-workspace/ocutilities/src/main/resources/templates/OSE-data"));
			for (String key : properties.stringPropertyNames()) {
				oseContent.put(key, properties.get(key).toString());
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return oseContent;
	}
	
	public void updateWithOseData(Map<String,String> oseData) {		
		for(ItemGroupData igData: this.onStudyFormData.getItemGroupDataList()) {
			for (ItemData id: igData.getItemDataList()) {
				String itemOid = id.getItemOID();
				if(oseData.containsKey(itemOid)){
					String value = id.getValue();
					id.setValue(oseData.get(itemOid));
				}
					
			}
		}	
	}
	
	
}
