package com.quantumleaphealth.oc.data;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.quantumleaphealth.oc.model.ClinicalData;
import com.quantumleaphealth.oc.model.FormData;
import com.quantumleaphealth.oc.model.ItemData;
import com.quantumleaphealth.oc.model.ItemGroupData;
import com.quantumleaphealth.oc.model.Odm;
import com.quantumleaphealth.oc.model.StudyEventData;
import com.quantumleaphealth.oc.model.SubjectData;

public class FormDataUtil extends OCData{

	public static DateFormat odmFileOidDf = new SimpleDateFormat("yyyyMMddHHmmss+0000");	
	private FormData formData;
	public FormDataUtil(FormData formData) {
		this.formData = formData;
	}
	
	
	
	public FormData getFormData() {
		return this.formData;
	}
	
	
	public void updateWithData(String file) {	
		
		Map<String, String> content = new HashMap<String, String>();
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(file));
			for (String key : properties.stringPropertyNames()) {
				content.put(key, properties.get(key).toString());
			}
	
			for(ItemGroupData igData: this.formData.getItemGroupDataList()) {
				for (ItemData id: igData.getItemDataList()) {
					String itemOid = id.getItemOID();
					if(content.containsKey(itemOid)){
						String value = id.getValue();
						id.setValue(content.get(itemOid));
					}
						
				}
			}	
		
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	
	public Odm addFormToOdm(String studyOid, 
			String subjectkey,
			String StudyEventOid) {
		
		//20210523140030+0000
		
		String fileOid = "Study-MetaD" + odmFileOidDf.format(new Date());;
		Odm  odm = new Odm(fileOid);
		ClinicalData  cd = new ClinicalData(studyOid);
		odm.setClinicalData(cd);
		
		List<StudyEventData> studyEventList = new ArrayList<StudyEventData>();
		
		SubjectData sd = new SubjectData( subjectkey);
		
		sd.setStudyEventDataList(studyEventList);
		cd.setSubjectData(sd);
		
		
		StudyEventData seData = new StudyEventData(StudyEventOid);
		seData.addFormData(this.formData);
		studyEventList.add(seData);
		
		return odm;
	}
	
	
	
}
