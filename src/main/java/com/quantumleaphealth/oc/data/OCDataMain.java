package com.quantumleaphealth.oc.data;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

import com.quantumleaphealth.oc.model.FormData;
import com.quantumleaphealth.oc.model.ItemData;
import com.quantumleaphealth.oc.model.ItemGroupData;
import com.quantumleaphealth.oc.model.Odm;
import com.quantumleaphealth.oc.model.StudyEventData;
import com.quantumleaphealth.oc.model.SubjectData;
import com.quantumleaphealth.oc.utils.OCUtil;
import com.quantumleaphealth.oc.utils.REligibilityUtil;

@SpringBootApplication
public class OCDataMain {
	
	public static void main(String[] args) {
		SpringApplication.run(OCDataMain.class, args);
		
	}
	
	@Autowired
	private Environment env;
	
	
	
	public static void testPTP() {
		
		Odm odm = null;
		Path fileName = Path.of("/Users/krishnaposa/eclipse-workspace/ocutilities/src/main/resources/templates/ptp-form.xml");
		String propFile = "/Users/krishnaposa/eclipse-workspace/ocutilities/src/main/resources/templates/ptp-data-1.properties";
		String xml = null;
		try {
			xml = Files.readString(fileName);
			FormData data = FormData.loadFromXml(xml);
			
			FormDataUtil fdUtil = new FormDataUtil(data);
			fdUtil.updateWithData(propFile);
			
			odm = fdUtil.addFormToOdm("S_ISPY(TEST)",
					"EAR0001102","SE_SCREENING");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.print(odm);
	
	}
	
	public static void testDA() {
		
		Odm odm = null;
		Path fileName = Path.of("/Users/krishnaposa/eclipse-workspace/ocutilities/src/main/resources/templates/da-form.xml");
		String propFile = "/Users/krishnaposa/eclipse-workspace/ocutilities/src/main/resources/templates/da-data-1.properties";
		String xml = null;
		try {
			xml = Files.readString(fileName);
			FormData data = FormData.loadFromXml(xml);
			
			FormDataUtil fdUtil = new FormDataUtil(data);
			fdUtil.updateWithData(propFile);
			
			odm = fdUtil.addFormToOdm("S_ISPY(TEST)",
					"EAR0001102","SE_SCREENING");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.print(odm);
	
	}
	
	public static void testGetSubjectId() {
		String oid = SubjectData.getSubjectIdForKey("EAR0001014");
		System.out.println(oid);
		
	}
	
	public static void testOdmUpdate() {
		String oid = SubjectData.getSubjectIdForKey("EAR0001102");
		
		Path path = Paths.get("/Users/krishnaposa/eclipse-workspace/ocutilities/src/main/resources/templates/odmOSEMain.xml");
		Odm odm = null;
		try {
			String content = Files.readString(path, StandardCharsets.US_ASCII);
			odm = Odm.loadFromXml(content);
			System.out.println(odm);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<StudyEventData> sdList = odm.getClinicalData().getSubjectData().getStudyEventDataList();
		for(StudyEventData sd: sdList) {
			if(sd.getStudyEventOID().equals("SE_SCREENING")) {
				List<FormData> fList = sd.getFormList();
				for(FormData fd: fList) {
					if(fd.getFormOID().equals("F_ONSTUDYELIGI")) {
						for(ItemGroupData igData: fd.getItemGroupDataList()) {
							for (ItemData id: igData.getItemDataList()) {
								if(id.getItemOID().equals("I_ONSTU_STUDY_INFO")) {
									String value = id.getValue();
									id.setValue("EOP");
								}
									
							}
						}
					}
				}
			}
		}
		System.out.println("AFTER UPDATE");
		System.out.println(odm);
		
	}
	

	
	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
			Odm odmData = Odm.getOdmForSubjectKey("EAR0001473");
			System.out.println(odmData);

		};
	}

}
