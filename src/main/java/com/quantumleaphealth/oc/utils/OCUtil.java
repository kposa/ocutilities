package com.quantumleaphealth.oc.utils;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Random;
import java.util.Scanner;

@Component
//@PropertySource({ "classpath:application.properties" })
public class OCUtil implements EnvironmentAware {

	@Autowired
	public static Environment env;

	@Override
	public void setEnvironment(Environment arg0) {
		if (env == null) {
			env = arg0;
		}

	}

	private Properties appproperties = null;

	public OCUtil() {
		appproperties = new Properties();
		try {
			appproperties.load(new FileInputStream(
					System.getProperty("user.dir") + "/src/main/resources/templates/application.properties"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public Properties getAppProperties() {
		return appproperties;
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	public static String getConfigProp(String key) {
		return env.getProperty(key);
	}

	private static final javax.xml.namespace.NamespaceContext NamespaceContext = new NamespaceContext() {
		public String getNamespaceURI(String prefix) {
			return prefix.equals("OpenClinica") ? "http://www.openclinica.org/ns/odm_ext_v130/v3.1" : null;
		}

		public Iterator getPrefixes(String val) {
			return null;
		}

		public String getPrefix(String uri) {
			return null;
		}
	};

	public static Document getXML(String urlString, String accessToken) throws Exception {
		Document xmlDocument = null;

		String resultString = httpGetString(urlString, accessToken, "application/xml");

		if (resultString != null) {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			// factory.setNamespaceAware(true);
			DocumentBuilder builder = factory.newDocumentBuilder();
			xmlDocument = builder.parse(new ByteArrayInputStream(resultString.getBytes("UTF-8")));
		}

		return xmlDocument;
	}

	public static JsonNode getJson(String urlString, String accessToken) throws Exception {
		JsonNode rootNode = null;

		String resultString = httpGetString(urlString, accessToken, null);

		if (resultString != null) {
			ObjectMapper mapper = new ObjectMapper();
			rootNode = mapper.readTree(resultString);
		}

		return rootNode;
	}

	public static String httpGetString(String urlString, String accessToken, String accept) {
		try {
			URL url = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setDoOutput(true);
			connection.setRequestProperty("Authorization", String.format("Bearer %s", accessToken));
			if (accept != null)
				connection.setRequestProperty("accept", accept);

			if (connection.getResponseCode() == 200) {
				Reader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				for (int c; (c = reader.read()) >= 0;) {
					sb.append((char) c);
				}

				return sb.toString();
			} else {
				Reader reader = new BufferedReader(new InputStreamReader(connection.getErrorStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				for (int c; (c = reader.read()) >= 0;) {
					sb.append((char) c);
				}

				System.out.println("httpGetString ERROR: bad response code " + connection.getResponseCode());// + " " +
																												// sb.toString());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}

	public static String getAccessToken(String hostname, String username, String password) {
		try {
			// System.out.println("getAccessToken begin");

			JSONObject requestJSON = new JSONObject();
			requestJSON.put("username", username);
			requestJSON.put("password", password);
			byte[] requestBytes = requestJSON.toString().getBytes("UTF-8");

			// System.out.println(requestJSON.toString());

			URL url = new URL(String.format("https://%s/user-service/api/oauth/token", hostname));
			// System.out.println(url.toString());
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("Content-Length", String.valueOf(requestBytes.length));
			connection.setRequestProperty("Accept", "*/*");
			connection.setDoOutput(true);

			connection.getOutputStream().write(requestBytes);

			if (connection.getResponseCode() == 200) {
				Reader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				for (int c; (c = reader.read()) >= 0;) {
					sb.append((char) c);
				}

				return sb.toString();
			} else {
				Reader reader = new BufferedReader(new InputStreamReader(connection.getErrorStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				for (int c; (c = reader.read()) >= 0;) {
					sb.append((char) c);
				}

				System.out.println("ERROR: bad response code " + connection.getResponseCode() + ", " + sb.toString());
			}
		} catch (Exception ex) {
			System.out.println("ERROR: " + ex.getMessage());
		}

		return null;
	}

	static String importData(String accessToken, String xmlData, String filename) throws Exception {
		System.out.println("importData begin");

		HttpPost post = new HttpPost("https://qlhc2.openclinica.io/OpenClinica/pages/auth/api/clinicaldata/import");
		post.addHeader("Authorization", String.format("Bearer %s", accessToken));

		// HttpPost post = new HttpPost("http://ptsv2.com/t/nd3ss-1580325822/post");

		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		builder.addBinaryBody("file", xmlData.getBytes(), ContentType.TEXT_XML, filename);
		HttpEntity entity = builder.build();
		post.setEntity(entity);

		CloseableHttpClient client = HttpClients.createDefault();
		CloseableHttpResponse response = client.execute(post);
		// client.close();
		System.out.println(response.getStatusLine().getStatusCode());
		String jobId = null;
		if (response.getStatusLine().getStatusCode() == 200) {

			Reader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
			StringBuilder sb = new StringBuilder();
			for (int c; (c = reader.read()) >= 0;) {
				sb.append((char) c);
			}

			jobId = sb.toString().replace("job uuid: ", "");
		}

		System.out.println("JobId=" + jobId);
		boolean importSucceeded = true;
		String jobResult = "";
		for (int i = 0; i < 10; i++) {
			Thread.sleep(2000);
			jobResult = OCUtil.httpGetString(String
					.format("https://qlhc2.openclinica.io/OpenClinica/pages/auth/api/jobs/%s/downloadFile", jobId),
					accessToken, "*/*");

			if (jobResult != null ) {
				if(jobResult.indexOf("Failed") > -1) {
					System.out.println("Job failed with jobResult: " + jobResult);
					importSucceeded = false;
				}
				else if(jobResult.indexOf("Updated") > -1) {
					System.out.println("Job Import succeeded: " + jobResult);
					importSucceeded = true;
					break;
				}
			}
			else {
				System.out.println("Job failed with null jobResult");
				importSucceeded = false;
			}
		}
		if(importSucceeded == false) {
			System.err.println("Import failed for job:" + jobId + " ,with error:" + jobResult);
			System.err.println(xmlData);
			System.err.println("_________________________________");
			jobId = jobId + ":false";
		}
		else
			jobId = jobId+ ":true";
		
		return jobId;

	}

	public static String convertDocToString(Document doc) {
		String output = null;
		try {
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			output = writer.getBuffer().toString().replaceAll("\n|\r", "");
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return output;
	}

	public static String getTagValue(Document xmlDoc, String tagName) throws Exception {
		xmlDoc.getDocumentElement().normalize();
		String tagValue = null;
		NodeList nodeList = xmlDoc.getElementsByTagName(tagName);

		for (int temp = 0; temp < nodeList.getLength(); temp++) {
			Node nNode = nodeList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				org.w3c.dom.Element eElement = (org.w3c.dom.Element) nNode;
				tagValue = eElement.getFirstChild().getNodeValue();
				break;
			}
		}
		return tagValue;
	}

	public static void setNodeAttrValue(Document xmlDoc, String selAttributeStr, String selAttrVal, String attrToChange,
			String newVal) throws XPathExpressionException {
		XPath xpath = XPathFactory.newInstance().newXPath();
		StringBuilder str = new StringBuilder();
		str.append("//*[contains(@").append(selAttributeStr).append(", '").append(selAttrVal).append("')]");
		String xpathStr = str.toString();
		NodeList nodes = (NodeList) xpath.evaluate(xpathStr, xmlDoc, XPathConstants.NODESET);

		for (int idx = 0; idx < nodes.getLength(); idx++) {
			NamedNodeMap attributes = nodes.item(idx).getAttributes();
			Node node = attributes.getNamedItem(attrToChange);
			if (node == null) {
				node = xmlDoc.createAttribute(attrToChange);
				node.setNodeValue(newVal);
				attributes.setNamedItem(node);
			}
			node.setNodeValue(newVal);
		}

	}

	public static void addUpdateFormAttrValue(Document xmlDoc, String selAttr, String selAttrValue, String attr,
			String attrVal) throws XPathExpressionException {
		// use xpath to update all CRF SDV status values to verified
		XPath xPath = XPathFactory.newInstance().newXPath();
		// xPath.setNamespaceContext(OCUtils.NamespaceContext);
		XPathExpression expressionLinkView = xPath.compile("//FormData");

		NodeList nodeList = (NodeList) expressionLinkView.evaluate(xmlDoc, XPathConstants.NODESET);
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node nodeForm = nodeList.item(i);
			NamedNodeMap attributes = nodeForm.getAttributes();
			Node node = attributes.getNamedItem(attr);
			String status = attributes.getNamedItem(selAttr).getNodeValue();
			if (status != null && status.equalsIgnoreCase(selAttrValue)) {
				if (node == null) {
					node = xmlDoc.createAttribute(attr);
					attributes.setNamedItem(node);
				}
				node.setNodeValue(attrVal);

			}
		}
		removeItemData(xmlDoc);

	}

	public static void removeItemData(Document xmlDoc) {
		NodeList itemGroupDataNodes = xmlDoc.getElementsByTagName("ItemGroupData");
		for (int i = 0; i < itemGroupDataNodes.getLength(); i++) {
			NodeList categorieslist = itemGroupDataNodes.item(i).getChildNodes();
			while (categorieslist.getLength() > 0) {
				Node node = categorieslist.item(0);
				node.getParentNode().removeChild(node);
			}
		}
	}

	// element = //StudyEventData
	public static boolean checkListAndAttrValues(Document xmlDoc, String element, HashMap<String, String> checkMap,
			HashMap<String, String> addMap) throws XPathExpressionException {
		// use xpath to update all CRF SDV status values to verified
		XPath xPath = XPathFactory.newInstance().newXPath();
		// xPath.setNamespaceContext(OCUtils.NamespaceContext);
		XPathExpression expressionLinkView = xPath.compile("//" + element);
		boolean allMatch = true;
		NodeList nodeList = (NodeList) expressionLinkView.evaluate(xmlDoc, XPathConstants.NODESET);
		for (int i = 0; i < nodeList.getLength(); i++) {
			allMatch = true;
			Node nodeStudyEventData = nodeList.item(i);
			NamedNodeMap attributes = nodeStudyEventData.getAttributes();

			for (String key : checkMap.keySet()) {
				String status = attributes.getNamedItem(key).getNodeValue();
				if (status != null && !status.equalsIgnoreCase(checkMap.get(key))) {
					allMatch = false;
					break;
				}
			}
			if (allMatch) {
				for (String addKey : addMap.keySet()) {
					Node node = attributes.getNamedItem(addKey);
					if (node == null) {
						node = xmlDoc.createAttribute(addKey);
						attributes.setNamedItem(node);
					}
					node.setNodeValue(addMap.get(addKey));

				}
				Element signature = xmlDoc.createElement("Signature");
				signature.setAttribute("Attestation", "k.posa+data_spec@quantumleaphealth.org");
				nodeStudyEventData.appendChild(signature);
				break;
			}
		}
		return allMatch;

	}

	public static String getNodeAttrValue(Document xmlDoc, String selAttributeStr, String selAttrVal, String attrToGet)
			throws XPathExpressionException {
		XPath xpath = XPathFactory.newInstance().newXPath();
		StringBuilder str = new StringBuilder();
		str.append("//*[contains(@").append(selAttributeStr).append(", '").append(selAttrVal).append("')]");
		String xpathStr = str.toString();
		NodeList nodes = (NodeList) xpath.evaluate(xpathStr, xmlDoc, XPathConstants.NODESET);
		String val = null;
		if (nodes.getLength() == 1) {
			Node value = nodes.item(0).getAttributes().getNamedItem(attrToGet);
			val = value.getNodeValue();
		}
		return val;

	}

	public void updateSDV(String username, String password, String subjectId) throws Exception {

		String accessToken = OCUtil.getAccessToken("qlhc2.build.openclinica.io", username, password); // username/password
																										// need to be
																										// for OC data
																										// manager,
																										// vandhana may
																										// already have
																										// them in a
																										// properties
																										// file
		String url = getConfigProp("odmUrlWithSubjectId");
		String urlODM = String.format(url, subjectId);

		Document documentODM = getXML(urlODM, accessToken);
		String prexml = convertDocToString(documentODM);
		System.out.println("XML before update =" + prexml);
		addUpdateFormAttrValue(documentODM, "OpenClinica:Status", "data entry complete", "OpenClinica:SdvStatus",
				"Verified");

		String xml = convertDocToString(documentODM);
		Random random = new Random();
		int x = random.nextInt(900) + 100;
		String importName = subjectId + "_" + x + ".xml";
		System.out.println("importing this xml for subjectId:" + importName);
		String jobId = importData(accessToken, xml, importName);

	}

	public void signMammaprint(String username, String password, String subjectId) throws Exception {
		String accessToken = OCUtil.getAccessToken("qlhc2.build.openclinica.io", username, password); // username/password
																										// need to be
																										// for OC data
																										// manager,
																										// vandhana may
																										// already have
																										// them in a
																										// properties
																										// file
		String url = getConfigProp("odmUrlWithSubjectId");
		String urlODM = String.format(url, subjectId);

		Document documentODM = getXML(urlODM, accessToken);
		String prexml = convertDocToString(documentODM);
		HashMap<String, String> checkMap = new HashMap<String, String>();
		checkMap.put("OpenClinica:EventName", "MammaPrint Test (Agendia)");
		// checkMap.put( "OpenClinica:WorkflowStatus", "data entry complete");

		HashMap<String, String> addUpdateMap = new HashMap<String, String>();
		addUpdateMap.put("OpenClinica:Signed", "Yes");
		addUpdateMap.put("OpenClinica:Status", "signed");
		addUpdateMap.put("OpenClinica:WorkflowStatus", "completed");

		System.out.println("XML before sign update =" + prexml);
		boolean importNeeded = checkListAndAttrValues(documentODM, "StudyEventData", checkMap, addUpdateMap);

		if (importNeeded) {
			String xml = convertDocToString(documentODM);

			Random random = new Random();
			int x = random.nextInt(900) + 100;
			String importName = subjectId + "_" + x + ".xml";
			System.out.println("importing this signMammaprint xml for subjectId:" + importName + "=" + xml);
			String jobId = importData(accessToken, xml, importName);
		} else
			System.out.print("Import for signMamma print not required for participant=" + subjectId);

	}

	public static void migrateData() {
		OCUtil ocUtil = new OCUtil();
		String fileName = ocUtil.getAppProperties().getProperty("DA_XML");
		HashMap<String, String> qlhcToqlhc2 = parseCSVFile();

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setValidating(false);
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();
			Document doc = db.parse(new FileInputStream(new File(fileName)));

			NodeList entries = doc.getElementsByTagName("*");
			String attrName = "";
			String attrValue = "";

			if (entries.getLength() > 0) { // if there are catelog items
				for (int itemIndex = 0; itemIndex < entries.getLength(); itemIndex++) {
					Node node = entries.item(itemIndex);

					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) node;
						if (eElement.hasAttributes()) {
							NamedNodeMap attrs = eElement.getAttributes();
							for (int i = 0; i < attrs.getLength(); i++) {
								attrName = ((Attr) attrs.item(i)).getName();
								attrValue = ((Attr) attrs.item(i)).getValue();
								if (qlhcToqlhc2.containsKey(attrValue))
									eElement.setAttribute(attrName, qlhcToqlhc2.get(attrValue));
							}
						}
					}
				}
			}

			DOMSource source = new DOMSource(doc);
			String newFileName = ocUtil.getAppProperties().getProperty("DA_UPDATED_XML");
			FileWriter writer = new FileWriter(new File(newFileName));
			StreamResult result = new StreamResult(writer);

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.transform(source, result);
			System.out.println("Done writing transformed xml");

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static HashMap<String, String> parseCSVFile() {
		HashMap<String, String> qlhc = new HashMap<String, String>();
		HashMap<String, String> qlhc2 = new HashMap<String, String>();
		OCUtil ocUtil = new OCUtil();
		String qlhcDataFile = ocUtil.getAppProperties().getProperty("QLHC_CSV");
		String qlhc2DataFile = ocUtil.getAppProperties().getProperty("QLHC2_CSV");
		try (BufferedReader br = new BufferedReader(new FileReader(qlhcDataFile))) {
			String line;
			line = br.readLine(); // skip first
			while ((line = br.readLine()) != null) {
				String[] values = line.split(",");
				if (!qlhc.containsKey(values[0]))
					qlhc.put(values[0], values[1]);
				if (!qlhc.containsKey(values[2]))
					qlhc.put(values[2], values[3]);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		try (BufferedReader br = new BufferedReader(new FileReader(qlhc2DataFile))) {
			String line;
			line = br.readLine(); // skip first
			while ((line = br.readLine()) != null) {
				String[] values = line.split(",");
				if (!qlhc2.containsKey(values[0]))
					qlhc2.put(values[0], values[1]);
				if (!qlhc2.containsKey(values[2]))
					qlhc2.put(values[2], values[3]);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		HashMap<String, String> qlhcMap = new HashMap<String, String>();
		for (Entry<String, String> entry : qlhc.entrySet()) {
			String qlhcVal = entry.getValue();
			String qlhc2Val = qlhc2.get(entry.getKey());
			if (qlhc2Val == null || !qlhc2Val.equals(qlhcVal)) {
				System.out.println("qlhc:" + qlhcVal + ",qlhc2:" + qlhc2Val);
				qlhcMap.put(qlhcVal, qlhc2Val);
			}

		}
		System.out.println("Done parsing csv");
		return qlhcMap;

	}

	public static void splitXml() {

		try {
			OCUtil ocUtil = new OCUtil();
			String fileName = ocUtil.getAppProperties().getProperty("DA_UPDATED_XML");
			String splitFilesLoc = ocUtil.getAppProperties().getProperty("SPLIT_LOC");

			File xmlFile = new File(fileName);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);

			Node odmNode = doc.getElementsByTagName("ODM").item(0);
			NodeList childNodes = odmNode.getChildNodes();

			System.out.println("study nodes size:" + childNodes.getLength());

			for (int i = 0; i < childNodes.getLength(); ++i) {
				Node studyNode = childNodes.item(i);
				if (studyNode.getNodeType() == Node.ELEMENT_NODE) {
					String nodeName = studyNode.getNodeName();
					if (nodeName.equals("ClinicalData")) {
						String name = ((Element) studyNode).getAttribute("StudyOID");

						Document newXmlDoc = dBuilder.newDocument();
						Node newOdmNode = newXmlDoc.importNode(odmNode, false);

						newXmlDoc.appendChild(newOdmNode);
						Node newStudyNode = newXmlDoc.importNode(studyNode, true);
						newOdmNode.appendChild(newStudyNode);

						TransformerFactory transformerFactory = TransformerFactory.newInstance();
						Transformer transformer = transformerFactory.newTransformer();
						transformer.setOutputProperty(OutputKeys.INDENT, "yes");
						DOMSource source = new DOMSource(newXmlDoc);

						StreamResult result = new StreamResult(new File(splitFilesLoc + name + ".xml"));
						transformer.transform(source, result);
						System.out.println("Done for " + name);
					}
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void readAndImport() {

		OCUtil ocUtil = new OCUtil();
		String splitFilesLoc = ocUtil.getAppProperties().getProperty("SPLIT_LOC");
		String username = ocUtil.getAppProperties().getProperty("username");
		String password = ocUtil.getAppProperties().getProperty("password");
		String accessToken = OCUtil.getAccessToken("qlhc2.build.openclinica.io", username, password);

		File directoryPath = new File(splitFilesLoc);
		File filesList[] = directoryPath.listFiles();
		System.out.println("List of files and directories in the specified directory:");
		Scanner sc = null;
		for (File file : filesList) {
			System.out.println("File name: " + file.getName());
			if (file.getName().contains(".xml")) {
				try {
					sc = new Scanner(file);
					String input;
					StringBuffer sb = new StringBuffer();
					while (sc.hasNextLine()) {
						input = sc.nextLine();
						sb.append(input + " ");
					}

					Random random = new Random();
					int x = random.nextInt(900) + 100;
					String importName = x + "_" + file.getName();
					System.out.println("importing :" + importName);

					// String jobId = OCUtil.importData(accessToken, sb.toString(), importName);

					// System.out.println("Import JobId: "+jobId );

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
	}

	public static Node getNodeByAttrValue(Document xmlDoc, String selAttributeStr, String selAttrVal)
			throws XPathExpressionException {
		Node retNode = null;
		XPath xpath = XPathFactory.newInstance().newXPath();
		StringBuilder str = new StringBuilder();
		str.append("//*[contains(@").append(selAttributeStr).append(", '").append(selAttrVal).append("')]");
		String xpathStr = str.toString();
		NodeList nodes = (NodeList) xpath.evaluate(xpathStr, xmlDoc, XPathConstants.NODESET);

		// System.out.println(nodes.getLength());
		if (nodes.getLength() == 1)
			retNode = nodes.item(0);

		return retNode;

	}

	public static Node getSubjectNode(Document xmlDoc) throws XPathExpressionException {
		Node retNode = null;
		XPath xpath = XPathFactory.newInstance().newXPath();
		NodeList nodes = (NodeList) xpath.evaluate("//SubjectData[@StudySubjectID]", xmlDoc, XPathConstants.NODESET);

		if (nodes.getLength() == 1)
			retNode = nodes.item(0);
		return retNode;

	}

	public static Element createStudyEventData(Document doc, String seOID, String eventName) {
		DateFormat startDf = new SimpleDateFormat("yyyy-MM-dd");
		Element se = doc.createElement("StudyEventData");
		se.setAttribute("StudyEventOID", seOID);
		se.setAttribute("OpenClinica:EventName", eventName);
		se.setAttribute("OpenClinica:StartDate", startDf.format(new Date()));
		se.setAttribute("OpenClinica:Status", "data entry started");
		se.setAttribute("OpenClinica:WorkflowStatus", "data entry started");
		se.setAttribute("StudyEventRepeatKey", "1");

		return se;
	}

	public static void migrateSubsequentSugData() {
		OCUtil ocUtil = new OCUtil();
		String fileName = ocUtil.getAppProperties().getProperty("SS_XML");

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setValidating(false);
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();
			Document doc = db.parse(new FileInputStream(new File(fileName)));

			Node subseSurgNode = getNodeByAttrValue(doc, "FormName", "Subsequent Surgeries");
			if (subseSurgNode != null) {
				// remove node from StudyEventData StudyEventOID="SE_FOLLOWUP"
				subseSurgNode.getParentNode().removeChild(subseSurgNode);

				// add to StudyEventData StudyEventOID="SE_ASNEEDED"
				// find OpenClinica:EventName="As Needed
				Node asNeeded = getNodeByAttrValue(doc, "EventName", "As Needed");
				if (asNeeded != null) {
					// Node newsubSurg = doc.importNode(subseSurgNode, true);
					// asNeeded.appendChild(newsubSurg);

					// remove any other F_SUBSEQUENTSU
					NodeList childNodes = asNeeded.getChildNodes();

					for (int i = 0; i < childNodes.getLength(); ++i) {
						Node childNode = childNodes.item(i);
						if (childNode.getNodeType() == Node.ELEMENT_NODE) {
							String nodeName = childNode.getNodeName();

							String subseVal = null;
							try {
								subseVal = childNode.getAttributes().getNamedItem("FormOID").getNodeValue();
							} catch (Exception e) {
								// ignore
							}
							if (nodeName.equals("FormRef") && subseVal != null && subseVal.equals("F_SUBSEQUENTSU")) {
								childNode.getParentNode().removeChild(childNode);
								break;
							}
						}
					}

					asNeeded.appendChild(subseSurgNode);
				} else {
					Node subjectNode = getSubjectNode(doc);
					Element seEl = createStudyEventData(doc, "SE_ASNEEDED", "As Needed");
					seEl.appendChild(subseSurgNode);
					subjectNode.appendChild(seEl);
				}
			}

			DOMSource source = new DOMSource(doc);
			String newFileName = ocUtil.getAppProperties().getProperty("SS_UPDATED_XML");
			FileWriter writer = new FileWriter(new File(newFileName));
			StreamResult result = new StreamResult(writer);

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.transform(source, result);
			System.out.println("Done writing transformed xml");

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public String getSessionToken(String hostname, String username, String password) {
		String url = String.format("https://%s/api/session", hostname);
		String output = postData(username, password, url);
		JSONObject json = new JSONObject(output);  
		String token = json.getString("id");
		return token;

	}

	public JSONArray queryData(String queryName, String username, String password, String hostname) throws Exception {
		String url = String.format("https://qlhc2.insight.openclinica.io/api/card/%s/query/json", queryName);
		String sessionkey = getSessionToken(hostname, username, password);

		HttpPost post = new HttpPost(url);
		post.addHeader("X-Metabase-Session", sessionkey);
		CloseableHttpClient client = HttpClients.createDefault();
		CloseableHttpResponse response = client.execute(post);
//client.close();
		//System.out.println(response.getStatusLine().getStatusCode());

		StringBuilder sb = new StringBuilder();
		if (response.getStatusLine().getStatusCode() == 200) {
			Reader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
			for (int c; (c = reader.read()) >= 0;) {
				sb.append((char) c);
			}

		}
		String jsonStr = sb.toString();
		

		return new JSONArray(jsonStr);
	}
	
	List<String> getStrListFromJsonArray(JSONArray jsonArray, String name){
		List<String> list = new ArrayList<String>();
		for(int i = 0; i < jsonArray.length(); i++){
		    list.add(jsonArray.getJSONObject(i).getString(name));
		}
		return list;
	}

	public static String postData(String username, String password, String urlString) {
		try {
//System.out.println("getAccessToken begin");

			JSONObject requestJSON = new JSONObject();
			requestJSON.put("username", username);
			requestJSON.put("password", password);
			byte[] requestBytes = requestJSON.toString().getBytes("UTF-8");

//System.out.println(requestJSON.toString());

			URL url = new URL(urlString);
//System.out.println(url.toString());
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("Content-Length", String.valueOf(requestBytes.length));
			connection.setRequestProperty("Accept", "*/*");
			connection.setDoOutput(true);

			connection.getOutputStream().write(requestBytes);

			if (connection.getResponseCode() == 200) {
				Reader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				for (int c; (c = reader.read()) >= 0;) {
					sb.append((char) c);
				}

				return sb.toString();
			} else {
				Reader reader = new BufferedReader(new InputStreamReader(connection.getErrorStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				for (int c; (c = reader.read()) >= 0;) {
					sb.append((char) c);
				}

				System.out.println("ERROR: bad response code " + connection.getResponseCode() + ", " + sb.toString());
			}
		} catch (Exception ex) {
			System.out.println("ERROR: " + ex.getMessage());
		}

		return null;
	}

}
