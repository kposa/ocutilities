package com.quantumleaphealth.oc.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@Component
public class REligibilityUtil implements EnvironmentAware {
	
	@Autowired
	public static  Environment env;
	
	public static  String getConfigProp(String key) {
        return env.getProperty(key);
    }
    @Override
    public void setEnvironment(Environment arg0) {
        if(env==null){
        	env=arg0;
        }
    }
    
	public static boolean isEligible(String username, String password, String subjectId) throws Exception {
		String accessToken = OCUtil.getAccessToken("qlhc2.build.openclinica.io", username, password); 
		String urlODM = String.format("https://qlhc2.openclinica.io/OpenClinica/pages/auth/api/clinicaldata/S_ISPY(TEST)/SS_%s/*/*?includeAudits=n&includeDNs=n&includeMetadata=n&showArchived=n",
		      subjectId);

		Document documentODM = OCUtil.getXML(urlODM, accessToken);
		
		
		//EOP eligible and Main not 
		//ArrayList<String> eopEligibleList1 = (ArrayList<String>) Arrays.asList("Low risk", "Negative", "No","Positive");
		//ArrayList<String> eopEligibleList2 = (ArrayList<String>) Arrays.asList("Low risk", "Negative", "Yes","Positive");
		List<String> eopEligibleList3 = Arrays.asList("High risk", "Negative", "Yes","Positive");		
		updateEOPEligibilityCriteria(documentODM,eopEligibleList3 );
		
		//EOP NOT eligible and Main eligible 
		/*
		ArrayList<String> nonEopEligibleList1 = (ArrayList<String>) Arrays.asList("Low risk", "Negative", "Yes","Negative");
		ArrayList<String> nonEopEligibleList2 = (ArrayList<String>) Arrays.asList("Low risk", "Negative", "Null","Negative");
		ArrayList<String> nonEopEligibleList3 = (ArrayList<String>) Arrays.asList("Low risk", "Positive", "Null","Positive");
		ArrayList<String> nonEopEligibleList4 = (ArrayList<String>) Arrays.asList("Low risk", "Positive", "Null","Positive");
		ArrayList<String> nonEopEligibleList5 = (ArrayList<String>) Arrays.asList("High risk", "Negative", "Yes","Negative");
		ArrayList<String> nonEopEligibleList6 = (ArrayList<String>) Arrays.asList("High risk", "Negative", "Yes","Positive");
		ArrayList<String> nonEopEligibleList7 = (ArrayList<String>) Arrays.asList("High risk", "Negative", "Null","Negative");
		ArrayList<String> nonEopEligibleList8 = (ArrayList<String>) Arrays.asList("High risk", "Negative", "Null","Positive");
		ArrayList<String> nonEopEligibleList9 = (ArrayList<String>) Arrays.asList("High risk", "Positive", "Null","Negative");
		ArrayList<String> nonEopEligibleList10 = (ArrayList<String>) Arrays.asList("High risk", "Positive", "Null","Positive");
		
		updateEOPEligibilityCriteria(documentODM,nonEopEligibleList1);
		*/
		
		String xml = OCUtil.convertDocToString(documentODM);
		System.out.println(xml);
		OCUtil.importData(accessToken, xml,"");
		return true;
		
	}

	//ArrayList (mp, her2Overall, her2Eligible, hrOverall)
	public static void updateEOPEligibilityCriteria(Document documentODM, List<String> list ) throws Exception {		

		//String mpVal = OCUtil.getNodeAttrValue(documentODM,"ItemOID", "I_MAMMA_MAMMAPRINT_RISK", "Value");
		//OCUtil.setNodeAttrValue(documentODM, "OpenClinica:ItemName", "MAMMAPRINT_RISK", "Value", list.get(0));
		OCUtil.setNodeAttrValue(documentODM, "ItemOID", "I_MAMMA_MAMMAPRINT_RISK", "Value", list.get(0));
		
		OCUtil.setNodeAttrValue(documentODM, "ItemOID", "I_ONSTU_OVERALL_HER2_STATUS_CALC", "Value", list.get(1));

		OCUtil.setNodeAttrValue(documentODM, "ItemOID", "I_ONSTU_PATIENT_ELIGIBLE_TO_CONT_SC", "Value", list.get(2));
		OCUtil.setNodeAttrValue(documentODM, "ItemOID", "I_ONSTU_PATIENT_ELIGIBLE_TO_CONT_SCREEN_CALC", "Value", list.get(2));
		OCUtil.setNodeAttrValue(documentODM, "ItemOID", "I_ONSTU_PATIENT_ELIGIBLE_TO_CONT_SCREEN_YES_CALC", "Value", list.get(2));		

		OCUtil.setNodeAttrValue(documentODM, "ItemOID", "I_ONSTU_OVERALL_HR_STAT", "Value", list.get(3));		
	}
	

	

}
