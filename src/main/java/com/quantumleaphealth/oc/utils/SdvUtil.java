package com.quantumleaphealth.oc.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.Normalizer.Form;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.quantumleaphealth.oc.db.DBCPDataSource;
import com.quantumleaphealth.oc.model.FormData;
import com.quantumleaphealth.oc.model.ItemData;
import com.quantumleaphealth.oc.model.ItemGroupData;
import com.quantumleaphealth.oc.model.JaxbUtil;
import com.quantumleaphealth.oc.model.Odm;
import com.quantumleaphealth.oc.model.SubjectData;

import org.apache.commons.lang3.text.StrSubstitutor;

@Component
public class SdvUtil implements EnvironmentAware {

	@Autowired
	public static Environment env;

	public static String getConfigProp(String key) {
		return env.getProperty(key);
	}

	@Override
	public void setEnvironment(Environment arg0) {
		if (env == null) {
			env = arg0;
		}

	}

	public static void updateSDV(String username, String password, String subjectId) throws Exception {
		String accessToken = OCUtil.getAccessToken("qlhc2.build.openclinica.io", username, password); // username/password
																										// need to be
																										// for OC data
																										// manager,
																										// vandhana may
																										// already have
																										// them in a
																										// properties
																										// file
		String url = getConfigProp("odmUrlWithSubjectId");
		String urlODM = String.format(url, subjectId);

		Document documentODM = OCUtil.getXML(urlODM, accessToken);
		OCUtil.addUpdateFormAttrValue(documentODM, "OpenClinica:Status", "OpenClinica:Status", "OpenClinica:SdvStatus",
				"Verified");

		String xml = OCUtil.convertDocToString(documentODM);
		Random random = new Random();
		int x = random.nextInt(900) + 100;
		String importName = subjectId + "_" + x + ".xml";
		System.out.println("importing this xml for subjectId:" + importName);

		String jobId = OCUtil.importData(accessToken, xml, importName);

	}

	public static void clearRadomizationForms(List<String> subjectKeyList) {
		OCUtil util = new OCUtil();
		String username = util.getAppProperties().getProperty("username");
		String password = util.getAppProperties().getProperty("password");
		String subId = null;
		for (String key : subjectKeyList) {
			subId = SubjectData.getSubjectIdForKey(key);
			System.out.println("key:" + key + ", subjectId:" + subId);

			String accessToken = OCUtil.getAccessToken("qlhc2.build.openclinica.io", username, password); // username/password
																											// need to
																											// be for OC
																											// data
																											// manager,
																											// vandhana
																											// may
																											// already
																											// have them
																											// in a
																											// properties
																											// file
			String url = util.getAppProperties().getProperty("odmUrlWithSubjectId");
			String urlODM = String.format(url, subId);

			Odm odmData = null;
			try {
				Document documentODM = OCUtil.getXML(urlODM, accessToken);
				String xml = OCUtil.convertDocToString(documentODM);
				odmData = Odm.loadFromXml(xml);
				FormData randForm = odmData.getFormData("F_RANDOMIZATIO");
				boolean upload = false;
				if (randForm != null) {
					randForm.clearFormData();
					upload = true;
				}

				FormData randResultsForm = odmData.getFormData("F_RANDOMIZATIO_5299");
				if (randResultsForm != null) {
					randResultsForm.clearFormData();
					upload = true;
				}

				if (upload == true) {
					String updatedXml = odmData.toString();

					Random random = new Random();
					int x = random.nextInt(900) + 100;
					String importName = subId + "_" + x + ".xml";
					System.out.println("importing this xml for subjectId:" + importName);

					String jobId = OCUtil.importData(accessToken, updatedXml, importName);
				} else
					System.out.println("No changes made, nothing to import");
			} catch (Exception e) {

				e.printStackTrace();
			}

		}

	}

	public static String checkStatus(String subKey) {

		Odm odmData = Odm.getOdmForSubjectKey(subKey);
		FormData disAss = odmData.getFormData("F_DISEASEASSES");
		FormData ptp = odmData.getFormData("F_ONSTUDYPATHO");
		FormData ose = odmData.getFormData("F_ONSTUDYELIGI");
		FormData mriVol = odmData.getFormData("F_MRIVOLUME");
		FormData patReg = odmData.getFormData("F_PATIENTDEMOG");
		FormData tissueSpe = odmData.getFormData("F_TISSUEPRE");
		FormData mp = odmData.getFormData("F_MAMMAPRINT");
		StringBuffer buf = new StringBuffer();
		if (disAss == null || !disAss.getWorkflowStatus().equals("data entry complete"))
			buf.append("Disease Assessment Pre-Treatment: Not complete").append("\n");
		if (ptp == null || !ptp.getWorkflowStatus().equals("data entry complete"))
			buf.append("Pre-Treatment Pathology: Not complete").append("\n");
		if (ose == null || !ose.getWorkflowStatus().equals("data entry complete"))
			buf.append("On-Study Eligibility: Not complete").append("\n");
		if (mriVol == null || !mriVol.getWorkflowStatus().equals("data entry complete"))
			buf.append("MRI Volume: Not complete").append("\n");
		if (patReg == null || !patReg.getWorkflowStatus().equals("data entry complete"))
			buf.append("Patient Demographics/Registration: Not complete").append("\n");
		if (tissueSpe == null || !tissueSpe.getWorkflowStatus().equals("data entry complete"))
			buf.append("Tissue Specimen Pre-Treatment (research): Not complete").append("\n");
		if (mp == null || !mp.getWorkflowStatus().equals("data entry complete"))
			buf.append("MammaPrint (Agendia): Not complete").append("\n");

		return buf.toString();

	}

	public static String checkRandomization(String subKey) {
		StringBuffer buf = new StringBuffer();
		Odm odmData = Odm.getOdmForSubjectKey(subKey);
		FormData randomization = odmData.getFormData("F_RANDOMIZATIO");
		if (randomization != null) {
			ItemGroupData itemData = randomization.getItemGroupDataList().get(0);
			if (itemData != null) {
				List<ItemData> itemList = itemData.getItemDataList();
				for (ItemData item : itemList) {
					if (!item.getValue().equals("Yes")) {
						buf.append(item.getItemName()).append(":").append(item.getValue());
					}
				}

			}
		} else
			buf.append("No Randomization form??");

		return buf.toString();

	}

	private static ArrayList<String> getSubKeyList() {
		OCUtil util = new OCUtil();

		String subListStr = util.getAppProperties().getProperty("checkSubList");

		String[] parts = subListStr.split("-");
		String first = parts[0];
		String last = parts[1];
		String nextSub = null;

		ArrayList<String> subList = new ArrayList<String>();
		subList.add(first);
		while (true) {
			nextSub = "EAR000" + (Integer.parseInt(first.substring(6, first.length())) + 1);
			subList.add(nextSub);
			first = nextSub;
			if (nextSub.equals(last))
				break;
		}

		return subList;

	}

	public static void checkStatuses() {

		ArrayList<String> subList = getSubKeyList();
		String errStr = "";
		for (String subkey : subList) {
			System.out.println("For subject:" + subkey);
			errStr = SdvUtil.checkStatus(subkey);
			System.out.println("Error str:" + errStr);
			System.out.println("--------------");
		}

		System.out.print(errStr);

	}

	public static void checkRandomization() {
		ArrayList<String> subList = getSubKeyList();
		String errStr = null;
		for (String subkey : subList) {
			System.out.println("For subject:" + subkey);
			errStr = SdvUtil.checkRandomization(subkey);
			System.out.println("Error str:" + errStr);
			System.out.println("--------------");
		}

		System.out.print(errStr);

	}

	public static void closeQuery(List<String> subjectKeyList) {
		OCUtil util = new OCUtil();
		String username = util.getAppProperties().getProperty("username");
		String password = util.getAppProperties().getProperty("password");
		String subId = null;
		for (String key : subjectKeyList) {
			subId = SubjectData.getSubjectIdForKey(key);
			System.out.println("key:" + key + ", subjectId:" + subId);

			String accessToken = OCUtil.getAccessToken("qlhc2.build.openclinica.io", username, password); // username/password
																											// need to
																											// be for OC
																											// data
																											// manager,
																											// vandhana
																											// may
																											// already
																											// have them
																											// in a
																											// properties
																											// file
			String url = util.getAppProperties().getProperty("odmUrlWithSubjectId");
			String urlODM = String.format(url, subId);

			boolean importNeeded = false;
			try {
				Document documentODM = OCUtil.getXML(urlODM, accessToken);
				String xml = OCUtil.convertDocToString(documentODM);
				// odmData = Odm.loadFromXml(xml);
				// System.out.println(xml);

				XPath xpath = XPathFactory.newInstance().newXPath();
				NodeList nodes = (NodeList) xpath.evaluate(
						"//ItemData[@ItemOID='I_FOLLO_FILE']/DiscrepancyNotes/DiscrepancyNote[@Status='New']",
						documentODM, XPathConstants.NODESET);

				Node subjectNode = OCUtil.getSubjectNode(documentODM);
				Node clinicalData = subjectNode.getParentNode();
				Node odm = clinicalData.getParentNode();

				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				factory.setNamespaceAware(true);
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document newDoc = builder.newDocument();

				Node odmNew = newDoc.importNode(odm, false);
				newDoc.appendChild(odmNew);

				Node clinDataNew = newDoc.importNode(clinicalData, false);
				odmNew.appendChild(clinDataNew);

				Node subNew = newDoc.importNode(subjectNode, false);
				clinDataNew.appendChild(subNew);

				if (nodes.getLength() > 0) {
					System.out.println(nodes.getLength());

					Node disNoteNode = nodes.item(0);

					Node newNotes = newDoc.importNode(disNoteNode.getParentNode(), false);
					Node newItemData = newDoc.importNode(disNoteNode.getParentNode().getParentNode(), false);
					Node newItemGrpData = newDoc.importNode(disNoteNode.getParentNode().getParentNode().getParentNode(),
							false);
					Node newFormData = newDoc.importNode(
							disNoteNode.getParentNode().getParentNode().getParentNode().getParentNode(), false);
					Node newStudyEventData = newDoc.importNode(
							disNoteNode.getParentNode().getParentNode().getParentNode().getParentNode().getParentNode(),
							false);

					/*
					 * Element newNotes = newDoc.createElement("DiscrepancyNotes"); Node disNotesOid
					 * = disNoteNode.getParentNode().getAttributes().getNamedItem("EntityID");
					 * newNotes.setAttribute("EntityID", disNotesOid.getNodeValue());
					 * 
					 * Element newItemData = newDoc.createElement("ItemData"); Node itemOid =
					 * disNoteNode.getParentNode().getParentNode().getAttributes().getNamedItem(
					 * "ItemOID"); newItemData.setAttribute("ItemOID", itemOid.getNodeValue());
					 * 
					 * Element newItemGrpData = newDoc.createElement("ItemGroupData"); Node
					 * itemGroupOid =
					 * disNoteNode.getParentNode().getParentNode().getParentNode().getAttributes().
					 * getNamedItem("ItemGroupOID"); newItemGrpData.setAttribute("ItemGroupOID",
					 * itemGroupOid.getNodeValue());
					 * 
					 * Element newFormData = newDoc.createElement("FormData"); Node formOid =
					 * disNoteNode.getParentNode().getParentNode().getParentNode().getParentNode().
					 * getAttributes().getNamedItem("FormOID"); newFormData.setAttribute("FormOID",
					 * formOid.getNodeValue());
					 * 
					 * Element newStudyEventData = newDoc.createElement("StudyEventData"); Node
					 * studyEventOid =
					 * disNoteNode.getParentNode().getParentNode().getParentNode().getParentNode().
					 * getParentNode().getAttributes().getNamedItem("StudyEventOID");
					 * newStudyEventData.setAttribute("StudyEventOID",
					 * studyEventOid.getNodeValue());
					 */
					subNew.appendChild(newStudyEventData);
					newStudyEventData.appendChild(newFormData);
					newFormData.appendChild(newItemGrpData);
					newItemGrpData.appendChild(newItemData);
					newItemData.appendChild(newNotes);

					importNeeded = true;

					for (int i = 0; i < nodes.getLength(); ++i) {
						Node disNoteNodeItm = nodes.item(i);
						disNoteNodeItm.getAttributes().getNamedItem("Status").setNodeValue("Closed");

						NodeList childNotes = (NodeList) xpath.evaluate("./ChildNote[@Status='New']", disNoteNodeItm,
								XPathConstants.NODESET);
						if (childNotes.getLength() == 1) {

							Node newChildNote = createChildNote(documentODM);
							String num = disNoteNodeItm.getAttributes().getNamedItem("NumberOfChildNotes")
									.getNodeValue();
							int numInt = Integer.parseInt(num);
							disNoteNodeItm.getAttributes().getNamedItem("NumberOfChildNotes")
									.setNodeValue(String.valueOf(numInt + 1));
							disNoteNodeItm.appendChild(newChildNote);

							Node newDisNoteNodeitm = newDoc.importNode(disNoteNodeItm, true);
							newNotes.appendChild(newDisNoteNodeitm);
						}

					}

				}
				String updatedXml = OCUtil.convertDocToString(newDoc);
				System.out.println(updatedXml);

				// importNeeded = false;

				if (importNeeded == true) {

					Random random = new Random();
					int x = random.nextInt(900) + 100;
					String importName = subId + "_" + x + ".xml";
					System.out.println("importing this xml for subjectId:" + importName);

					String jobId = OCUtil.importData(accessToken, updatedXml, importName);
				} else
					System.out.println("No changes made, nothing to import");
			} catch (Exception e) {

				e.printStackTrace();
			}

		}

	}

	private static Element createItemData(Document documentODM, String itemOid, String itemName, String Value) {
		Element itemData = documentODM.createElement("ItemData");
		itemData.setAttribute("ItemOID", itemOid);
		itemData.setAttribute("OpenClinica:ItemName", itemName);
		itemData.setAttribute("Value", Value);

		return itemData;

	}

	private static Element createItemGroupData(Document documentODM, String itemGroupOid, String itemGroupName) {
		Element itemGroupData = documentODM.createElement("ItemGroupData");
		itemGroupData.setAttribute("ItemGroupOID", itemGroupOid);
		itemGroupData.setAttribute("OpenClinica:ItemGroupName", itemGroupName);
		itemGroupData.setAttribute("ItemGroupRepeatKey", "1");
		itemGroupData.setAttribute("TransactionType", "Insert");

		return itemGroupData;

	}

	private static Element createDiscrepancyNote(Document documentODM, String entityId, String status, String noteType,
			String childNoteStatus, String userName, String detailedNote) {

		Element dNote = documentODM.createElement("OpenClinica:DiscrepancyNote");
		dNote.setAttribute("ID", "");
		dNote.setAttribute("Status", status);
		dNote.setAttribute("NoteType", noteType);

		Element childNote = documentODM.createElement("OpenClinica:ChildNote");
		childNote.setAttribute("ID", "");
		childNote.setAttribute("Status", childNoteStatus);
		childNote.setAttribute("UserName", userName);
		dNote.appendChild(childNote);

		Element detailedNoteNode = documentODM.createElement("OpenClinica:DetailedNote");
		detailedNoteNode.setTextContent(detailedNote);

		childNote.appendChild(detailedNoteNode);

		return dNote;
	}

	private static Element createStudyEventForMri(Document documentODM, String studyEventOid, String studyEventName,
			String reason, String startDate) {
		Element studyEvent = documentODM.createElement("StudyEventData");
		studyEvent.setAttribute("StudyEventOID", studyEventOid);
		studyEvent.setAttribute("OpenClinica:EventName", studyEventName);
		if (startDate == null || "".equals(startDate))
			studyEvent.setAttribute("OpenClinica:StartDate", "9999-09-09");
		else
			studyEvent.setAttribute("OpenClinica:StartDate", startDate);

		Element formData = documentODM.createElement("FormData");
		formData.setAttribute("FormOID", "F_MRIVOLUME");
		formData.setAttribute("OpenClinica:FormName", "MRI Volume");
		formData.setAttribute("OpenClinica:FormLayoutOID", "3");
		formData.setAttribute("OpenClinica:Status", "data entry complete");
		formData.setAttribute("OpenClinica:WorkflowStatus", "data entry complete");
		formData.setAttribute("OpenClinica:SdvStatus", "Verified");
		formData.setAttribute("OpenClinica:Required", "No");
		formData.setAttribute("OpenClinica:Visible", "Yes");
		formData.setAttribute("OpenClinica:Editable", "Yes");

		Node itemGroupNode = createItemGroupData(documentODM, "IG_MRIVO_S_MRI_VOLUME", "S_MRI_VOLUME");
		formData.appendChild(itemGroupNode);

		Node a = createItemData(documentODM, "I_MRIVO_EVENT_CONVERSION", "EVENT_CONVERSION", studyEventName);
		Node b = createItemData(documentODM, "I_MRIVO_MRI_COMPLETED", "EVENT_CONVERSION", "N");
		Node c = createItemData(documentODM, "I_MRIVO_REASON_NC", "REASON_NC", "Other");
		Node d = createItemData(documentODM, "I_MRIVO_REASON_NC_OTH", "REASON_NC_OTH", reason);

		itemGroupNode.appendChild(a);
		itemGroupNode.appendChild(b);
		itemGroupNode.appendChild(c);
		itemGroupNode.appendChild(d);

		studyEvent.appendChild(formData);

		return studyEvent;

	}

	private static Element createStudyEventForPSS(Document documentODM, String studyEventOid, String reason,
			String startDate) {
		Element studyEvent = documentODM.createElement("StudyEventData");
		studyEvent.setAttribute("StudyEventOID", studyEventOid);
		// studyEvent.setAttribute("OpenClinica:EventName", studyEventName);
		if (startDate == null || "".equals(startDate))
			studyEvent.setAttribute("OpenClinica:StartDate", "9999-09-09");
		else
			studyEvent.setAttribute("OpenClinica:StartDate", startDate);

		Element formData = documentODM.createElement("FormData");
		formData.setAttribute("FormOID", "F_POSTSURGERYS");
		// formData.setAttribute("OpenClinica:FormName", "MRI Volume");
		formData.setAttribute("OpenClinica:FormLayoutOID", "4.3");
		formData.setAttribute("OpenClinica:Status", "data entry complete");
		// formData.setAttribute("OpenClinica:WorkflowStatus", "data entry complete");
		formData.setAttribute("OpenClinica:SdvStatus", "Verified");
		formData.setAttribute("OpenClinica:Required", "No");
		formData.setAttribute("OpenClinica:Visible", "Yes");
		formData.setAttribute("OpenClinica:Editable", "Yes");

		Node itemGroupNode = createItemGroupData(documentODM, "IG_POSTS_S_PSS_PROCEDURE", "S_PSS_PROCEDURE");
		formData.appendChild(itemGroupNode);

		Node a = createItemData(documentODM, "I_POSTS_HAD_SURGERY", "had_surgery", "N");
		Node b = createItemData(documentODM, "I_POSTS_NO_SURGERY_REASON", "no_surgery_reason", reason);

		itemGroupNode.appendChild(a);
		itemGroupNode.appendChild(b);

		studyEvent.appendChild(formData);

		return studyEvent;

	}

	private static Element createChildNote(Document documentODM) {
		DateFormat startDf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
		Element newChildNote = documentODM.createElement("OpenClinica:ChildNote");
		newChildNote.setAttribute("Status", "Closed");
		newChildNote.setAttribute("Name", "Krishna Posa");
		newChildNote.setAttribute("DateCreated", startDf.format(new Date()));
		newChildNote.setAttribute("UserName", "k.posa");
		Random rnd = new Random();
		newChildNote.setAttribute("ID", "CDN_" + String.format("%09d", rnd.nextInt(1000000000)));

		Element newChildDetNote = documentODM.createElement("OpenClinica:DetailedNote");
		newChildDetNote.setTextContent("Programmatically Resolved");
		newChildNote.appendChild(newChildDetNote);

		Element newChildUserRef = documentODM.createElement("UserRef");
		newChildUserRef.setAttribute("FullName", "Krishna Posa");
		newChildUserRef.setAttribute("UserName", "k.posa");
		newChildUserRef.setAttribute("UserOID", "USR_281");
		newChildNote.appendChild(newChildUserRef);

		return newChildNote;

	}

	static class AEEntry {
		Date visitDate = null;
		String visitDateStr = "";
		String status = "";
		String sdv = "";

		AEEntry(Node aeNode) throws Exception {
			XPath xpath = XPathFactory.newInstance().newXPath();

			try {
				NodeList vDtNodeList = (NodeList) xpath.evaluate(
						"./ItemGroupData[@ItemGroupOID='IG_AE_S_ADVERSE_EVENT']/ItemData[@ItemOID='I_AE_VISIT_DATE']",
						aeNode, XPathConstants.NODESET);
				visitDateStr = vDtNodeList.item(0).getAttributes().getNamedItem("Value") != null
						? vDtNodeList.item(0).getAttributes().getNamedItem("Value").getNodeValue()
						: "";
				if (!"".equals(visitDateStr))
					visitDate = new SimpleDateFormat("yyyy-mm-dd").parse(visitDateStr);
			} catch (Exception e) {
				e.printStackTrace();
			}

			status = (aeNode.getAttributes().getNamedItem("OpenClinica:Status") != null)
					? aeNode.getAttributes().getNamedItem("OpenClinica:Status").getNodeValue()
					: "";
			sdv = (aeNode.getAttributes().getNamedItem("OpenClinica:SdvStatus") != null)
					? aeNode.getAttributes().getNamedItem("OpenClinica:SdvStatus").getNodeValue()
					: "";

			System.out.println("visit date: " + visitDateStr + ", status:" + status + ", sdv:" + sdv);

		}
	}

	public static void updateSDVForAEAttachment(List<String> subjectKeyList) {
		OCUtil util = new OCUtil();
		String username = util.getAppProperties().getProperty("username");
		String password = util.getAppProperties().getProperty("password");
		String subId = null;
		String padSubId = null;

		for (String key : subjectKeyList) {
			padSubId = String.format("%05d", Integer.parseInt(key));
			subId = SubjectData.getSubjectIdForKey(padSubId);
			System.out.println("key:" + padSubId + ", subjectId:" + subId);

			String stmt1 = "UPDATE aeAttach SET status='P', createddate = now(), results='Complete' WHERE   subjectid = ?  ";
			updateDB(stmt1, key, null);

			if (subId != null) {

				String accessToken = OCUtil.getAccessToken("qlhc2.build.openclinica.io", username, password); // username/password
																												// need
																												// to be
																												// for
																												// OC
																												// data
																												// manager,
																												// vandhana
																												// may
																												// already
																												// have
																												// them
																												// in a
																												// properties
																												// file
				String url = util.getAppProperties().getProperty("odmUrlWithSubjectId");
				String urlODM = String.format(url, subId);

				boolean importNeeded = false;
				try {
					Document documentODM = OCUtil.getXML(urlODM, accessToken);

					XPath xpath = XPathFactory.newInstance().newXPath();

					NodeList aeAttachNodes = (NodeList) xpath.evaluate("//FormData[@FormOID='F_AE_SOURCE']",
							documentODM, XPathConstants.NODESET);

					NodeList aeNodes = (NodeList) xpath.evaluate("//FormData[@FormOID='F_AE']", documentODM,
							XPathConstants.NODESET);
					List<AEEntry> aeList = new ArrayList<AEEntry>();
					if (aeNodes.getLength() > 0) {
						System.out.println(aeNodes.getLength());
						AEEntry aeEntry = null;

						for (int i = 0; i < aeNodes.getLength(); ++i) {
							Node aeNode = aeNodes.item(i);
							aeEntry = new AEEntry(aeNode);

							aeList.add(aeEntry);
						}
					}

					if (aeAttachNodes.getLength() > 0) {
						System.out.println(aeAttachNodes.getLength());
						boolean check = false;
						boolean alreadyGood = false;

						for (int i = 0; i < aeAttachNodes.getLength(); ++i) {
							Node aeAttachNode = aeAttachNodes.item(i);
							alreadyGood = checkAlreadyGood(aeAttachNode);
							if (!alreadyGood) {
								check = checkValuesWithAENodes(aeAttachNode, aeList);
								if (check) {

									Document newDoc = createNewDocForAEAttach(documentODM, aeAttachNode);
									String updatedXml = OCUtil.convertDocToString(newDoc);
									System.out.println(updatedXml);
									importNeeded = true;
									if (importNeeded == true) {

										Random random = new Random();
										int x = random.nextInt(900) + 100;
										String importName = subId + "_" + x + ".xml";
										System.out.println("importing this xml for subjectId:" + importName);

										String jobId = OCUtil.importData(accessToken, updatedXml, importName);

										String stmt = "UPDATE aeAttach SET status='C', createddate = now(), results='Complete' WHERE   subjectid = ?  ";
										updateDB(stmt, key, null);

										System.out.println("Completing for:" + padSubId);
									} else
										System.out.println("No changes made, nothing to import");

								} else {
									System.out.println("Check with AE failed:" + padSubId);
									String stmt = "UPDATE aeAttach SET status='C', createddate = now(), results='Check with AE failed' WHERE   subjectid = ?  ";
									updateDB(stmt, key, null);
								}

							} // NOT alreadyGood
							else {
								System.out.println("Already good for:" + padSubId);
								String stmt = "UPDATE aeAttach SET status='C', createddate = now(), results='Already Good' WHERE   subjectid = ?  ";
								updateDB(stmt, key, null);
							}

						} // for
					}
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("Error for:" + padSubId);
					String stmt = "UPDATE aeAttach SET status='E', createddate = now(), results='Error' WHERE   subjectid = ?  ";
					updateDB(stmt, key, null);
				}

			} // if
			else {
				System.out.println("Subjectid not avaialable for:" + padSubId);
				String stmt = "UPDATE aeAttach SET status='E', createddate = now(), results='subjectID not available' WHERE   subjectid = ?  ";
				updateDB(stmt, key, null);
			}

		} // for

	}

	private static boolean checkAlreadyGood(Node aeAttachNode) {
		boolean allGood = false;
		Node studyEvent = aeAttachNode.getParentNode();
		String status = studyEvent.getAttributes().getNamedItem("OpenClinica:Status") != null
				? studyEvent.getAttributes().getNamedItem("OpenClinica:Status").getNodeValue()
				: "";
		String sdvSatus = aeAttachNode.getAttributes().getNamedItem("OpenClinica:SdvStatus") != null
				? aeAttachNode.getAttributes().getNamedItem("OpenClinica:SdvStatus").getNodeValue()
				: "";
		if ("Verified".equals(sdvSatus) && "completed".equals(status))
			allGood = true;

		return allGood;
	}

	static void updateDB(String sqlStr, String pid, String seRepeat) {

		Connection connection = null;
		PreparedStatement statementCRFs = null;

		try {
			connection = DBCPDataSource.getConnection();
			connection.setAutoCommit(false);

			statementCRFs = connection.prepareStatement(sqlStr);
			statementCRFs.setString(1, pid);
			if (seRepeat != null)
				statementCRFs.setString(2, seRepeat);
			statementCRFs.executeUpdate();
			connection.commit();
			System.out.println("Update DB:" + sqlStr + ", for pid:" + pid);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (statementCRFs != null) {
					statementCRFs.close();

				}
				if (null != connection) {
					connection.close();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	static Map<String, String> selectSingleRowDB(String sqlStr, String pid) {

		Connection connection = null;
		PreparedStatement statementCRFs = null;
		Map<String, String> dbMap = new HashMap<String, String>();
		ResultSet rs = null;
		String subjectId, subjectKey;

		try {
			connection = DBCPDataSource.getConnection();
			connection.setAutoCommit(false);

			statementCRFs = connection.prepareStatement(sqlStr);
			statementCRFs.setString(1, pid);

			rs = statementCRFs.executeQuery();

			String studyeventoid, startDate, instance, reason = "";
			Node studyEvent = null;

			while (rs.next()) {
				subjectKey = rs.getString("subjectKey");
				subjectId = rs.getString("subjectId");
				dbMap.put("SUBJECT_ID", subjectId);
				dbMap.put("SUBJECT_KEY", subjectKey);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (statementCRFs != null) {
					statementCRFs.close();

				}
				if (null != connection) {
					connection.close();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return dbMap;
	}

	static Map<String, String> selectSingleRowDBForReconsent(ResultSet rs) {

		Map<String, String> dbMap = new HashMap<String, String>();

		String subjectId, subjectKey, seRepeatKey, siteOid, icfVerDate, irbApprovalProtoDate, irbApprovalIcfDate,
				protoVerDate;

		try {			
				subjectKey = rs.getString("subjectKey");
				subjectId = rs.getString("subjectId");
				seRepeatKey = rs.getString("study_event_repeat_key");
				siteOid = rs.getString("siteoid");
				icfVerDate = rs.getString("icf_ver_date");
				irbApprovalProtoDate = rs.getString("irb_approval_proto_date");
				irbApprovalIcfDate = rs.getString("irb_approval_icf_date");
				protoVerDate = rs.getString("proto_ver_date");

				dbMap.put("subjectId", subjectId);
				dbMap.put("subjectKey", subjectKey);
				dbMap.put("study_event_repeat_key", seRepeatKey);
				dbMap.put("siteoid", siteOid);
				dbMap.put("icf_ver_date", icfVerDate);
				dbMap.put("irb_approval_proto_date", irbApprovalProtoDate);
				dbMap.put("irb_approval_icf_date", irbApprovalIcfDate);
				dbMap.put("proto_ver_date", protoVerDate);			

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return dbMap;
	}

	static void updateImportDB(String sqlStr, String pid, String jobId, String seKey) {

		Connection connection = null;
		PreparedStatement statementCRFs = null;

		try {
			connection = DBCPDataSource.getConnection();
			connection.setAutoCommit(false);

			statementCRFs = connection.prepareStatement(sqlStr);
			statementCRFs.setString(1, jobId);
			statementCRFs.setString(2, pid);
			if (seKey != null)
				statementCRFs.setString(3, seKey);
			statementCRFs.executeUpdate();
			connection.commit();
			System.out.println("updateImportDB :" + sqlStr + ", for pid:" + pid + ", jobId:" + jobId);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (statementCRFs != null) {
					statementCRFs.close();

				}
				if (null != connection) {
					connection.close();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	static void insertDB(String sqlStr) {

		Connection connection = null;
		PreparedStatement statementCRFs = null;

		try {
			connection = DBCPDataSource.getConnection();
			connection.setAutoCommit(false);

			statementCRFs = connection.prepareStatement(sqlStr);
			statementCRFs.execute();
			connection.commit();
			System.out.println("insertDB executed:" + sqlStr);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (statementCRFs != null) {
					statementCRFs.close();

				}
				if (null != connection) {
					connection.close();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	static Document createNewDocForAEAttach(Document documentODM, Node aeAttachNode) throws Exception {
		Node subjectNode = OCUtil.getSubjectNode(documentODM);
		Node clinicalData = subjectNode.getParentNode();
		Node odm = clinicalData.getParentNode();

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document newDoc = builder.newDocument();

		Node odmNew = newDoc.importNode(odm, false);
		newDoc.appendChild(odmNew);

		Node clinDataNew = newDoc.importNode(clinicalData, false);
		odmNew.appendChild(clinDataNew);

		Node subNew = newDoc.importNode(subjectNode, false);
		clinDataNew.appendChild(subNew);

		Node newStudyEventData = newDoc.importNode(aeAttachNode.getParentNode(), false);
		newStudyEventData.getAttributes().getNamedItem("OpenClinica:Status").setNodeValue("completed");
		newStudyEventData.getAttributes().getNamedItem("OpenClinica:WorkflowStatus").setNodeValue("completed");

		subNew.appendChild(newStudyEventData);

		Node newAEAttachForm = newDoc.importNode(aeAttachNode, false);
		Node sdvNode = newAEAttachForm.getAttributes().getNamedItem("OpenClinica:SdvStatus");
		if (sdvNode != null)
			sdvNode.setNodeValue("Verified");
		else
			((Element) newAEAttachForm).setAttribute("OpenClinica:SdvStatus", "Verified");

		newAEAttachForm.getAttributes().getNamedItem("OpenClinica:Status").setNodeValue("data entry complete");
		newAEAttachForm.getAttributes().getNamedItem("OpenClinica:WorkflowStatus").setNodeValue("data entry complete");

		newStudyEventData.appendChild(newAEAttachForm);

		return newDoc;

	}

	static boolean checkValuesWithAENodes(Node aeAttach, List<AEEntry> aeList) {
		boolean check = false;
		String createdBy = aeAttach.getAttributes().getNamedItem("OpenClinica:CreatedBy").getNodeValue();
		if ("api_ispy_dm".equals(createdBy)) {

			XPath xpath = XPathFactory.newInstance().newXPath();

			try {

				NodeList endNodeList = (NodeList) xpath.evaluate(
						"./ItemGroupData[@ItemGroupOID='IG_AE_SO_GR_DT_RANGE']/ItemData[@ItemOID='I_AE_SO_DT_SOURCE_END']",
						aeAttach, XPathConstants.NODESET);
				String endDateVal = endNodeList.item(0).getAttributes().getNamedItem("Value").getNodeValue();
				Date endDate = new SimpleDateFormat("yyyy-mm-dd").parse(endDateVal);

				NodeList startNodeList = (NodeList) xpath.evaluate(
						"./ItemGroupData[@ItemGroupOID='IG_AE_SO_GR_DT_RANGE']/ItemData[@ItemOID='I_AE_SO_DT_SOURCE_START']",
						aeAttach, XPathConstants.NODESET);
				String startDateVal = startNodeList.item(0).getAttributes().getNamedItem("Value").getNodeValue();
				Date startDate = new SimpleDateFormat("yyyy-mm-dd").parse(startDateVal);

				for (AEEntry ae : aeList) {
					if (ae.visitDate != null) {
						if (ae.visitDate.compareTo(startDate) >= 0 && ae.visitDate.compareTo(endDate) <= 0) {
							if (ae.sdv.equals("Verified") && ae.status.equals("data entry complete")) {
								check = true;
							} else {
								check = false;
								break;
							}
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		return check;

	}

	public static void updateDBFuTimepoints(List<String> subjectKeyList, int subBatchCount) {

		String selectStr = "SELECT subjectid, timepoint, maps_to_timepoint, start_date, create_date from futimepoints where subjectid = ? and status = 'N'";
		List<Map<String, String>> rows = null;
		// ODM1-3.xsd
		String clinicalDataStartStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
				+ "<ODM xmlns=\"http://www.cdisc.org/ns/odm/v1.3\" xmlns:OpenClinica=\"http://www.openclinica.org/ns/odm_ext_v130/v3.1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" FileOID=\"D20200925211615+0000\" Description=\"Study Metadata\" CreationDateTime=\"2020-10-01T12:00:00\" FileType=\"snapshot\" ODMVersion=\"1.3\" xsi:schemaLocation=\"http://www.cdisc.org/ns/odm/v1.3 OpenClinica-ODM1-3-0-OC3-0.xsd\">\n"
				+ "  <ClinicalData StudyOID=\"S_ISPY(PROD)\" MetaDataVersionOID=\"null\">\n";

		String clinicalDataEndStr = "  </ClinicalData></ODM>";
		String uploadStr = "";

		int subjectCount = 1;
		String pidList = "";

		for (String key : subjectKeyList) {
			pidList += key;
			// if(subjectCount == 1) {
			// uploadStr = clinicalDataStartStr;
			// }

			rows = selectRows(selectStr, key);

			String stmt1 = "UPDATE futimepoints SET status='P', results='Processing' WHERE   subjectid = ?  ";
			updateDB(stmt1, key, null);

			String subjectStartStr = "    <SubjectData SubjectKey=\"SS_{PID}\" OpenClinica:StudySubjectID=\"{PID}\" OpenClinica:Status=\"Available\">\n";

			String studyEventStr = " <StudyEventData StudyEventOID=\"{TIMEPOINT}\" OpenClinica:StartDate=\"{START_DATE}\" StudyEventRepeatKey=\"1\">\n"
					+ "				<FormData FormOID=\"F_FOLLOWUP\" OpenClinica:FormName=\"FOLLOWUP\" OpenClinica:FormLayoutOID=\"2.91\" OpenClinica:CreatedDate=\"{CURRENT_DATE}\" OpenClinica:CreatedBy=\"api_ispy_dm\" OpenClinica:UpdatedDate=\"{CURRENT_DATE}\" OpenClinica:UpdatedBy=\"api_ispy_dm\" OpenClinica:Status=\"data entry complete\" OpenClinica:WorkflowStatus=\"data entry complete\" OpenClinica:SdvStatus=\"Verified\" OpenClinica:Required=\"No\">\n"
					+ "					<ItemGroupData ItemGroupOID=\"IG_FOLLO_FOLLOW_UP_STUB\" ItemGroupRepeatKey=\"1\" OpenClinica:ItemGroupName=\"FOLLOW_UP_STUB\" TransactionType=\"Insert\">\n"
					+ "						<ItemData ItemOID=\"I_FOLLO_TIMEPOINT_NOT_REPORTED\" OpenClinica:ItemName=\"TIMEPOINT_NOT_REPORTED\" Value=\"{MAPS_TO_TIMEPOINT}\"/>\n"
					+ "					</ItemGroupData>\n" + "				</FormData>\n"
					+ "			</StudyEventData>\n";

			String subjectEndStr = "</SubjectData>";

			Map<String, String> pidMap = Map.of("PID", key);

			StrSubstitutor sub = new StrSubstitutor(pidMap, "{", "}");

			String subStrMod = sub.replace(subjectStartStr);
			uploadStr += subStrMod;
			StrSubstitutor subStudyEvent = null;
			String studyEventMod = "";

			for (Map<String, String> mergeMap : rows) {
				subStudyEvent = new StrSubstitutor(mergeMap, "{", "}");
				studyEventMod = subStudyEvent.replace(studyEventStr);
				uploadStr += studyEventMod;
			}
			uploadStr += subjectEndStr;

			String stmt = "UPDATE futimepoints SET status='C', create_date = now(), results='PROCESSED' WHERE   subjectid = ?  ";
			updateDB(stmt, key, null);

			if (subjectCount == subBatchCount || key.equals(subjectKeyList.get(subjectKeyList.size() - 1))) {
				subjectCount = 1; // reset
				uploadStr += clinicalDataEndStr;

				System.out.println("batch ODM for pid list: " + pidList + "=" + uploadStr);

				String insertstmt = "INSERT INTO futimepointsImportbatch (subjectidlist, status, create_date, importXml) VALUES ('"
						+ pidList + "', 'C', now(), '" + uploadStr + "')";
				insertDB(insertstmt);
				pidList = "";
				uploadStr = clinicalDataStartStr;
			} else {
				subjectCount++;
				pidList += "|";
			}

		} // for

	}

	public static void updateMri(List<String> subjectKeyList, int subBatchCount) {

		String clinicalDataStartStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
				+ "<ODM xmlns=\"http://www.cdisc.org/ns/odm/v1.3\" xmlns:OpenClinica=\"http://www.openclinica.org/ns/odm_ext_v130/v3.1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" FileOID=\"D20200925211615+0000\" Description=\"Study Metadata\" CreationDateTime=\"2020-10-01T12:00:00\" FileType=\"snapshot\" ODMVersion=\"1.3\" xsi:schemaLocation=\"http://www.cdisc.org/ns/odm/v1.3 ODM1-3.xsd\">\n"
				+ "  <ClinicalData StudyOID=\"S_ISPY(PROD)\" MetaDataVersionOID=\"null\">\n";

		String clinicalDataEndStr = "  </ClinicalData></ODM>";
		String uploadStr = clinicalDataStartStr;

		int subjectCount = 1;
		String pidList = "";
		String padSubId = "";
		for (String key : subjectKeyList) {
			padSubId = String.format("%05d", Integer.parseInt(key));
			// rows = selectRowsForMri(selectStr, key);
			pidList += key;
			String stmt1 = "UPDATE mri_inserts SET status='P', results='Processing' WHERE   subjectid = ?  ";
			updateDB(stmt1, key, null);

			Node subjectNode = findStudyEventsForMri(key, padSubId);
			StringBuffer buff = new StringBuffer();
			JaxbUtil.getXMLString(subjectNode, false, buff, true);

			String subjectStr = buff.toString();
			System.out.println(subjectStr);
			uploadStr += subjectStr;

			String stmt = "UPDATE mri_inserts SET status='C', create_date = now(), results='PROCESSED' WHERE   subjectid = ?  ";
			updateDB(stmt, key, null);

			if (subjectCount == subBatchCount || padSubId.equals(subjectKeyList.get(subjectKeyList.size() - 1))) {
				subjectCount = 1; // reset

				uploadStr += clinicalDataEndStr;

				// System.out.println("batch ODM for pid list: " + pidList +"=" + uploadStr);

				String insertstmt = "INSERT INTO mriImportbatch (subjectidlist, status, create_date, importXml) VALUES ('"
						+ pidList + "', 'C', now(), '" + uploadStr + "')";
				insertDB(insertstmt);
				pidList = "";
				uploadStr = clinicalDataStartStr;
			} else {
				subjectCount++;
				pidList += "|";
			}

		} // for

	}

	public static Node findStudyEventsForMri(String dbKey, String subjectId) {
		OCUtil util = new OCUtil();
		String username = util.getAppProperties().getProperty("username");
		String password = util.getAppProperties().getProperty("password");
		// String subId = SubjectData.getSubjectIdForKey(subjectId);

		String subId = "SS_" + subjectId;
		System.out.println("key:" + subjectId + ", subjectId:" + subId);

		String accessToken = OCUtil.getAccessToken("qlhc2.build.openclinica.io", username, password); // username/password
																										// need to be
																										// for OC data
																										// manager,
																										// vandhana may
																										// already have
																										// them in a
																										// properties
																										// file
		String url = util.getAppProperties().getProperty("odmUrlWithSubjectId");
		String urlODM = String.format(url, subId);

		List<Node> studyEventList = new ArrayList<Node>();
		Document documentODM = null;
		Element subject = null;

		Connection connection = null;
		PreparedStatement statementCRFs = null;
		ResultSet rs = null;

		try {

			connection = DBCPDataSource.getConnection();
			connection.setAutoCommit(true);

			String strstmt = "";

			strstmt = "select studyeventoid, start_date, reason, instance from mri_inserts where subjectid = ?";
			statementCRFs = connection.prepareStatement(strstmt);
			statementCRFs.setString(1, dbKey);

			rs = statementCRFs.executeQuery();

			String studyeventoid, startDate, instance, reason = "";
			Node studyEvent = null;
			documentODM = OCUtil.getXML(urlODM, accessToken);

			while (rs.next()) {
				studyeventoid = rs.getString("studyeventoid");
				startDate = rs.getString("studyeventoid");
				reason = rs.getString("reason");
				instance = rs.getString("instance");

				XPath xpath = XPathFactory.newInstance().newXPath();
				NodeList nodes = (NodeList) xpath.evaluate("//StudyEventData[@StudyEventOID='" + studyeventoid + "']",
						documentODM, XPathConstants.NODESET);
				if (nodes.getLength() > 0) {
					for (int i = 0; i < nodes.getLength(); i++) {
						studyEvent = nodes.item(i);
						String startDateS = studyEvent.getAttributes().getNamedItem("OpenClinica:StartDate") != null
								? studyEvent.getAttributes().getNamedItem("OpenClinica:StartDate").getNodeValue()
								: "";
						System.out.println("StudyEvent startDate: " + startDateS);

						studyEvent.getParentNode().removeChild(studyEvent);
						studyEvent = createStudyEventForMri(documentODM, studyeventoid, instance, reason, startDateS);
						studyEventList.add(studyEvent);
					}
				} else { // No StudyEvent
					studyEvent = createStudyEventForMri(documentODM, studyeventoid, instance, reason, "");
					studyEventList.add(studyEvent);
				}

			}

			if (documentODM != null) {
				subject = documentODM.createElement("SubjectData");
				subject.setAttribute("SubjectKey", subId);
				subject.setAttribute("OpenClinica:StudySubjectID", subjectId);
				subject.setAttribute("OpenClinica:Status", "Available");

				for (Node studyEventNode : studyEventList) {
					subject.appendChild(studyEventNode);
				}
			}

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			try {
				if (null != rs) {
					rs.close();
					statementCRFs.close();

				}
				if (null != connection) {
					connection.close();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return subject;

	}

	public static void insertIntoImportPatDemReconsent(List<String> subjecIdList, String insertTableName) {

		Connection connection = null;
		PreparedStatement statementCRFs = null;
		ResultSet rs = null;
		Map<String, String> dbMap = null;
		String subKey;

		for (String subId : subjecIdList) {

			try {

				String strStmt = null;
				String insertImportTable;
				if (subId != null) {
					String stmt1 = "UPDATE " + insertTableName + " SET status='P' WHERE   subjectid = ?  ";
					updateDB(stmt1, subId, null);

					Document odmDocument = null;
					if (insertTableName.equals("patDemoInserts")) {
						strStmt = "select subjectKey, subjectId from " + insertTableName
								+ " where subjectId = ? and status = 'N' ";
						dbMap = selectSingleRowDB(strStmt, subId);
						subKey = dbMap.get("SUBJECT_KEY");
						odmDocument = findAndUpdatePatDem(subKey, subId);
						insertImportTable = "patDemoImport";
					} else {
						System.out.println("Wrong tableName (patDemoInserts/reconsentInserts):" + insertTableName);
						continue;
					}
					StringBuffer buff = new StringBuffer();
					JaxbUtil.getXMLString(odmDocument, false, buff, true);
					String odmImportStr = buff.toString();
					odmImportStr = odmImportStr.replace("<#document>", "");
					odmImportStr = odmImportStr.replace("</#document>", "");
					System.out.println("Import XML:" + odmImportStr);

					String insertstmt = "INSERT INTO " + insertImportTable
							+ " (subjectidlist, status, create_date, importXml) VALUES ('" + subId + "', 'C', now(), '"
							+ odmImportStr + "')";
					insertDB(insertstmt);

					String stmt = "UPDATE " + insertTableName
							+ " SET status='C', create_date = now() WHERE   subjectid = ?  ";
					updateDB(stmt, subId, null);
				} else {
					System.out.println("DB record not found for subjectId:" + subId);
				}

			} catch (Exception e) {

				e.printStackTrace();
			} finally {
				try {
					if (null != rs) {
						rs.close();
						statementCRFs.close();

					}
					if (null != connection) {
						connection.close();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} // for

	}

	public static void insertIntoImportPatDemReconsentBySelectAllN(String insertTableName) {

		Connection connection = null;
		PreparedStatement statementCRFs = null;
		ResultSet rs = null;
		Map<String, String> dbMap = null;
		String subKey = null;

		String subId = null;
		String strStmt = null;
		String seRepeatKey = null;
		String siteOid = null;

		String insertImportTable;

		try {

			connection = DBCPDataSource.getConnection();
			connection.setAutoCommit(true);
			strStmt = "select * from " + insertTableName + " where status = 'N' ";
			statementCRFs = connection.prepareStatement(strStmt);
			rs = statementCRFs.executeQuery();
			while (rs.next()) {
				subId = rs.getString("subjectId");
				subKey = rs.getString("subjectKey");
				seRepeatKey = rs.getString("study_event_repeat_key");
				String stmt1 = "UPDATE " + insertTableName
						+ " SET status='P' WHERE   subjectid = ? and study_event_repeat_key = ? ";
				updateDB(stmt1, subId, seRepeatKey);

				Document odmDocument = null;
				if (insertTableName.equals("reconsentInserts")) {
					seRepeatKey = rs.getString("study_event_repeat_key");

					dbMap = selectSingleRowDBForReconsent(rs);

					odmDocument = findAndUpdateReconsent(dbMap);
					insertImportTable = "reconsentImport";
				} else {
					System.out.println("Wrong tableName (patDemoInserts/reconsentInserts):" + insertTableName);
					continue;
				}
				StringBuffer buff = new StringBuffer();
				JaxbUtil.getXMLString(odmDocument, false, buff, true);
				String odmImportStr = buff.toString();
				odmImportStr = odmImportStr.replace("<#document>", "");
				odmImportStr = odmImportStr.replace("</#document>", "");
				System.out.println("Import XML:" + odmImportStr);

				String insertstmt = "INSERT INTO " + insertImportTable
						+ " (subjectid, study_event_repeat_key, status, create_date, importXml) VALUES ('" + subId
						+ "','" + seRepeatKey + "' , 'N', now(), '" + odmImportStr + "')";
				insertDB(insertstmt);

				String stmt = "UPDATE " + insertTableName
						+ " SET status='C', create_date = now() WHERE   subjectid = ? and study_event_repeat_key = ? ";
				updateDB(stmt, subId, seRepeatKey);
			}

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			try {
				if (null != rs) {
					rs.close();
					statementCRFs.close();

				}
				if (null != connection) {
					connection.close();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public static Document findAndUpdatePatDem(String subjectKey, String subjectId) {
		OCUtil util = new OCUtil();
		String username = util.getAppProperties().getProperty("username");
		String password = util.getAppProperties().getProperty("password");

		System.out.println("key:" + subjectKey + ", subjectId:" + subjectId);

		String accessToken = OCUtil.getAccessToken("qlhc2.build.openclinica.io", username, password);

		String url = util.getAppProperties().getProperty("odmUrlWithSubjectId");
		String urlODM = String.format(url, subjectId);

		Document documentODM = null;
		Document newDoc = null;

		try {

			Node studyEvent = null;
			Node formNode = null;
			documentODM = OCUtil.getXML(urlODM, accessToken);
			StringBuffer buff = new StringBuffer();
			JaxbUtil.getXMLString(documentODM, true, buff, true);
			String odmXml = buff.toString();
			System.out.println("ODM xml=" + odmXml);

			Node subjectNode = OCUtil.getSubjectNode(documentODM);
			Node clinicalData = subjectNode.getParentNode();
			Node odm = clinicalData.getParentNode();

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			DocumentBuilder builder = factory.newDocumentBuilder();
			newDoc = builder.newDocument();

			Node odmNew = newDoc.importNode(odm, false);
			newDoc.appendChild(odmNew);

			Node clinDataNew = newDoc.importNode(clinicalData, false);
			odmNew.appendChild(clinDataNew);

			Node subNew = newDoc.importNode(subjectNode, false);
			clinDataNew.appendChild(subNew);

			String dateValue = "oct_07_2021";
			// String version = "3.1";

			XPath xpath = XPathFactory.newInstance().newXPath();
			NodeList nodes = (NodeList) xpath.evaluate(".//StudyEventData", documentODM, XPathConstants.NODESET);
			if (nodes.getLength() > 0) {
				for (int i = 0; i < nodes.getLength(); i++) {
					studyEvent = nodes.item(i);
					String seOID = studyEvent.getAttributes().getNamedItem("StudyEventOID") != null
							? studyEvent.getAttributes().getNamedItem("StudyEventOID").getNodeValue()
							: "";
					// System.out.println("StudyEvent OID: " + seOID);
					if (!seOID.equals("SE_SCREENING"))
						studyEvent.getParentNode().removeChild(studyEvent);
					else {
						NodeList formList = (NodeList) xpath.evaluate(".//FormData", studyEvent,
								XPathConstants.NODESET);

						for (int j = 0; j < formList.getLength(); j++) {
							formNode = formList.item(j);

							String formOid = formNode.getAttributes().getNamedItem("FormOID") != null
									? formNode.getAttributes().getNamedItem("FormOID").getNodeValue()
									: "";
							// System.out.println("Form OID: " + formOid);
							if (!formOid.equals("F_PATIENTDEMOG"))
								formNode.getParentNode().removeChild(formNode);
							else {
								String status = formNode.getAttributes().getNamedItem("OpenClinica:Status") != null
										? formNode.getAttributes().getNamedItem("OpenClinica:Status").getNodeValue()
										: "";
								String sdvStatus = formNode.getAttributes()
										.getNamedItem("OpenClinica:SdvStatus") != null
												? formNode.getAttributes().getNamedItem("OpenClinica:SdvStatus")
														.getNodeValue()
												: "";
								if (status.equalsIgnoreCase("data entry complete")
										&& (sdvStatus.equalsIgnoreCase("Never Verified")
												|| sdvStatus.equalsIgnoreCase("Changed Since Verified")
												|| sdvStatus.trim().equalsIgnoreCase("")
												|| sdvStatus.equalsIgnoreCase("Verified"))) {

									Node newStudyEventData = newDoc.importNode(formNode.getParentNode(), false);
									subNew.appendChild(newStudyEventData);

									Node newForm = newDoc.importNode(formNode, false);
									newStudyEventData.appendChild(newForm);

									Node itemGroupNode = createItemGroupData(newDoc, "IG_PATIE_REG_INFO", "reg_info");
									newForm.appendChild(itemGroupNode);

									((Element) newForm).setAttribute("OpenClinica:ReasonForChangeForCompleteForms",
											"Update via Import");
									Node icfDateItem = createItemData(newDoc, "I_PATIE_ICF_DATE", "icf_date",
											dateValue);
									itemGroupNode.appendChild(icfDateItem);
									Node protoDateItem = createItemData(newDoc, "I_PATIE_PROTOCOL_DATE",
											"protocol_date", dateValue);
									itemGroupNode.appendChild(protoDateItem);

									/*
									 * Node icfDateItem = (Node) xpath.evaluate(
									 * ".//ItemGroupData/ItemData[@ItemOID='I_PATIE_ICF_DATE']", formNode,
									 * XPathConstants.NODE); if (icfDateItem != null) {
									 * icfDateItem.getAttributes().getNamedItem("Value").setNodeValue(dateValue);
									 * ((Element)
									 * formNode).setAttribute("OpenClinica:ReasonForChangeForCompleteForms",
									 * "Update via Import");
									 * 
									 * }
									 * 
									 * 
									 * Node protoDateItem = (Node) xpath.evaluate(
									 * ".//ItemGroupData/ItemData[@ItemOID='I_PATIE_PROTOCOL_DATE']", formNode,
									 * XPathConstants.NODE); if (protoDateItem != null) {
									 * protoDateItem.getAttributes().getNamedItem("Value").setNodeValue(dateValue);
									 * ((Element)
									 * formNode).setAttribute("OpenClinica:ReasonForChangeForCompleteForms",
									 * "Update via Import"); }
									 */
								}
								// formNode.getAttributes().getNamedItem("OpenClinica:FormLayoutOID").setNodeValue(version);

							}

						} // for
					} // else
				} // for
			} // if
		} // try
		catch (Exception e) {

			e.printStackTrace();
		}

		return newDoc;

	}

	public static Document findAndUpdateReconsent(Map<String, String> dbMap) {

		String subjectId = dbMap.get("subjectId");
		String subjectKey = dbMap.get("subjectKey");
		String seRepeat = dbMap.get("study_event_repeat_key");
		String siteOid = dbMap.get("siteoid");
		String icfVerDate = SdvUtil.convertDate(dbMap.get("icf_ver_date"));
		String irbApprovalProtoDate = SdvUtil.convertDate(dbMap.get("irb_approval_proto_date"));
		String irbApprovalIcfDate = SdvUtil.convertDate(dbMap.get("irb_approval_icf_date"));
		String protoVerDate = SdvUtil.convertDate(dbMap.get("proto_ver_date"));

		OCUtil util = new OCUtil();
		String username = util.getAppProperties().getProperty("username");
		String password = util.getAppProperties().getProperty("password");

		System.out
				.println("key:" + subjectKey + ", subjectId:" + subjectId + ", se repeat key:" + seRepeat + ", siteOid:"
						+ siteOid + ", icfVerDate:" + icfVerDate + ", irbApprovalProtoDate:" + irbApprovalProtoDate
						+ ", irbApprovalIcfDate: " + irbApprovalIcfDate + ",protoVerDate: " + protoVerDate);

		String accessToken = OCUtil.getAccessToken("qlhc2.build.openclinica.io", username, password);
		String url = util.getAppProperties().getProperty("odmUrlWithSubjectId");
		String urlODM = String.format(url, subjectKey);

		Document documentODM = null;
		Document newDoc = null;
		try {

			Node studyEvent = null;
			Node formNode = null;
			documentODM = OCUtil.getXML(urlODM, accessToken);
			StringBuffer buff = new StringBuffer();
			// JaxbUtil.getXMLString(documentODM, true, buff, true);
			// String odmXml = buff.toString();
			// System.out.println("ODM xml=" + odmXml);


			Node subjectNode = OCUtil.getSubjectNode(documentODM);
			Node clinicalData = subjectNode.getParentNode();
			Node odm = clinicalData.getParentNode();

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			DocumentBuilder builder = factory.newDocumentBuilder();
			newDoc = builder.newDocument();

			Node odmNew = newDoc.importNode(odm, false);
			newDoc.appendChild(odmNew);

			Node clinDataNew = newDoc.importNode(clinicalData, false);
			odmNew.appendChild(clinDataNew);

			Node subNew = newDoc.importNode(subjectNode, false);
			clinDataNew.appendChild(subNew);

			XPath xpath = XPathFactory.newInstance().newXPath();
			NodeList nodes = (NodeList) xpath.evaluate(".//StudyEventData", documentODM, XPathConstants.NODESET);
			if (nodes.getLength() > 0) {
				for (int i = 0; i < nodes.getLength(); i++) {
					studyEvent = nodes.item(i);
					String seOID = studyEvent.getAttributes().getNamedItem("StudyEventOID") != null
							? studyEvent.getAttributes().getNamedItem("StudyEventOID").getNodeValue()
							: "";
					String seRepeatKey = studyEvent.getAttributes().getNamedItem("StudyEventRepeatKey") != null
							? studyEvent.getAttributes().getNamedItem("StudyEventRepeatKey").getNodeValue()
							: "";
					System.out.println("StudyEvent OID: " + seOID + ", repeatKey:" + seRepeatKey);

					if (seOID.equals("SE_RECONSENTS") && seRepeat.equals(seRepeatKey)) {

						NodeList formList = (NodeList) xpath.evaluate(".//FormData", studyEvent,
								XPathConstants.NODESET);

						for (int j = 0; j < formList.getLength(); j++) {
							formNode = formList.item(j);

							String formOid = formNode.getAttributes().getNamedItem("FormOID") != null
									? formNode.getAttributes().getNamedItem("FormOID").getNodeValue()
									: "";
							System.out.println("Form OID: " + formOid);
							if (!formOid.equals("F_RE_CONSENT"))
								formNode.getParentNode().removeChild(formNode);
							else {

								String status = formNode.getAttributes().getNamedItem("OpenClinica:Status") != null
										? formNode.getAttributes().getNamedItem("OpenClinica:Status").getNodeValue()
										: "";
								String sdvStatus = formNode.getAttributes()
										.getNamedItem("OpenClinica:SdvStatus") != null
												? formNode.getAttributes().getNamedItem("OpenClinica:SdvStatus")
														.getNodeValue()
												: "";
								String formlayoutOid = formNode.getAttributes()
										.getNamedItem("OpenClinica:FormLayoutOID") != null
												? formNode.getAttributes().getNamedItem("OpenClinica:FormLayoutOID")
														.getNodeValue()
												: "";

								Node siteorTreatment = (Node) xpath.evaluate(
										".//ItemGroupData/ItemData[@ItemOID='I_RE_CO_RECONSENT_TYPE' and @ItemName='RECONSENT_TYPE' "
												+ "and  (@Value='Site_Specific_Screening_Consent' or @Value='Treatment_Consent' or @Value='Screening_Consent')]",
										formNode, XPathConstants.NODE);
								if (siteorTreatment != null) {
									Node newStudyEventData = newDoc.importNode(formNode.getParentNode(), false);
									subNew.appendChild(newStudyEventData);

									Node newForm = newDoc.importNode(formNode, false);
									newStudyEventData.appendChild(newForm);

									if (formlayoutOid != null && formlayoutOid.length() > 0) {
										if (!formlayoutOid.equals("1.11")) {
											formlayoutOid = "1.11";
											((Element) newForm).setAttribute("OpenClinica:FormLayoutOID",
													formlayoutOid);
										}
									}

									Node itemGroupNode = createItemGroupData(newDoc, "IG_RE_CO_S_SCREENING_RECONSENT",
											"S_SCREENING_RECONSENT");
									newForm.appendChild(itemGroupNode);
									((Element) newForm).setAttribute("OpenClinica:ReasonForChangeForCompleteForms",
											"Update via Import");

									Node siteScreeningTreatment = (Node) xpath.evaluate(
											".//ItemGroupData/ItemData[@ItemOID='I_RE_CO_RECONSENT_TYPE' and @ItemName='RECONSENT_TYPE']",
											formNode, XPathConstants.NODE);
									String val;
									if (siteScreeningTreatment != null) {
										val = siteScreeningTreatment.getAttributes().getNamedItem("Value")
												.getNodeValue();
										Node reconsentItemGroupNode = createItemGroupData(newDoc,
												"IG_RE_CO_S_RECONSENT", "S_RECONSENT");
										newForm.appendChild(reconsentItemGroupNode);
										Node reconsentDateItem = createItemData(newDoc, "I_RE_CO_RECONSENT_TYPE",
												"RECONSENT_TYPE", val);
										reconsentItemGroupNode.appendChild(reconsentDateItem);

										Node siteInfoItemGroupNode = createItemGroupData(newDoc, "IG_RE_CO_SITE_INFO",
												"S_RECONSENT");
										newForm.appendChild(siteInfoItemGroupNode);
										Node siteIcfCalcDateItem = createItemData(newDoc, "I_RE_CO_SITE_ICF_DATE_CALC",
												"site_icf_date_calc", "ALL");
										siteInfoItemGroupNode.appendChild(siteIcfCalcDateItem);

										if(icfVerDate!=null && icfVerDate.length()> 0) {
											 Node icfVerDateOldItem = createItemData(newDoc,
											 "I_RE_CO_SCREENING_ICF_VER_DATE", "SCREENING_ICF_VER_DATE", "");
											 itemGroupNode.appendChild(icfVerDateOldItem);
	
											Node icfVerDateNewItem = createItemData(newDoc,
													"I_RE_CO_SCREENING_ICF_VER_DATE2", "SCREENING_ICF_VER_DATE2",
													icfVerDate);
											itemGroupNode.appendChild(icfVerDateNewItem);
										}

										// Node irbApprovalDateOldItem = createItemData(newDoc,
										// "I_RE_CO_SCREENING_IRB_APPROVAL_DATE", "SCREENING_IRB_APPROVAL_DATE", "");
										// itemGroupNode.appendChild(irbApprovalDateOldItem);
										
										if(irbApprovalProtoDate!=null && irbApprovalProtoDate.length()>0) {

											 Node irbApprovalProtoOldDateItem = createItemData(newDoc,
											 "I_RE_CO_SCREENING_IRB_APPROVAL_PROT",
											 "SCREENING_IRB_APPROVAL_PROTOCOL_DATE", "");
											 itemGroupNode.appendChild(irbApprovalProtoOldDateItem);
	
											Node irbApprovalProtoDateNewItem = createItemData(newDoc,
													"I_RE_CO_SCREENING_IRB_APPROVAL_PROT_5154",
													"SCREENING_IRB_APPROVAL_PROTOCOL_DATE2", irbApprovalProtoDate);
											itemGroupNode.appendChild(irbApprovalProtoDateNewItem);
										}

										//if (val != null && val.equalsIgnoreCase("Treatment_Consent")
										//		|| val.equalsIgnoreCase("Screening_Consent")) {
										
										if(irbApprovalIcfDate!=null && irbApprovalIcfDate.length()> 0) {
											 Node irbApprovalIcfOldDateItem = createItemData(newDoc,
											 "I_RE_CO_SCREENING_IRB_APPROVAL_ICF_", "SCREENING_IRB_APPROVAL_ICF_DATE",
											 "");
											 itemGroupNode.appendChild(irbApprovalIcfOldDateItem);

											Node irbApprovalIcfNewDateItem = createItemData(newDoc,
													"I_RE_CO_SCREENING_IRB_APPROVAL_ICF_4950",
													"SCREENING_IRB_APPROVAL_ICF_DATE2", irbApprovalIcfDate);
											itemGroupNode.appendChild(irbApprovalIcfNewDateItem);
										}
										
										if(protoVerDate!=null && protoVerDate.length()>0) {

											 Node protoVerOldDateItem = createItemData(newDoc,
											 "I_RE_CO_SCREENING_PROTOCOL_VER_DATE", "SCREENING_PROTOCOL_VER_DATE",
											 "");
											 itemGroupNode.appendChild(protoVerOldDateItem);

											Node protoVerNewDateItem = createItemData(newDoc,
													"I_RE_CO_SCREENING_PROTOCOL_VER_DATE_2881",
													"SCREENING_PROTOCOL_VER_DATE2", protoVerDate);
											itemGroupNode.appendChild(protoVerNewDateItem);
										}

										//}
									} // if (siteScreeningTreatment != null)

								} // if(siteorTreatment!=null)

							}

						} // for

					} else {
						studyEvent.getParentNode().removeChild(studyEvent);
					}
				} // for
			} // if
		} // try
		catch (Exception e) {

			e.printStackTrace();
		}

		return newDoc;

	}

	private static String convertDate(String dateStr) {
		String convertedDate = null;
		if (dateStr != null) {
			if (dateStr.equals("October_15_2021_A27"))
				convertedDate = "2021-10-15";
			if (dateStr.equals("October_07_2021_A27"))
				convertedDate = "2021-10-07";
			if (dateStr.equals("September_13_2021_A27"))
				convertedDate = "2021-09-13";
			if (dateStr.equals("August_23_2021_A27"))
				convertedDate = "2021-08-23";
			if (dateStr.equals("October_20_2021_A27"))
				convertedDate = "2021-10-20";
		}

		return convertedDate;
	}

	public static void updatePSS(List<String> subjectKeyList, int subBatchCount) {

		String clinicalDataStartStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
				+ "<ODM xmlns=\"http://www.cdisc.org/ns/odm/v1.3\" xmlns:OpenClinica=\"http://www.openclinica.org/ns/odm_ext_v130/v3.1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" FileOID=\"D20200925211615+0000\" Description=\"OpenClinica 4 Stack 12.3 Data Import Template\" CreationDateTime=\"2020-10-01T12:00:00\" FileType=\"snapshot\" ODMVersion=\"1.3\" xsi:schemaLocation=\"http://www.cdisc.org/ns/odm/v1.3 ODM1-3.xsd\">\n"
				+ "  <ClinicalData StudyOID=\"S_ISPY(PROD)\" MetaDataVersionOID=\"null\">\n";

		String clinicalDataEndStr = "  </ClinicalData></ODM>";
		String uploadStr = clinicalDataStartStr;

		int subjectCount = 1;
		String pidList = "";
		String padSubId = "";
		for (String key : subjectKeyList) {
			padSubId = String.format("%05d", Integer.parseInt(key));
			// rows = selectRowsForMri(selectStr, key);
			pidList += key;
			String stmt1 = "UPDATE pss_inserts SET status='P' WHERE   subjectid = ?  ";
			updateDB(stmt1, key, null);

			Node subjectNode = findStudyEventsForPSS(key, padSubId);
			StringBuffer buff = new StringBuffer();
			JaxbUtil.getXMLString(subjectNode, false, buff, true);

			String subjectStr = buff.toString();
			System.out.println(subjectStr);
			uploadStr += subjectStr;

			String stmt = "UPDATE pss_inserts SET status='C', create_date = now() WHERE   subjectid = ?  ";
			updateDB(stmt, key, null);

			if (subjectCount == subBatchCount || padSubId.equals(subjectKeyList.get(subjectKeyList.size() - 1))) {
				subjectCount = 1; // reset

				uploadStr += clinicalDataEndStr;

				// System.out.println("batch ODM for pid list: " + pidList +"=" + uploadStr);

				String insertstmt = "INSERT INTO pssImportbatch (subjectidlist, status, create_date, importXml) VALUES ('"
						+ pidList + "', 'C', now(), '" + uploadStr + "')";
				insertDB(insertstmt);
				pidList = "";
				uploadStr = clinicalDataStartStr;
			} else {
				subjectCount++;
				pidList += "|";
			}

		} // for

	}

	public static void validateAndInsertImport(List<String> subjectKeyList, int subBatchCount) {

		OCUtil util = new OCUtil();
		String prod = util.getAppProperties().getProperty("PROD_RUN");
		String clinicalDataStartStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
				+ "<ODM xmlns=\"http://www.cdisc.org/ns/odm/v1.3\" xmlns:OpenClinica=\"http://www.openclinica.org/ns/odm_ext_v130/v3.1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" FileOID=\"D20200925211615+0000\" Description=\"OpenClinica 4 Stack 12.3 Data Import Template\" CreationDateTime=\"2020-10-01T12:00:00\" FileType=\"snapshot\" ODMVersion=\"1.3\" xsi:schemaLocation=\"http://www.cdisc.org/ns/odm/v1.3 ODM1-3.xsd\">\n"
				+ "  <ClinicalData StudyOID=\"S_ISPY(%s)\" MetaDataVersionOID=\"null\">\n";

		String clinicalDataEndStr = "  </ClinicalData></ODM>";
		String uploadStr = String.format(clinicalDataStartStr, prod.equalsIgnoreCase("true") ? "PROD" : "TEST");

		int subjectCount = 1;
		String pidList = "";
		String padSubId = "";
		String timepoint = "";
		for (String key : subjectKeyList) {
			// padSubId = String.format("%05d", Integer.parseInt(key));

			padSubId = key.split("\\|")[0];
			timepoint = key.split("\\|")[1];
			pidList += key.replace("|", "-");

			String stmt1 = "UPDATE ror_inserts SET status='P' WHERE   subjectid = ?  ";
			updateDB(stmt1, padSubId, null);

			Node subjectNode = evaluateForRoR(padSubId, timepoint);
			StringBuffer buff = new StringBuffer();
			JaxbUtil.getXMLString(subjectNode, false, buff, true);

			String subjectStr = buff.toString();
			System.out.println(subjectStr);
			uploadStr += subjectStr;

			String stmt = "UPDATE ror_inserts SET status='C', create_date = now() WHERE   subjectid = ?  ";
			updateDB(stmt, padSubId, null);

			if (subjectCount == subBatchCount || padSubId.equals(subjectKeyList.get(subjectKeyList.size() - 1))) {
				subjectCount = 1; // reset

				uploadStr += clinicalDataEndStr;

				// System.out.println("batch ODM for pid list: " + pidList +"=" + uploadStr);

				String insertstmt = "INSERT INTO rorImportbatch (subjectidlist, status, create_date, importXml, jobId) VALUES ('"
						+ pidList + "', 'C', now(), '" + uploadStr + "', '')";
				insertDB(insertstmt);
				pidList = "";
				uploadStr = String.format(clinicalDataStartStr, prod.equalsIgnoreCase("true") ? "PROD" : "TEST");
				;
			} else {
				subjectCount++;
				pidList += "|";
			}

		} // for

	}

	public static Element createRoRLetterFormData(Document documentODM) {
		DateFormat dateDf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
		String dateStr = dateDf.format(Calendar.getInstance().getTime());
		Element formData = documentODM.createElement("FormData");
		formData.setAttribute("FormOID", "F_RETURNOFRESU");
		formData.setAttribute("OpenClinica:FormName", "Return of Results Letter");
		formData.setAttribute("OpenClinica:FormLayoutOID", "1.2");
		formData.setAttribute("OpenClinica:CreatedDate", dateStr);
		formData.setAttribute("OpenClinica:CreatedBy", "kposa");
		formData.setAttribute("OpenClinica:UpdatedDate", dateStr);
		formData.setAttribute("OpenClinica:UpdatedBy", "kposa");
		formData.setAttribute("OpenClinica:Status", "initial data entry");
		formData.setAttribute("OpenClinica:WorkflowStatus", "initial data entry");
		// formData.setAttribute("OpenClinica:SdvStatus", "Verified");
		formData.setAttribute("OpenClinica:Required", "No");
		formData.setAttribute("OpenClinica:Visible", "Yes");
		formData.setAttribute("OpenClinica:Editable", "Yes");

		return formData;
	}

	/*
	 * 
	 * public static Element createRoRCheckListFormData(Document documentODM) {
	 * DateFormat dateDf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss"); String
	 * dateStr = dateDf.format(Calendar.getInstance().getTime()); Element formData =
	 * documentODM.createElement("FormData"); formData.setAttribute("FormOID",
	 * "F_ROR"); formData.setAttribute("OpenClinica:FormName",
	 * "Return of Results Checklist");
	 * formData.setAttribute("OpenClinica:FormLayoutOID", "1");
	 * formData.setAttribute("OpenClinica:CreatedDate", dateStr);
	 * formData.setAttribute("OpenClinica:CreatedBy", "kposa");
	 * formData.setAttribute("OpenClinica:UpdatedDate", dateStr);
	 * formData.setAttribute("OpenClinica:UpdatedBy", "kposa");
	 * formData.setAttribute("OpenClinica:Status", "initial data entry");
	 * formData.setAttribute("OpenClinica:WorkflowStatus", "initial data entry");
	 * formData.setAttribute("OpenClinica:Required", "No");
	 * formData.setAttribute("OpenClinica:Visible", "Yes");
	 * formData.setAttribute("OpenClinica:Editable", "Yes");
	 * 
	 * return formData; }
	 */

	/*
	 * 
	 * public static Node evaluateForRoRNew(String subjectId, String timepointOid) {
	 * DateFormat dateDf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss"); OCUtil
	 * util = new OCUtil(); String username =
	 * util.getAppProperties().getProperty("username"); String password =
	 * util.getAppProperties().getProperty("password"); String prod =
	 * util.getAppProperties().getProperty("PROD_RUN");
	 * 
	 * String subId = "SS_" + subjectId; System.out.println("key:" + subjectId +
	 * ", subjectId:" + subId);
	 * 
	 * String accessToken = OCUtil.getAccessToken("qlhc2.build.openclinica.io",
	 * username, password); //username/password need to be for OC data manager,
	 * vandhana may already have them in a properties file String url =
	 * util.getAppProperties().getProperty("odmUrlWithSubjectId");
	 * 
	 * String urlODM =
	 * String.format(url,prod.equalsIgnoreCase("true")?"PROD":"TEST", subId);
	 * 
	 * Document documentODM = null; String formOid, studyEventOid = null;
	 * 
	 * Connection connection = null; PreparedStatement statementCRFs = null;
	 * ResultSet rs = null; Date updateDate, twoHrsDate = null; Map<String, Node>
	 * studyEventMap = new HashMap<String, Node>(); Node odmNew, clinDataNew, subNew
	 * = null; boolean importBool = false; boolean checkCrf = false; try { Date
	 * currentTime = new Date(); String formattedCurTimeStr =
	 * dateDf.format(currentTime); connection = DBCPDataSource.getConnection();
	 * connection.setAutoCommit(true);
	 * 
	 * String strstmt = ""; String updateDateTime; Date updateDt; documentODM =
	 * OCUtil.getXML(urlODM, accessToken); String docXmlStr =
	 * OCUtil.convertDocToString(documentODM); System.out.println(docXmlStr); XPath
	 * xpath = XPathFactory.newInstance().newXPath(); String version; Node
	 * timepointStudyEvent =
	 * (Node)xpath.evaluate(".//StudyEventData[@StudyEventOID='" + timepointOid
	 * +"' and @OpenClinica:Status != 'completed']", documentODM,
	 * XPathConstants.NODE); checkCrf = rorCrfCheck(documentODM,
	 * timepointStudyEvent, timepointOid); if(checkCrf && timepointStudyEvent !=
	 * null) { if(timepointOid.equalsIgnoreCase("SE_EARLYTREATMENT")) {
	 * 
	 * //Check if SE_EARLYTREATMENT exists and not completed
	 * 
	 * Node mriWeek6SE = (Node)xpath.
	 * evaluate(".//StudyEventData[@StudyEventOID='SE_MRIWEEK6' and @OpenClinica:Status != 'completed']"
	 * , documentODM, XPathConstants.NODE); Node interRegSE = (Node)xpath.
	 * evaluate(".//StudyEventData[@StudyEventOID='SE_INTERREGIMEN' and @OpenClinica:Status != 'completed']"
	 * , documentODM, XPathConstants.NODE); //Check if SE_MRIWEEK6 and
	 * SE_INTERREGIMEN is not scheduled if(mriWeek6SE == null && interRegSE == null)
	 * importBool = true;
	 * 
	 * 
	 * }//if SE_EARLYTIMEPOINT else if
	 * (timepointOid.equalsIgnoreCase("SE_MRIWEEK6")) { Node interregimenSE =
	 * (Node)xpath.
	 * evaluate(".//StudyEventData[@StudyEventOID='SE_INTERREGIMEN' and @OpenClinica:Status != 'completed']"
	 * , documentODM, XPathConstants.NODE); //Check if SE_INTERREGIMEN is not
	 * scheduled if(interregimenSE == null) importBool = true;
	 * 
	 * } else if (timepointOid.equalsIgnoreCase("SE_INTERREGIMEN")) { Node surgerySE
	 * = (Node)xpath.
	 * evaluate(".//StudyEventData[@StudyEventOID='SE_SURGERY' and @OpenClinica:Status != 'completed']"
	 * , documentODM, XPathConstants.NODE); //Check if SE_SURGERY is not scheduled
	 * if(surgerySE == null) importBool = true; } }
	 * 
	 * 
	 * Node studyEvent,formNode,studyEventNew = null; Node rorLetterformData = null;
	 * Node rorLetterformDataOrig = null; Node rorLetterItemGroupData = null;
	 * if(importBool) { DocumentBuilderFactory factory =
	 * DocumentBuilderFactory.newInstance(); factory.setNamespaceAware(true);
	 * DocumentBuilder builder = factory.newDocumentBuilder(); Document newDoc =
	 * builder.newDocument();
	 * 
	 * Node subjectNode = OCUtil.getSubjectNode(documentODM); Node clinicalData =
	 * subjectNode.getParentNode(); Node odm = clinicalData.getParentNode();
	 * 
	 * odmNew = newDoc.importNode(odm, false); newDoc.appendChild(odmNew);
	 * 
	 * clinDataNew = newDoc.importNode(clinicalData, false);
	 * odmNew.appendChild(clinDataNew);
	 * 
	 * subNew = newDoc.importNode(subjectNode, false);
	 * clinDataNew.appendChild(subNew); studyEventNew =
	 * (Node)xpath.evaluate("./StudyEventData[@StudyEventOID='" + timepointOid +
	 * "']", subNew, XPathConstants.NODE); if(studyEventNew == null) { studyEventNew
	 * = newDoc.importNode(timepointStudyEvent, false);
	 * subNew.appendChild(studyEventNew); } rorLetterformData =
	 * (Node)xpath.evaluate("./FormData[@FormOID='F_RETURNOFRESU']", studyEventNew,
	 * XPathConstants.NODE); rorLetterformDataOrig =
	 * (Node)xpath.evaluate("./FormData[@FormOID='F_RETURNOFRESU']",
	 * timepointStudyEvent, XPathConstants.NODE); if(rorLetterformData == null) {
	 * if(rorLetterformDataOrig != null) //import FormData of F_RETURNOFRESU if it
	 * exists for studyEvent rorLetterformData =
	 * newDoc.importNode(rorLetterformDataOrig, false); else rorLetterformData =
	 * createRoRLetterFormData(newDoc);
	 * studyEventNew.appendChild(rorLetterformData);
	 * 
	 * }
	 * 
	 * 
	 * rorLetterItemGroupData =
	 * (Node)xpath.evaluate("./ItemGroupData[@ItemGroupOID='IG_RETUR_ROR']",
	 * rorLetterformData, XPathConstants.NODE); if(rorLetterItemGroupData == null) {
	 * rorLetterItemGroupData = createItemGroupData(newDoc,"IG_RETUR_ROR", "ROR" );
	 * rorLetterformData.appendChild(rorLetterItemGroupData); } Node
	 * itemDataForLetter =createItemData(newDoc, formToItemOid.get(formOidKey),
	 * formToItemName.get(formOidKey), itemDataValue);
	 * rorLetterItemGroupData.appendChild(itemDataForLetter);
	 * 
	 * if(itemDataValue!=null) { String itemDataTimeValue =
	 * findLastUpdated(formNode, itemDataValue); if(itemDataTimeValue!=null) { Node
	 * itemDataUpdatedDate =createItemData(newDoc, "I_RETUR_" +
	 * formToTime.get(formOidKey), formToTime.get(formOidKey), itemDataTimeValue);
	 * rorLetterItemGroupData.appendChild(itemDataUpdatedDate); }
	 * 
	 * }
	 * 
	 * 
	 * }
	 * 
	 * }//try catch(Exception e) {
	 * 
	 * }
	 * 
	 * return odmNew; }
	 */

	/*
	 * public static boolean rorCrfCheck(Document documentODM, Node
	 * timepointStudyEvent, String timepointOid) throws Exception {
	 * 
	 * boolean crfCheck = false; DateFormat dateDf = new
	 * SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss"); XPath xpath =
	 * XPathFactory.newInstance().newXPath(); String version;
	 * 
	 * 
	 * Date currentTime = new Date(); String updateDateTime; Date updateDt;
	 * 
	 * Node mriForm = (Node)xpath.
	 * evaluate("./FormData[@FormOID = 'F_MRIVOLUME' and @OpenClinica:FormLayoutOID > 1 ]"
	 * , timepointStudyEvent, XPathConstants.NODE); Node preRcbForm = (Node)xpath.
	 * evaluate("./FormData[@FormOID = 'F_PRERCBASSESS' and @OpenClinica:FormLayoutOID > 1 ]"
	 * , timepointStudyEvent, XPathConstants.NODE); //MRI form with version > 1
	 * present if(mriForm!=null) { updateDateTime =
	 * mriForm.getAttributes().getNamedItem("OpenClinica:UpdatedDate").getNodeValue(
	 * ); updateDt = dateDf.parse(updateDateTime); //MRI form with version > 1
	 * present and updated time of form is greater than xxx
	 * if(updateDt.after(currentTime)) { crfCheck = true;
	 * 
	 * } } if(!crfCheck && timepointOid.equalsIgnoreCase("SE_INTERREGIMEN") &&
	 * preRcbForm!=null) { updateDateTime =
	 * mriForm.getAttributes().getNamedItem("OpenClinica:UpdatedDate").getNodeValue(
	 * ); updateDt = dateDf.parse(updateDateTime); //MRI form with version > 1
	 * present and updated time of form is greater than xxx
	 * if(updateDt.after(currentTime)) { crfCheck = true;
	 * 
	 * } } if(!crfCheck) { //check for patient demo
	 * 
	 * Node screeningSEPatDemoForm = (Node)xpath.evaluate(
	 * "./StudyEventData[@StudyEventOID='SE_SCREENING']/FormData[@FormOID='F_PATIENTDEMOG']",
	 * documentODM, XPathConstants.NODE); if(screeningSEPatDemoForm!=null) { version
	 * = screeningSEPatDemoForm.getAttributes().getNamedItem(
	 * "OpenClinica:FormLayoutOID").getNodeValue(); if(Float.parseFloat(version) >=
	 * 3) { updateDateTime =
	 * screeningSEPatDemoForm.getAttributes().getNamedItem("OpenClinica:UpdatedDate"
	 * ).getNodeValue(); updateDt = dateDf.parse(updateDateTime);
	 * if(updateDt.after(currentTime)) { crfCheck = true; } } else { //check
	 * RECONSENT Node reconsentNode = (Node)xpath.
	 * evaluate(".//StudyEventData[@StudyEventOID='SE_RECONSENTS']/FormData[@FormOID='F_RE_CONSENT' @OpenClinica:FormLayoutOID>1]"
	 * , documentODM, XPathConstants.NODE); if(reconsentNode!=null) { Node
	 * screeningNode = (Node)xpath.evaluate(
	 * ".//ItemGroupData//ItemData[@ItemOID='I_RE_CO_RECONSENT_TYPE' and @Value='Screening_Consent' ]"
	 * , reconsentNode, XPathConstants.NODE); if(screeningNode != null) { crfCheck =
	 * true; } } else { Node screeningSiteNode = (Node)xpath.evaluate(
	 * ".//ItemGroupData//ItemData[@ItemOID='I_RE_CO_RECONSENT_TYPE' and  @Value='Site_Specific_Screening_Consent']"
	 * , reconsentNode, XPathConstants.NODE); if(screeningSiteNode != null) {
	 * crfCheck = true; }
	 * 
	 * 
	 * } } }
	 * 
	 * 
	 * } if(!crfCheck) { Node radomizationResForm = (Node)xpath.evaluate(
	 * "./StudyEventData[@StudyEventOID='SE_RANDOMIZATION']/FormData[@FormOID='F_RANDOMIZATIO_5299']",
	 * documentODM, XPathConstants.NODE); if(radomizationResForm!=null) { version =
	 * radomizationResForm.getAttributes().getNamedItem("OpenClinica:FormLayoutOID")
	 * .getNodeValue(); if(Float.parseFloat(version) >= 4) { updateDateTime =
	 * radomizationResForm.getAttributes().getNamedItem("OpenClinica:UpdatedDate").
	 * getNodeValue(); updateDt = dateDf.parse(updateDateTime);
	 * if(updateDt.after(currentTime)) { crfCheck = true; } } else {//check
	 * RECONSENT Node reconsentItemData = (Node)xpath.
	 * evaluate(".//StudyEventData[@StudyEventOID='SE_RECONSENTS']/FormData[@FormOID='F_RE_CONSENT' @OpenClinica:FormLayoutOID>1]//ItemGroupData//ItemData[@ItemOID='I_RE_CO_RECONSENT_TYPE' and @Value='Treatment_Consent' ]"
	 * , documentODM, XPathConstants.NODE); if(reconsentItemData!=null) { crfCheck =
	 * true; Node reconsentFormNode =
	 * reconsentItemData.getParentNode().getParentNode(); updateDateTime =
	 * reconsentFormNode.getAttributes().getNamedItem("OpenClinica:UpdatedDate").
	 * getNodeValue(); updateDt = dateDf.parse(updateDateTime);
	 * if(updateDt.after(currentTime)) { crfCheck = true; } }
	 * 
	 * }
	 * 
	 * } }
	 * 
	 * return crfCheck; }
	 */

	public static Hashtable<String, String> getDatesForSubjectOID(String subjectOid) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		Hashtable<String, String> dates = null;
		try {
			connection = DBCPDataSource.getConnection();
			connection.setAutoCommit(false);
			String sql = "select OID, datecreated, dateupdated, datesdvupdated, datequeryupdated, daterefreshed from oc_ispy.Subject where oid = '"
					+ subjectOid + "'";
			statement = connection.prepareStatement(sql);
			rs = statement.executeQuery();

			while (rs.next()) {
				dates = new Hashtable<String, String>();
				dates.put("OID", rs.getString("OID"));
				dates.put("DATE_CREATED", rs.getString("datecreated") != null ? rs.getString("datecreated") : "");
				dates.put("DATE_UPDATED", rs.getString("dateupdated") != null ? rs.getString("dateupdated") : "");
				dates.put("DATE_SDV_UPDATED",
						rs.getString("datesdvupdated") != null ? rs.getString("datesdvupdated") : "");
				dates.put("DATE_QUERY_UPDATED",
						rs.getString("datequeryupdated") != null ? rs.getString("datequeryupdated") : "");
				dates.put("DATE_REFRESHED", rs.getString("daterefreshed") != null ? rs.getString("daterefreshed") : "");

				break;
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			System.out.println("ERROR: getDatesForSubjectOID");
		} finally {
			try {
				if (connection != null)
					connection.close();
				if (statement != null)
					statement.close();
				if (rs != null)
					rs.close();
			} catch (Exception ex) {
				// do nothing
			}
		}
		return dates;
	}

	public static boolean rorCrfCheck(Document documentODM, Node timepointStudyEvent, String timepointOid,
			String subjectOid) throws Exception {

		boolean crfCheck = false;
		DateFormat dateDf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
		DateFormat dbDateDf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
		XPath xpath = XPathFactory.newInstance().newXPath();
		String version;
		Hashtable<String, String> dates = getDatesForSubjectOID(subjectOid);
		String dbLastUpdatedDateStr;
		if (dates != null) {
			dbLastUpdatedDateStr = dates.get("DATE_UPDATED");
			Date currentTime = new Date();
			String updateDateTime;
			Date updateDt;
			Date dbLastUpdatedDate = dbDateDf.parse(dbLastUpdatedDateStr);
			Date dbAfterHalfHour = addMinsToJavaUtilDate(dbLastUpdatedDate, 30);
			Date dbBeforeHalfHour = addMinsToJavaUtilDate(dbLastUpdatedDate, -30);
			Node mriForm = (Node) xpath.evaluate(".//FormData[@FormOID = 'F_MRIVOLUME'  ]", timepointStudyEvent,
					XPathConstants.NODE);
			System.out.println(subjectOid + ":dateupdated from DB/API update datetime:" + dbLastUpdatedDateStr);

			if (mriForm != null) {
//MRI form with version > 1 present
				version = mriForm.getAttributes().getNamedItem("OpenClinica:FormLayoutOID").getNodeValue();
				if (Float.parseFloat(version) > 1) {
					updateDateTime = mriForm.getAttributes().getNamedItem("OpenClinica:UpdatedDate").getNodeValue();
					updateDt = dateDf.parse(updateDateTime);
					System.out.println(subjectOid + ":F_MRIVOLUME form UpdatedDate with ver > 1" + updateDateTime);

//MRI form with version > 1 present and updated time of form is greater than xxx
					if (updateDt.after(dbBeforeHalfHour) && updateDt.before(dbAfterHalfHour))
						crfCheck = true;
				}

			}
			if (!crfCheck) { // check for patient demo

				Node screeningSEPatDemoForm = (Node) xpath.evaluate(
						".//StudyEventData[@StudyEventOID='SE_SCREENING']//FormData[@FormOID='F_PATIENTDEMOG']",
						documentODM, XPathConstants.NODE);
				if (screeningSEPatDemoForm != null) {
					version = screeningSEPatDemoForm.getAttributes().getNamedItem("OpenClinica:FormLayoutOID")
							.getNodeValue();
					if (Float.parseFloat(version) >= 3) {
						updateDateTime = screeningSEPatDemoForm.getAttributes().getNamedItem("OpenClinica:UpdatedDate")
								.getNodeValue();
						updateDt = dateDf.parse(updateDateTime);
						System.out.println(subjectOid + ":SE_SCREENING:F_PATIENTDEMOG form UpdatedDate with ver > 3"
								+ updateDateTime);

						if (updateDt.after(dbBeforeHalfHour) && updateDt.before(dbAfterHalfHour))
							crfCheck = true;

					} else { // check RECONSENT
						Node reconsentNode = (Node) xpath.evaluate(
								".//StudyEventData[@StudyEventOID='SE_RECONSENTS']//FormData[@FormOID='F_RE_CONSENT' ]",
								documentODM, XPathConstants.NODE);
						if (reconsentNode != null) {
							version = reconsentNode.getAttributes().getNamedItem("OpenClinica:FormLayoutOID")
									.getNodeValue();
							if (Float.parseFloat(version) > 1) {
								Node screeningNode = (Node) xpath.evaluate(
										".//ItemGroupData//ItemData[@ItemOID='I_RE_CO_RECONSENT_TYPE' and @Value='Screening_Consent' ]",
										reconsentNode, XPathConstants.NODE);
								if (screeningNode != null) {
									updateDateTime = reconsentNode.getAttributes()
											.getNamedItem("OpenClinica:UpdatedDate").getNodeValue();
									updateDt = dateDf.parse(updateDateTime);
									System.out.println(subjectOid
											+ ":SE_RECONSENTS:F_RE_CONSENT form UpdatedDate with ver > 1, Screening_Consent"
											+ updateDateTime);

									if (updateDt.after(dbBeforeHalfHour) && updateDt.before(dbAfterHalfHour))
										crfCheck = true;
								}

							} else {
								Node screeningSiteNode = (Node) xpath.evaluate(
										".//ItemGroupData//ItemData[@ItemOID='I_RE_CO_RECONSENT_TYPE' and  @Value='Site_Specific_Screening_Consent']",
										reconsentNode, XPathConstants.NODE);
								if (screeningSiteNode != null) {
									updateDateTime = reconsentNode.getAttributes()
											.getNamedItem("OpenClinica:UpdatedDate").getNodeValue();
									updateDt = dateDf.parse(updateDateTime);
									System.out.println(subjectOid
											+ ":SE_RECONSENTS:F_RE_CONSENT form UpdatedDate with ver > 1, Site_Specific_Screening_Consent"
											+ updateDateTime);

									if (updateDt.after(dbBeforeHalfHour) && updateDt.before(dbAfterHalfHour))
										crfCheck = true;

								}
							}
						}
					}
				}

			}
			if (!crfCheck) {
				Node radomizationResForm = (Node) xpath.evaluate(
						".//StudyEventData[@StudyEventOID='SE_RANDOMIZATION']//FormData[@FormOID='F_RANDOMIZATIO_5299']",
						documentODM, XPathConstants.NODE);
				if (radomizationResForm != null) {
					version = radomizationResForm.getAttributes().getNamedItem("OpenClinica:FormLayoutOID")
							.getNodeValue();
					if (Float.parseFloat(version) >= 4) {
						updateDateTime = radomizationResForm.getAttributes().getNamedItem("OpenClinica:UpdatedDate")
								.getNodeValue();
						updateDt = dateDf.parse(updateDateTime);
						System.out.println(
								subjectOid + ":SE_RANDOMIZATION:F_RANDOMIZATIO_5299 form UpdatedDate with ver >= 4:"
										+ updateDateTime);

						if (updateDt.equals(dbLastUpdatedDate)
								|| (updateDt.after(dbLastUpdatedDate) && updateDt.before(dbAfterHalfHour))) {
							crfCheck = true;
						}
					} else {// check RECONSENT
						Node reconsentItemData = (Node) xpath.evaluate(
								".//StudyEventData[@StudyEventOID='SE_RECONSENTS']//FormData[@FormOID='F_RE_CONSENT' and @OpenClinica:FormLayoutOID>1]//ItemGroupData//ItemData[@ItemOID='I_RE_CO_RECONSENT_TYPE' and @Value='Treatment_Consent' ]",
								documentODM, XPathConstants.NODE);
						if (reconsentItemData != null) {
							Node reconsentFormNode = reconsentItemData.getParentNode().getParentNode();
							updateDateTime = reconsentFormNode.getAttributes().getNamedItem("OpenClinica:UpdatedDate")
									.getNodeValue();
							updateDt = dateDf.parse(updateDateTime);
							System.out.println(subjectOid
									+ ":SE_RECONSENTS:F_RE_CONSENT form UpdatedDate with ver >1,Treatment_Consent:"
									+ updateDateTime);

							if (updateDt.equals(dbLastUpdatedDate)
									|| (updateDt.after(dbLastUpdatedDate) && updateDt.before(dbAfterHalfHour))) {
								crfCheck = true;
							}
						}
					}
				}
			}
			if (!crfCheck && timepointOid.equalsIgnoreCase("SE_INTERREGIMEN")) {
				Node formNode = (Node) xpath.evaluate(
						".//StudyEventData[@StudyEventOID='SE_INTERREGIMEN']//FormData[@FormOID='F_DISEASEASSES' or @FormOID='F_ONSTUDYPATHO' or @FormOID='F_INTERPATHO' or @FormOID='F_INTERREGIMEN']",
						documentODM, XPathConstants.NODE);
				if (formNode != null) {
					updateDateTime = formNode.getAttributes().getNamedItem("OpenClinica:UpdatedDate").getNodeValue();
					updateDt = dateDf.parse(updateDateTime);
//MRI form with version > 1 present and updated time of form is greater than xxx
					System.out.println(subjectOid
							+ ":SE_INTERREGIMEN:F_DISEASEASSES/F_ONSTUDYPATHO/F_INTERPATHO/F_INTERREGIMEN form UpdatedDate:"
							+ updateDateTime);

					if (updateDt.equals(dbLastUpdatedDate)
							|| (updateDt.after(dbLastUpdatedDate) && updateDt.before(dbAfterHalfHour))) {
						crfCheck = true;

					}
				}

			}
		}
		return crfCheck;
	}

	public static Date addMinsToJavaUtilDate(Date date, int mins) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MINUTE, mins);
		return calendar.getTime();
	}

	public static Node getSubjectNode(Document xmlDoc) throws XPathExpressionException {
		Node retNode = null;
		XPath xpath = XPathFactory.newInstance().newXPath();
		NodeList nodes = (NodeList) xpath.evaluate("//SubjectData[@StudySubjectID]", xmlDoc, XPathConstants.NODESET);

		if (nodes.getLength() == 1)
			retNode = nodes.item(0);
		return retNode;

	}

	public static Node evaluateForRoR(String subjectId, String timepointOid) {

		OCUtil util = new OCUtil();

		String prod = util.getAppProperties().getProperty("PROD_RUN");
		final String NO = "No open queries";
		final String YES = "Open queries";
		Map<String, String> formToItemOid = new HashMap<String, String>();
		formToItemOid.put("F_MRIVOLUME_EARLY", "I_RETUR_MRI_QUERIES");
		formToItemOid.put("F_MRIVOLUME_6WEEK", "I_RETUR_MRI_QUERIES");
		formToItemOid.put("F_MRIVOLUME_INTERREGIMEN", "I_RETUR_MRI_QUERIES");
		formToItemOid.put("F_RE_CONSENT_TREATMENT", "I_RETUR_RECONSENT_TREATMENT_QUERIES");
		formToItemOid.put("F_RANDOMIZATIO_5299", "I_RETUR_RANDOMIZ_QUERIES");
		formToItemOid.put("F_RE_CONSENT_SCREENING", "I_RETUR_RECONSENT_SCREENING_QUERIES");
		formToItemOid.put("F_RE_CONSENT_SITE_SCREENING", "I_RETUR_RECONSENT_SITE_SCREENING_QU");
		formToItemOid.put("F_INTERREGIMEN", "I_RETUR_IR_BIO_QUERIES");
		formToItemOid.put("F_INTERPATHO", "I_RETUR_IR_PATH_QUERIES");
		formToItemOid.put("F_DISEASEASSES", "I_RETUR_DA_QUERIES");
		formToItemOid.put("F_ONSTUDYPATHO", "I_RETUR_PTP_QUERIES");
		// formToItemOid.put("F_PATIENTDEMOG_SCREENING", "");

		Map<String, String> formToItemName = new HashMap<String, String>();
		formToItemName.put("F_MRIVOLUME_EARLY", "MRI_QUERIES");
		formToItemName.put("F_MRIVOLUME_6WEEK", "MRI_QUERIES");
		formToItemName.put("F_MRIVOLUME_INTERREGIMEN", "MRI_QUERY");
		formToItemName.put("F_RE_CONSENT_TREATMENT", "RECONSENT_TREATMENT_QUERIES");
		formToItemName.put("F_RANDOMIZATIO_5299", "RANDOMIZ_QUERIES");
		formToItemName.put("F_RE_CONSENT_SCREENING", "RECONSENT_SCREENING_QUERIES");
		formToItemName.put("F_RE_CONSENT_SITE_SCREENING", "RECONSENT_SITE_SCREENING_QUERIES");
		formToItemName.put("F_INTERREGIMEN", "IR_BIO_QUERIES");
		formToItemName.put("F_INTERPATHO", "IR_PATH_QUERIES");
		formToItemName.put("F_DISEASEASSES", "DA_QUERIES");
		formToItemName.put("F_ONSTUDYPATHO", "PTP_QUERIES");
		// formToItemName.put("F_PATIENTDEMOG_SCREENING", "");

		Map<String, String> formToTime = new HashMap<String, String>();
		formToTime.put("F_MRIVOLUME_EARLY", "MRI_QUERY_TIME");
		formToTime.put("F_MRIVOLUME_6WEEK", "MRI_QUERY_TIME");
		formToTime.put("F_MRIVOLUME_INTERREGIMEN", "MRI_QUERY_TIME");
		formToTime.put("F_RE_CONSENT_TREATMENT", "RETREAT_QUERY_TIME");
		formToTime.put("F_RANDOMIZATIO_5299", "RAND_QUERY_TIME");
		formToTime.put("F_RE_CONSENT_SCREENING", "RESCR_QUERY_TIME");
		formToTime.put("F_RE_CONSENT_SITE_SCREENING", "RESITE_QUERY_TIME");
		formToTime.put("F_INTERREGIMEN", "IR_BIO_QUERIES_TIME");
		formToTime.put("F_INTERPATHO", "IR_PATH_QUERIES_TIME");
		formToTime.put("F_DISEASEASSES", "DA_QUERIES_TIME");
		formToTime.put("F_ONSTUDYPATHO", "PTP_QUERIES_TIME");
		// formToTime.put("F_PATIENTDEMOG_SCREENING", "");

		List<String> formList = null;

		List<String> eaFormList = Arrays.asList("F_MRIVOLUME", "F_RE_CONSENT", "F_RANDOMIZATIO_5299");
		List<String> wk6FormList = Arrays.asList("F_MRIVOLUME", "F_RE_CONSENT", "F_RANDOMIZATIO_5299");
		List<String> irformList = Arrays.asList("F_MRIVOLUME", "F_RE_CONSENT", "F_RANDOMIZATIO_5299", "F_INTERREGIMEN",
				"F_INTERPATHO", "F_DISEASEASSES", "F_ONSTUDYPATHO", "F_PATIENTDEMOG");

		List<String> sixWkSEList = Arrays.asList("SE_MRIWEEK6", "SE_RECONSENTS", "SE_RANDOMIZATION", "SE_SCREENING");
		List<String> earlySEList = Arrays.asList("SE_RECONSENTS", "SE_EARLYTREATMENT", "SE_RANDOMIZATION",
				"SE_SCREENING");
		List<String> interRegSEList = Arrays.asList("SE_RECONSENTS", "SE_INTERREGIMEN", "SE_RANDOMIZATION",
				"SE_SCREENING");

		DateFormat dateDf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");

		String username = util.getAppProperties().getProperty("username");
		String password = util.getAppProperties().getProperty("password");
		String subjectOID = SubjectData.getSubjectIdForKey(subjectId);

		// String subId = "SS_" + subjectId;
		System.out.println("key:" + subjectId + ", subjectId:" + subjectOID);

		String accessToken = OCUtil.getAccessToken("qlhc2.build.openclinica.io", username, password); // username/password
																										// need to be
																										// for OC data
																										// manager,
																										// vandhana may
																										// already have
																										// them in a
																										// properties
																										// file
		String url = util.getAppProperties().getProperty("odmUrlWithSubjectId");

		String urlODM = String.format(url, prod.equalsIgnoreCase("true") ? "PROD" : "TEST", subjectOID);

		List<Node> studyEventList = new ArrayList<Node>();
		Document documentODM = null;
		String formOid, studyEventOid = null;

		Connection connection = null;
		PreparedStatement statementCRFs = null;
		ResultSet rs = null;
		Date updateDate, twoHrsDate = null;
		Map<String, Node> studyEventMap = new HashMap<String, Node>();
		Node odmNew, clinDataNew, subNew = null;

		Node studyEvent, formNode, studyEventNew = null;
		Node rorLetterformData = null;
		Node rorLetterformDataOrig = null;
		Node rorLetterItemGroupData = null;
		try {

			connection = DBCPDataSource.getConnection();
			connection.setAutoCommit(true);

			String strstmt = "";
			documentODM = OCUtil.getXML(urlODM, accessToken);
			String docXmlStr = OCUtil.convertDocToString(documentODM);
			System.out.println(docXmlStr);

			XPath xpath = XPathFactory.newInstance().newXPath();

			String evalSE = ".//StudyEventData[(@StudyEventOID='SE_PRESURGERYTIMEPOINT' or @StudyEventOID='SE_SURGERY'"
					+ " or @StudyEventOID='SE_30DAYFOLLOWUP' or @StudyEventOID='SE_3MONTHFOLLOWUP' "
					+ " or @StudyEventOID='SE_6MONTHFOLLOWUP' or @StudyEventOID='SE_9MONTHFOLLOWUP' "
					+ " or @StudyEventOID='SE_1YEARFOLLOWUP' or @StudyEventOID='SE_2YEARFOLLOWUP' "
					+ " or @StudyEventOID='SE_3YEARFOLLOWUP' or @StudyEventOID='SE_4YEARFOLLOWUP' "
					+ " or @StudyEventOID='SE_5YEARFOLLOWUP' or @StudyEventOID='SE_6YEARFOLLOWUP' "
					+ " or @StudyEventOID='SE_7YEARFOLLOWUP' or @StudyEventOID='SE_8YEARFOLLOWUP' "
					+ " or @StudyEventOID='SE_9YEARFOLLOWUP' or @StudyEventOID='SE_10YEARFOLLOWUP' "
					+ " or @StudyEventOID='SE_15YEARFOLLOWUP' or @StudyEventOID='SE_25YEARFOLLOWUP' "
					+ " or @StudyEventOID='SE_3_5YEARFOLLOWUP' or @StudyEventOID='SE_4_5YEARFOLLOWUP' or @StudyEventOID='SE_STUDYACCYCLES') "
					+ " and ( @Status = 'completed' or @Status = 'data entry started')]";

			NodeList skipSEs = (NodeList) xpath.evaluate(evalSE, documentODM, XPathConstants.NODESET);

			if (skipSEs != null && skipSEs.getLength() > 0) {
				throw new Exception("SKIP_STUDY_EVENTS");
			}

			boolean crfCheck = false;
			Node timepointStudyEvent = (Node) xpath.evaluate(".//StudyEventData[@StudyEventOID='" + timepointOid + "']",
					documentODM, XPathConstants.NODE);
			List<String> studyList = null;
			if (!crfCheck && timepointStudyEvent != null) {
				if (timepointOid.equalsIgnoreCase("SE_EARLYTREATMENT")) {
					studyList = earlySEList;
					formList = eaFormList;

					// Check if SE_EARLYTREATMENT exists and not completed

					Node mriWeek6SE = (Node) xpath.evaluate(
							".//StudyEventData[@StudyEventOID='SE_MRIWEEK6' and @OpenClinica:Status != 'completed']",
							documentODM, XPathConstants.NODE);
					Node interRegSE = (Node) xpath.evaluate(
							".//StudyEventData[@StudyEventOID='SE_INTERREGIMEN' and @OpenClinica:Status != 'completed']",
							documentODM, XPathConstants.NODE);
					// Check if SE_MRIWEEK6 and SE_INTERREGIMEN is not scheduled
					if (mriWeek6SE == null && interRegSE == null) {
						crfCheck = true;
						System.out.println(subjectId + "SE_MRIWEEK6 and SE_INTERREGIMEN not scheduled");
					}

				} // if SE_EARLYTIMEPOINT
				else if (timepointOid.equalsIgnoreCase("SE_MRIWEEK6")) {
					studyList = sixWkSEList;
					formList = wk6FormList;

					Node interregimenSE = (Node) xpath.evaluate(
							".//StudyEventData[@StudyEventOID='SE_INTERREGIMEN' and @OpenClinica:Status != 'completed']",
							documentODM, XPathConstants.NODE);
					// Check if SE_INTERREGIMEN is not scheduled
					if (interregimenSE == null) {
						crfCheck = true;
						System.out.println(subjectId + "SE_INTERREGIMEN not scheduled");

					}

				} else if (timepointOid.equalsIgnoreCase("SE_INTERREGIMEN")) {
					studyList = interRegSEList;
					formList = irformList;

					Node surgerySE = (Node) xpath.evaluate(
							".//StudyEventData[@StudyEventOID='SE_SURGERY' and @OpenClinica:Status != 'completed']",
							documentODM, XPathConstants.NODE);
					// Check if SE_SURGERY is not scheduled
					if (surgerySE == null) {
						crfCheck = true;
						System.out.println(subjectId + ":SE_SURGERY not scheduled");

					}
				}
			}

			if (crfCheck)
				crfCheck = rorCrfCheck(documentODM, timepointStudyEvent, timepointOid, subjectOID);

			boolean add = false;

			Node subjectNode = getSubjectNode(documentODM);
			Node clinicalData = subjectNode.getParentNode();
			Node odm = clinicalData.getParentNode();

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document newDoc = builder.newDocument();

			odmNew = newDoc.importNode(odm, false);
			newDoc.appendChild(odmNew);

			clinDataNew = newDoc.importNode(clinicalData, false);
			odmNew.appendChild(clinDataNew);

			subNew = newDoc.importNode(subjectNode, false);
			clinDataNew.appendChild(subNew);
			String version;
			NodeList studEventNodes = (NodeList) xpath.evaluate("//StudyEventData", documentODM,
					XPathConstants.NODESET);

			String formOidKey;
			for (int i = 0; i < studEventNodes.getLength(); i++) {
				studyEvent = studEventNodes.item(i);
				studyEventOid = studyEvent.getAttributes().getNamedItem("StudyEventOID").getNodeValue();

				if (studyList != null && studyList.contains(studyEventOid)) {

					NodeList formNodeList = (NodeList) xpath.evaluate("./FormData", studyEvent, XPathConstants.NODESET);

					for (int j = 0; j < formNodeList.getLength(); j++) {
						formNode = formNodeList.item(j);

						formOid = formNode.getAttributes().getNamedItem("FormOID").getNodeValue();
						version = formNode.getAttributes().getNamedItem("OpenClinica:FormLayoutOID").getNodeValue();

						formOidKey = formOid;
						System.out.println(subjectId + ":StudyEventOid=" + studyEventOid + ":FormOid=" + formOid);
						if (formList.contains(formOid)) {

							NodeList discrepancyNotes = (NodeList) xpath.evaluate(
									"./ItemGroupData/ItemData/DiscrepancyNotes/DiscrepancyNote[(@Status='New' or @Status='Updated') and @NoteType='Query']",
									formNode, XPathConstants.NODESET);
							String itemDataValue = NO;
							if (formOid.equalsIgnoreCase("F_RE_CONSENT")) {
								NodeList reconsentTypeNodeList = (NodeList) xpath.evaluate(
										"./ItemGroupData/ItemData[@ItemOID='I_RE_CO_RECONSENT_TYPE']", formNode,
										XPathConstants.NODESET);

								for (int k = 0; k < reconsentTypeNodeList.getLength(); k++) {
									add = false;
									Node reconsentTypeNode = reconsentTypeNodeList.item(k);
									String val = reconsentTypeNode.getAttributes().getNamedItem("Value").getNodeValue();
									NodeList demList = (NodeList) xpath.evaluate(
											".//FormData[@FormOID='F_PATIENTDEMOG' and @FormLayoutOID<3]", documentODM,
											XPathConstants.NODESET);
									// look at screening
									if (val != null && (val.equalsIgnoreCase("Screening_Consent")
											|| val.equalsIgnoreCase("Site_Specific_Screening_Consent"))) {
										if (demList != null && demList.getLength() > 0) {
											if (val.equalsIgnoreCase("Screening_Consent"))
												formOidKey = formOid + "_SCREENING";
											else if (val.equalsIgnoreCase("Site_Specific_Screening_Consent"))
												formOidKey = formOid + "_SITE_SCREENING";

											if (discrepancyNotes != null && discrepancyNotes.getLength() > 0) {
												itemDataValue = YES;
												add = true;
											} else {
												itemDataValue = NO;
												add = true;
											}
										} else
											continue;

									} else if (val != null && val.equalsIgnoreCase("Treatment_Consent")) {
										NodeList randResList = (NodeList) xpath.evaluate(
												".//FormData[@FormOID='F_RANDOMIZATIO_5299' and @FormLayoutOID<4]",
												documentODM, XPathConstants.NODESET);
										if (randResList != null && randResList.getLength() > 0) {
											formOidKey = formOid + "_TREATMENT";

											if (discrepancyNotes != null && discrepancyNotes.getLength() > 0) {
												itemDataValue = YES;
												add = true;
											} else {
												itemDataValue = NO;
												add = true;
											}
										} else
											continue;

									}

									// }

								}
							} else if (formOid.equalsIgnoreCase("F_MRIVOLUME")) {
								if (studyEventOid.equalsIgnoreCase("SE_EARLYTREATMENT")) {
									formOidKey = formOid + "_EARLY";
								} else if (studyEventOid.equalsIgnoreCase("SE_MRIWEEK6")) {
									formOidKey = formOid + "_6WEEK";
								} else if (studyEventOid.equalsIgnoreCase("SE_INTERREGIMEN")) {
									formOidKey = formOid + "_INTERREGIMEN";
								} else
									continue;

								if (discrepancyNotes != null && discrepancyNotes.getLength() > 0) {
									itemDataValue = YES;
									add = true;
								} else {
									itemDataValue = NO;
									add = true;
								}

							} else if (formOid.equalsIgnoreCase("F_RANDOMIZATIO_5299")) {
								// String ver =
								// formNode.getAttributes().getNamedItem("OpenClinica:FormLayoutOID").getNodeValue();
								if (Float.parseFloat(version) >= 4) {
									if (discrepancyNotes != null && discrepancyNotes.getLength() > 0) {
										itemDataValue = YES;
										add = true;
									} else {
										itemDataValue = NO;
										add = true;
									}
								} else {
									continue;
								}

							} else if (formOid.equalsIgnoreCase("F_INTERREGIMEN")
									|| formOid.equalsIgnoreCase("F_INTERPATHO")) {
								if (studyEventOid.equalsIgnoreCase("SE_INTERREGIMEN")) {
									if (discrepancyNotes != null && discrepancyNotes.getLength() > 0) {
										itemDataValue = YES;
										add = true;
									} else {
										itemDataValue = NO;
										add = true;
									}
								} else
									continue;
							} else if (formOid.equalsIgnoreCase("F_DISEASEASSES")
									|| formOid.equalsIgnoreCase("F_ONSTUDYPATHO")) {
								if (studyEventOid.equalsIgnoreCase("SE_SCREENING")) {
									if (discrepancyNotes != null && discrepancyNotes.getLength() > 0) {
										itemDataValue = YES;
										add = true;
									} else {
										itemDataValue = NO;
										add = true;
									}
								} else
									continue;
							} else
								continue;

							if (add) {

								studyEventNew = (Node) xpath.evaluate(
										"./StudyEventData[@StudyEventOID='" + timepointOid + "']", subNew,
										XPathConstants.NODE);

								if (studyEventNew == null) {
									studyEventNew = newDoc.importNode(timepointStudyEvent, false);
									subNew.appendChild(studyEventNew);
								}
								rorLetterformData = (Node) xpath.evaluate("./FormData[@FormOID='F_RETURNOFRESU']",
										studyEventNew, XPathConstants.NODE);
								rorLetterformDataOrig = (Node) xpath.evaluate("./FormData[@FormOID='F_RETURNOFRESU']",
										timepointStudyEvent, XPathConstants.NODE);
								if (rorLetterformData == null) {
									if (rorLetterformDataOrig != null) // import FormData of F_RETURNOFRESU if it exists
																		// for studyEvent
										rorLetterformData = newDoc.importNode(rorLetterformDataOrig, false);
									else
										rorLetterformData = createRoRLetterFormData(newDoc);
									studyEventNew.appendChild(rorLetterformData);

								}

								rorLetterItemGroupData = (Node) xpath.evaluate(
										"./ItemGroupData[@ItemGroupOID='IG_RETUR_ROR']", rorLetterformData,
										XPathConstants.NODE);
								if (rorLetterItemGroupData == null) {
									rorLetterItemGroupData = createItemGroupData(newDoc, "IG_RETUR_ROR", "ROR");
									rorLetterformData.appendChild(rorLetterItemGroupData);
								}
								Node itemDataForLetter = createItemData(newDoc, formToItemOid.get(formOidKey),
										formToItemName.get(formOidKey), itemDataValue);
								rorLetterItemGroupData.appendChild(itemDataForLetter);

								if (itemDataValue != null) {
									String itemDataTimeValue = findLastUpdated(formNode, itemDataValue);
									if (itemDataTimeValue != null) {
										Node itemDataUpdatedDate = createItemData(newDoc,
												"I_RETUR_" + formToTime.get(formOidKey), formToTime.get(formOidKey),
												itemDataTimeValue);
										rorLetterItemGroupData.appendChild(itemDataUpdatedDate);
									}

								}

							}

						} // if(formToItem.containsKey(formOid)
					} // for(formList)
				} // if in studyevent list
			} // for(studyEvent

		} catch (Exception e) {
			e.printStackTrace();
		}

		return subNew;

	}

	private static String findLastUpdated(Node formNode, String dataValue) throws Exception {
		final String NO = "No open queries";
		final String YES = "Open queries";
		DateFormat dateDf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");

		XPath xpath = XPathFactory.newInstance().newXPath();
		NodeList nodeList = null;

		if (dataValue.equalsIgnoreCase(YES)) {
			nodeList = (NodeList) xpath.evaluate(
					"./ItemGroupData/ItemData/DiscrepancyNotes/DiscrepancyNote[(@Status='New' or @Status='Updated') and @NoteType='Query']",
					formNode, XPathConstants.NODESET);
		} else {
			nodeList = (NodeList) xpath.evaluate(
					"./ItemGroupData/ItemData/DiscrepancyNotes/DiscrepancyNote[@NoteType='Query']", formNode,
					XPathConstants.NODESET);

		}
		String dateTimeStr = null;
		String dateTimeLatest = null;
		if (nodeList != null && nodeList.getLength() > 0) {
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node n = nodeList.item(i);
				dateTimeStr = n.getAttributes().getNamedItem("DateUpdated").getNodeValue();
				if (dateTimeLatest == null || (dateDf.parse(dateTimeStr).after(dateDf.parse(dateTimeLatest))))
					dateTimeLatest = dateTimeStr;
			}
		}
		return dateTimeLatest;
	}

	public static Node findStudyEventsForPSS(String dbKey, String subjectId) {
		OCUtil util = new OCUtil();
		String username = util.getAppProperties().getProperty("username");
		String password = util.getAppProperties().getProperty("password");
		// String subId = SubjectData.getSubjectIdForKey(subjectId);

		String subId = "SS_" + subjectId;
		System.out.println("key:" + subjectId + ", subjectId:" + subId);

		String accessToken = OCUtil.getAccessToken("qlhc2.build.openclinica.io", username, password); // username/password
																										// need to be
																										// for OC data
																										// manager,
																										// vandhana may
																										// already have
																										// them in a
																										// properties
																										// file
		String url = util.getAppProperties().getProperty("odmUrlWithSubjectId");
		String urlODM = String.format(url, subId);

		List<Node> studyEventList = new ArrayList<Node>();
		Document documentODM = null;
		Element subject = null;

		Connection connection = null;
		PreparedStatement statementCRFs = null;
		ResultSet rs = null;

		try {

			connection = DBCPDataSource.getConnection();
			connection.setAutoCommit(true);

			String strstmt = "";

			strstmt = "select studyeventoid, start_date, reason from pss_inserts where subjectid = ?";
			statementCRFs = connection.prepareStatement(strstmt);
			statementCRFs.setString(1, dbKey);

			rs = statementCRFs.executeQuery();

			String studyeventoid, startDate, instance, reason = "";
			Node studyEvent = null;
			documentODM = OCUtil.getXML(urlODM, accessToken);

			while (rs.next()) {
				studyeventoid = rs.getString("studyeventoid");
				startDate = rs.getString("start_date");
				reason = rs.getString("reason");

				XPath xpath = XPathFactory.newInstance().newXPath();
				NodeList nodes = (NodeList) xpath.evaluate("//StudyEventData[@StudyEventOID='" + studyeventoid + "']",
						documentODM, XPathConstants.NODESET);
				if (nodes.getLength() > 0) {
					for (int i = 0; i < nodes.getLength(); i++) {
						studyEvent = nodes.item(i);
						String startDateS = studyEvent.getAttributes().getNamedItem("OpenClinica:StartDate") != null
								? studyEvent.getAttributes().getNamedItem("OpenClinica:StartDate").getNodeValue()
								: "";
						System.out.println("StudyEvent startDate: " + startDateS);

						studyEvent.getParentNode().removeChild(studyEvent);
						studyEvent = createStudyEventForPSS(documentODM, studyeventoid, reason, startDateS);
						studyEventList.add(studyEvent);
					}
				} else { // No StudyEvent
					studyEvent = createStudyEventForPSS(documentODM, studyeventoid, reason, "");
					studyEventList.add(studyEvent);
				}

			}

			if (documentODM != null) {
				subject = documentODM.createElement("SubjectData");
				subject.setAttribute("SubjectKey", subId);
				subject.setAttribute("OpenClinica:StudySubjectID", subjectId);
				subject.setAttribute("OpenClinica:Status", "Available");

				for (Node studyEventNode : studyEventList) {
					subject.appendChild(studyEventNode);
				}
			}

		} catch (Exception e) {

			e.printStackTrace();
		} finally {
			try {
				if (null != rs) {
					rs.close();
					statementCRFs.close();

				}
				if (null != connection) {
					connection.close();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return subject;

	}

	static void importTimepoints(String importTable) {
		OCUtil util = new OCUtil();
		String username = util.getAppProperties().getProperty("username");
		String password = util.getAppProperties().getProperty("password");

		Connection connection = null;
		PreparedStatement statementCRFs = null;
		ResultSet rs = null;

		try {

			connection = DBCPDataSource.getConnection();
			connection.setAutoCommit(true);

			String strstmt = "";

			strstmt = "select importXml, subjectidlist from " + importTable + " where status = 'N'";
			statementCRFs = connection.prepareStatement(strstmt);

			rs = statementCRFs.executeQuery();

			String jobId = null;
			while (rs.next()) {
				String xml = rs.getString("importXml");
				String subjectidlist = rs.getString("subjectidlist");
				String pid = "";
				if (subjectidlist.contains("|"))
					pid = subjectidlist.split("\\|")[0];
				else
					pid = subjectidlist;

				jobId = importODMXml(xml, username, password, pid);
				if (jobId != null) {
					System.out.println("Import done for jobid:" + jobId);
					if (jobId.indexOf("false") > -1)
						strstmt = "UPDATE " + importTable
								+ " SET status='E', create_date = now(), jobId=? where subjectidlist  = ?";
					else
						strstmt = "UPDATE " + importTable
								+ " SET status='C', create_date = now(), jobId=? where subjectidlist  = ?";

					updateImportDB(strstmt, pid, jobId, null);
				} else {
					System.out.println("importTimepoints jobId is null ");
				}

			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		finally {
			try {
				if (null != rs) {
					rs.close();
					statementCRFs.close();

				}
				if (null != connection) {
					connection.close();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	static void importReconsent() {
		OCUtil util = new OCUtil();
		String username = util.getAppProperties().getProperty("username");
		String password = util.getAppProperties().getProperty("password");

		Connection connection = null;
		PreparedStatement statementCRFs = null;
		ResultSet rs = null;

		try {

			connection = DBCPDataSource.getConnection();
			connection.setAutoCommit(true);

			String strstmt = "";

			strstmt = "select importXml, subjectid, study_event_repeat_key from public.reconsentimport where status = 'N'";
			statementCRFs = connection.prepareStatement(strstmt);

			rs = statementCRFs.executeQuery();

			String jobId = null;
			while (rs.next()) {
				String xml = rs.getString("importXml");
				String subjectid = rs.getString("subjectid");
				String seKey = rs.getString("study_event_repeat_key");

				jobId = importODMXml(xml, username, password, subjectid);
				if (jobId != null) {
					System.out.println("Import done for jobid:" + jobId);
					if (jobId.indexOf("false") > -1)
						strstmt = "UPDATE public.reconsentimport  SET status='E', create_date = now(), jobId=? where subjectid  = ? and study_event_repeat_key = ?";
					else
						strstmt = "UPDATE public.reconsentimport  SET status='C', create_date = now(), jobId=? where subjectid  = ? and study_event_repeat_key = ?";

					updateImportDB(strstmt, subjectid, jobId, seKey);
				} else {
					System.out.println("importTimepoints jobId is null ");
				}

			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		finally {
			try {
				if (null != rs) {
					rs.close();
					statementCRFs.close();

				}
				if (null != connection) {
					connection.close();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	static List<Map<String, String>> selectRowsForMri(String sqlStr, String pid) {
		List<Map<String, String>> lst = new ArrayList<Map<String, String>>();

		Connection connection = null;
		PreparedStatement statementCRFs = null;
		ResultSet rs = null;
		try {
			connection = DBCPDataSource.getConnection();
			connection.setAutoCommit(true);

			statementCRFs = connection.prepareStatement(sqlStr);
			statementCRFs.setString(1, pid);
			rs = statementCRFs.executeQuery();

			while (rs.next()) {
				String subjectid = rs.getString("subjectid");
				String studyeventoid = rs.getString("studyeventoid");
				String instance = rs.getString("instance");
				String reason = rs.getString("reason");
				String start_date = rs.getString("start_date");
				String status = rs.getString("status");
				String create_date = rs.getString("create_date");
				System.out.println("PID=" + subjectid + ",studyeventoid:" + studyeventoid);

				lst.add(Map.of("PID", subjectid, "STUDYEVENTOID", studyeventoid, "INSTANCE", reason, "REASON", reason,
						"START_DATE", start_date));

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();

				}
				if (statementCRFs != null) {
					statementCRFs.close();

				}
				if (null != connection) {
					connection.close();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return lst;

	}

	static List<Map<String, String>> selectRows(String sqlStr, String pid) {
		List<Map<String, String>> lst = new ArrayList<Map<String, String>>();

		Connection connection = null;
		PreparedStatement statementCRFs = null;
		ResultSet rs = null;
		try {
			connection = DBCPDataSource.getConnection();
			connection.setAutoCommit(true);

			statementCRFs = connection.prepareStatement(sqlStr);
			statementCRFs.setString(1, pid);
			rs = statementCRFs.executeQuery();

			while (rs.next()) {
				String subjectid = rs.getString("subjectid");
				String timepoint = rs.getString("timepoint");
				String mapsToTimepoint = rs.getString("maps_to_timepoint");
				String startDate = rs.getString("start_date");
				String createDate = rs.getString("create_date");
				System.out.println("PID=" + subjectid + ",timepoint:" + timepoint + ",mapsToTimepoint:"
						+ mapsToTimepoint + ",startDate:" + startDate + ",createDate:" + createDate);

				lst.add(Map.of("PID", subjectid, "TIMEPOINT", timepoint, "MAPS_TO_TIMEPOINT", mapsToTimepoint,
						"START_DATE", startDate, "CURRENT_DATE", createDate));

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();

				}
				if (statementCRFs != null) {
					statementCRFs.close();

				}
				if (null != connection) {
					connection.close();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return lst;

	}

	public static String importODMXml(String uploadStr, String username, String password, String subId) {
		String accessToken = OCUtil.getAccessToken("qlhc2.build.openclinica.io", username, password); // username/password
																										// need to be
																										// for OC data
																										// manager,
																										// vandhana may
																										// already have
																										// them in a
																										// properties
																										// file
		String jobId = "";
		try {

			System.out.println(uploadStr);

			Random random = new Random();
			int x = random.nextInt(900) + 100;
			String importName = subId + "_" + x + ".xml";
			System.out.println("importing this xml for subjectId:" + importName);

			jobId = OCUtil.importData(accessToken, uploadStr, importName);

		} catch (Exception e) {
			e.printStackTrace();

		}
		return jobId;

	}

	public static void updateSubSurg() {
		OCUtil.migrateSubsequentSugData();
	}

	public static void updateDAData() throws Exception {
		// OCUtil.migrateData();
		// OCUtil.splitXml();
		// OCUtil.readAndImport();

	}
	
	public static String getODMXml(String subjectId) throws Exception {
		OCUtil util = new OCUtil();
	
		String username = util.getAppProperties().getProperty("username");
		String password = util.getAppProperties().getProperty("password");

		String accessToken = OCUtil.getAccessToken("qlhc2.build.openclinica.io", username, password);
		String url = util.getAppProperties().getProperty("odmUrlWithSubjectId");
		String urlODM = String.format(url, subjectId);

		Document documentODM  = OCUtil.getXML(urlODM, accessToken);
		StringBuffer buff = new StringBuffer();
		JaxbUtil.getXMLString(documentODM, true, buff, true);
		String odmXml = buff.toString();
		return  odmXml;
	}

}
