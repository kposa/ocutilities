package com.quantumleaphealth.oc.utils;




import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.quantumleaphealth.oc.db.DBCPDataSource;
import com.quantumleaphealth.oc.model.Odm;



@SpringBootApplication
public class SdvUtilApplication {
	
	@Autowired
	private Environment env;
	
	 @Autowired
	    private ConfigurableApplicationContext context;

	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(SdvUtilApplication.class);
		application.setAddCommandLineProperties(false);
		application.run(args);
	}
	
	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
			
			
			//List<String> list = new ArrayList<String>(Arrays.asList("09925|SE_MRIWEEK6") );
							
			//SdvUtil.validateAndInsertImport(list, 1);
				
					
			//SdvUtil.insertIntoImportPatDemReconsentBySelectAllN("reconsentInserts");
			
			//SdvUtil.importReconsent();
			
			String xml = SdvUtil.getODMXml("SS_12852");
			System.out.println(xml);			
			
			
			//SdvUtil.insertIntoImportPatDemReconsent(patSubIdList, "patDemoInserts");
			//SdvUtil.importTimepoints("patDemoImport");
			
			
			
		};
	}
	
	/*

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
			
			if(args.length > 0 ) {
				if(args[0].equalsIgnoreCase("ROR_INSERT"))
					InsightUtil.insertRorDataForEarlyTreatment();
				else if(args[0].equalsIgnoreCase("ROR_INSERT_IMPORTS")) {
					OCUtil util = new OCUtil();
					int  batchCount = Integer.parseInt(util.getAppProperties().getProperty("SUBJECT_BATCH_COUNT"));
					List<String> subKeyList = getUniquePids();			
					SdvUtil.validateAndInsertImport(subKeyList, batchCount);
					
				}
				else if(args[0].equalsIgnoreCase("ROR_IMPORT")) {
					SdvUtil.importTimepoints("rorImportbatch");
				}
				else if(args[0].equalsIgnoreCase("ROR_ALL")) {
					InsightUtil.insertRorDataForEarlyTreatment();
					
					OCUtil util = new OCUtil();
					int  batchCount = Integer.parseInt(util.getAppProperties().getProperty("SUBJECT_BATCH_COUNT"));
					List<String> subKeyList = getUniquePids();			
					SdvUtil.validateAndInsertImport(subKeyList, batchCount);
					
					SdvUtil.importTimepoints("rorImportbatch");
				}
				else {
					System.out.println("Arguments required [ROR_INSERT/ROR_INSERT_IMPORTS/ROR_IMPORT/ROR_ALL");
					
				}
				
				System.out.println("DONEEEEEEEEE");
				System.exit(SpringApplication.exit(context));
			}
			else {
				System.out.println("Arguments required [ROR_INSERT/ROR_INSERT_IMPORTS/ROR_IMPORT/ROR_ALL");
				System.exit(SpringApplication.exit(context));
			}
			
			
		};
	}
	*/
	
	public List<String> getUniquePids() {
		Connection connection = null;
		PreparedStatement statementCRFs = null;
		ResultSet rs = null;
		List<String> pids = new ArrayList<String>();

		try {

			connection = DBCPDataSource.getConnection();
			connection.setAutoCommit(true);
			
			String strstmt = "select distinct(subjectid), timepointoid  from ror_inserts where status = 'N' order by subjectid ";			
			statementCRFs = connection.prepareStatement(strstmt);

			rs = statementCRFs.executeQuery();
			
			while (rs.next()) {
				String pid = rs.getString("subjectid");	
				String timepointoid = rs.getString("timepointoid");	
				System.out.println("PID|TIMEPOINTOID="+ pid + "|" + timepointoid);
				pids.add(pid + "|" + timepointoid);

			}

		}
		 catch (Exception ex) {
				ex.printStackTrace();
			}

			finally {
				try {
					if (null != rs) {
						rs.close();
						statementCRFs.close();

					}
					if (null != connection) {
						connection.close();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		return pids;

	}

}
