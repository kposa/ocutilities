package com.quantumleaphealth.oc.utils;

public class EopEligibilityData {
	
	private String mp;
	private String her2Overall;
	private String her2Eligible;
	private String hrOverall;
	public String getMp() {
		return mp;
	}
	public void setMp(String mp) {
		this.mp = mp;
	}
	public String getHer2Overall() {
		return her2Overall;
	}
	public void setHer2Overall(String her2Overall) {
		this.her2Overall = her2Overall;
	}
	public String getHer2Eligible() {
		return her2Eligible;
	}
	public void setHer2Eligible(String her2Eligible) {
		this.her2Eligible = her2Eligible;
	}
	public String getHrOverall() {
		return hrOverall;
	}
	public void setHrOverall(String hrOverall) {
		this.hrOverall = hrOverall;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((her2Eligible == null) ? 0 : her2Eligible.hashCode());
		result = prime * result + ((her2Overall == null) ? 0 : her2Overall.hashCode());
		result = prime * result + ((hrOverall == null) ? 0 : hrOverall.hashCode());
		result = prime * result + ((mp == null) ? 0 : mp.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EopEligibilityData other = (EopEligibilityData) obj;
		if (her2Eligible == null) {
			if (other.her2Eligible != null)
				return false;
		} else if (!her2Eligible.equals(other.her2Eligible))
			return false;
		if (her2Overall == null) {
			if (other.her2Overall != null)
				return false;
		} else if (!her2Overall.equals(other.her2Overall))
			return false;
		if (hrOverall == null) {
			if (other.hrOverall != null)
				return false;
		} else if (!hrOverall.equals(other.hrOverall))
			return false;
		if (mp == null) {
			if (other.mp != null)
				return false;
		} else if (!mp.equals(other.mp))
			return false;
		return true;
	}

}
