package com.quantumleaphealth.oc.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.Normalizer.Form;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.quantumleaphealth.oc.db.DBCPDataSource;
import com.quantumleaphealth.oc.model.FormData;
import com.quantumleaphealth.oc.model.ItemData;
import com.quantumleaphealth.oc.model.ItemGroupData;
import com.quantumleaphealth.oc.model.JaxbUtil;
import com.quantumleaphealth.oc.model.Odm;
import com.quantumleaphealth.oc.model.SubjectData;

import org.apache.commons.lang3.text.StrSubstitutor;
import org.json.JSONArray;

@Component
public class InsightUtil implements EnvironmentAware {
	

	@Autowired
	public static  Environment env;
	
	public static  String getConfigProp(String key) {
        return env.getProperty(key);
    }
    @Override
    public void setEnvironment(Environment arg0) {
        if(env==null){
        	env=arg0;
        }

    }
	
	public static void insertRorDataForEarlyTreatment() throws Exception {
		OCUtil util = new OCUtil();
		
		String prod = util.getAppProperties().getProperty("PROD_RUN");
		String eaKey, irKey, wk6Key;
		if("true".equalsIgnoreCase(prod)) {
			eaKey = "insight_ROR_EA-prod";
			irKey = "insight_ROR_IR-prod";
			wk6Key = "insight_ROR_WK6-prod";
		}
		else {
			eaKey = "insight_ROR_EA-test";
			irKey = "insight_ROR_IR-test";
			wk6Key = "insight_ROR_WK6-test";
		}
		String host = util.getAppProperties().getProperty("insight-host");
		String username = util.getAppProperties().getProperty("insight-username");
		String password = util.getAppProperties().getProperty("insight-password");
		
		String eaKeyData = util.getAppProperties().getProperty(eaKey);
		String irKeyData = util.getAppProperties().getProperty(irKey);
		String wk6KeyData = util.getAppProperties().getProperty(wk6Key);
		
		JSONArray eaData = util.queryData(eaKeyData, username, password, host);
		List<String> eaPids = util.getStrListFromJsonArray(eaData, "Participant ID");
		//System.out.println(eaPids);
		
		String insertstmtStr = "INSERT INTO ror_inserts (subjectid, timepointoid, status, create_date) VALUES ('%s', '%s', 'N', now())";
		String insertSmt = "";
		
		for(String pid:eaPids) {
			pid = pid.replace("RoR", "");
			insertSmt = String.format(insertstmtStr, pid, "SE_EARLYTREATMENT");
			SdvUtil.insertDB(insertSmt);	
		}
		
		JSONArray irData = util.queryData(irKeyData, username, password, host);
		List<String> irPids = util.getStrListFromJsonArray(irData, "Participant ID");
		
		for(String pid:irPids) {
			pid = pid.replace("RoR", "");
			insertSmt = String.format(insertstmtStr, pid, "SE_INTERREGIMEN");
			SdvUtil.insertDB(insertSmt);	
		}
		
		
		JSONArray sixData = util.queryData(wk6KeyData, username, password, host);
		List<String> sixPids = util.getStrListFromJsonArray(sixData, "Participant ID");
		
		for(String pid:sixPids) {
			pid = pid.replace("RoR", "");
			insertSmt = String.format(insertstmtStr, pid, "SE_MRIWEEK6");
			SdvUtil.insertDB(insertSmt);	
		}
		
	}
	
	

}
