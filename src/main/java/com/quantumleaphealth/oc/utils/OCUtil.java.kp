package com.quantumleaphealth.oc.sdvUtil;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;

public class OCUtil
{
	private OCUtil()
	{

	}

	private static final javax.xml.namespace.NamespaceContext NamespaceContext = new NamespaceContext()
	{
		public String getNamespaceURI(String prefix)
		{
			return prefix.equals("OpenClinica") ? "http://www.openclinica.org/ns/odm_ext_v130/v3.1" : null;
		}

		public Iterator getPrefixes(String val)
		{
			return null;
		}

		public String getPrefix(String uri)
		{
			return null;
		}
	};

	static Document getXML(String urlString, String accessToken) throws Exception
	{
		Document xmlDocument = null;

		String resultString = httpGetString(urlString, accessToken, "application/xml");

		if (resultString != null)
		{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			//factory.setNamespaceAware(true);
			DocumentBuilder builder = factory.newDocumentBuilder();
			xmlDocument = builder.parse(new ByteArrayInputStream(resultString.getBytes("UTF-8")));
		}

		return xmlDocument;
	}

	static String httpGetString(String urlString, String accessToken, String accept)
	{
		try
		{
			URL url = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setDoOutput(true);
			connection.setRequestProperty("Authorization", String.format("Bearer %s", accessToken));
			connection.setRequestProperty("accept", accept);

			if (connection.getResponseCode() == 200)
			{
				Reader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				for (int c; (c = reader.read()) >= 0;)
				{
					sb.append((char) c);
				}

				return sb.toString();
			}
			else
			{
				Reader reader = new BufferedReader(new InputStreamReader(connection.getErrorStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				for (int c; (c = reader.read()) >= 0;)
				{
					sb.append((char)c);
				}

				System.out.println("httpGetString ERROR: bad response code " + connection.getResponseCode());// + " " + sb.toString());
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

		return null;
	}

	static String getAccessToken(String hostname,
										 String username,
										 String password)
	{
		try
		{
			//System.out.println("getAccessToken begin");

			JSONObject requestJSON = new JSONObject();
			requestJSON.put("username", username);
			requestJSON.put("password", password);
			byte[] requestBytes = requestJSON.toString().getBytes("UTF-8");

			//System.out.println(requestJSON.toString());

			URL url = new URL(String.format("https://%s/user-service/api/oauth/token", hostname));
			//System.out.println(url.toString());
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("Content-Length", String.valueOf(requestBytes.length));
			connection.setRequestProperty("Accept", "*/*");
			connection.setDoOutput(true);

			connection.getOutputStream().write(requestBytes);

			if (connection.getResponseCode() == 200)
			{
				Reader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				for (int c; (c = reader.read()) >= 0;)
				{
					sb.append((char) c);
				}

				return sb.toString();
			}
			else
			{
				Reader reader = new BufferedReader(new InputStreamReader(connection.getErrorStream(), "UTF-8"));
				StringBuilder sb = new StringBuilder();
				for (int c; (c = reader.read()) >= 0;)
				{
					sb.append((char) c);
				}

				System.out.println("ERROR: bad response code " + connection.getResponseCode() + ", " + sb.toString());
			}
		}
		catch (Exception ex)
		{
			System.out.println("ERROR: " + ex.getMessage());
		}

		return null;
	}

	static String importData(String accessToken, String xmlData) throws Exception
	{
		System.out.println("importData begin");


		HttpPost post = new HttpPost("https://qlhc2.openclinica.io/OpenClinica/pages/auth/api/clinicaldata/import");
		post.addHeader("Authorization", String.format("Bearer %s", accessToken));

		//HttpPost post = new HttpPost("http://ptsv2.com/t/nd3ss-1580325822/post");

		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
		builder.addBinaryBody("file", xmlData.getBytes(), ContentType.TEXT_XML, "import.xml");
		HttpEntity entity = builder.build();
		post.setEntity(entity);

		CloseableHttpClient client = HttpClients.createDefault();
		CloseableHttpResponse response = client.execute(post);
		//client.close();
		System.out.println(response.getStatusLine().getStatusCode());
		String jobId = null;
		if (response.getStatusLine().getStatusCode() == 200)
		{

			Reader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
			StringBuilder sb = new StringBuilder();
			for (int c; (c = reader.read()) >= 0;)
			{
				sb.append((char) c);
			}

			jobId =  sb.toString().replace("job uuid: ", "");
		}
		
		System.out.println("JobId=" + jobId);
		

		for (int i = 0; i < 10; i++){
		   Thread.sleep(2000);
		   String jobResult = OCUtil.httpGetString(String.format("https://qlhc2.openclinica.io/OpenClinica/pages/auth/api/jobs/%s/downloadFile", jobId), accessToken, "*/*");

		   if (jobResult!= null && jobResult.indexOf("Updated") > -1){
			   System.out.println("Job successfully completed");
		       break;
		   }
		}
		return jobId;

	}
	
	public static String convertDocToString(Document doc) {
		String output = null;
		try {
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			output = writer.getBuffer().toString().replaceAll("\n|\r", "");
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		return output;
	}
		
	public static String getTagValue(Document xmlDoc, String tagName) throws Exception {
	        xmlDoc.getDocumentElement().normalize();
	        String tagValue = null;
	        NodeList nodeList = xmlDoc.getElementsByTagName(tagName);

	        for (int temp = 0; temp < nodeList.getLength(); temp++) {
	            Node nNode = nodeList.item(temp);
	            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	                org.w3c.dom.Element eElement = (org.w3c.dom.Element) nNode;
	                tagValue = eElement.getFirstChild().getNodeValue();
	                break;
	            }
	        }
	        return tagValue;
	  }
	
	public static void setNodeAttrValue(Document xmlDoc, String selAttributeStr, String selAttrVal, String attrToChange, String newVal) throws XPathExpressionException {
		XPath xpath = XPathFactory.newInstance().newXPath();
		StringBuilder str = new StringBuilder();
		str.append("//*[contains(@").append(selAttributeStr).append(", '").append(selAttrVal).append("')]");
		String xpathStr = str.toString();
		NodeList nodes = (NodeList)xpath.evaluate(xpathStr, xmlDoc, XPathConstants.NODESET);
	
		for (int idx = 0; idx < nodes.getLength(); idx++) {
			NamedNodeMap attributes = nodes.item(idx).getAttributes();
		    Node node = attributes.getNamedItem(attrToChange);
		    if(node==null) {
		    	node = xmlDoc.createAttribute(attrToChange);
		        node.setNodeValue(newVal);
		        attributes.setNamedItem(node);
		    	
		    }
		    node.setNodeValue(newVal);
		}
	
	}
	
	public static void addUpdateFormAttrValue(Document xmlDoc, String selAttr, String selAttrValue,
			String attr, String attrVal) throws XPathExpressionException {
		//use xpath to update all CRF SDV status values to verified
		XPath xPath = XPathFactory.newInstance().newXPath();
		//xPath.setNamespaceContext(OCUtils.NamespaceContext);
		XPathExpression expressionLinkView = xPath.compile("//FormData");

		NodeList nodeList = (NodeList)expressionLinkView.evaluate(xmlDoc, XPathConstants.NODESET);
		for (int i = 0; i < nodeList.getLength(); i++)
		{
		   Node nodeForm = nodeList.item(i);
		   NamedNodeMap attributes = nodeForm.getAttributes();
		   Node node = attributes.getNamedItem(attr);
		   String status = attributes.getNamedItem(selAttr).getNodeValue();
		   if (status!=null && status.equalsIgnoreCase(selAttrValue)) {
			   if(node==null) {
			    	node = xmlDoc.createAttribute(attr);
			        attributes.setNamedItem(node);			    	
			    }
			    node.setNodeValue(attrVal);
		   }
		}

	}
	
	public static String getNodeAttrValue(Document xmlDoc, String selAttributeStr, String selAttrVal, String attrToGet) throws XPathExpressionException {
		XPath xpath = XPathFactory.newInstance().newXPath();
		StringBuilder str = new StringBuilder();
		str.append("//*[contains(@").append(selAttributeStr).append(", '").append(selAttrVal).append("')]");
		String xpathStr = str.toString();
		NodeList nodes = (NodeList)xpath.evaluate(xpathStr, xmlDoc, XPathConstants.NODESET);
		String val = null;
		if(nodes.getLength()==1) {
			Node value = nodes.item(0).getAttributes().getNamedItem(attrToGet);
		    val = value.getNodeValue();
		}
		return val;
	
	}
}
