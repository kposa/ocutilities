package com.quantumleaphealth.oc.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;


@Repository
public class OCJdbcRepository {
	
	@Autowired
	private JdbcTemplate springJdbcTemplate;
	
	private static String INSERT_QUERY = "INSERT INTO RECONSENTINSERTS (SUBJECTID, SUBJECTKEY, "
			+ "STUDY_EVENT_REPEAT_KEY, SITEOID, ICF_VER_DATE, IRB_APPROVAL_PROTO_DATE, "
			+ "IRB_APPROVAL_ICF_DATE, PROTO_VER_DATE, CREATE_DATE, STATUS) "
			+ "VALUES (?, ?, ?,?,?,?,?,?,?,?) ";
	private static String SELECT_QUERY = "SELECT * FROM reconsentinserts WHERE SUBJECTID = ? AND STUDY_EVENT_REPEAT_KEY = ? ";
	
	public void insert(ReconsentInsert reInsert) {
		springJdbcTemplate.update(INSERT_QUERY);
	}
	
	public ReconsentInsert findByIdAndEventNum(String subjectId, String eventNum) {
		return springJdbcTemplate.queryForObject(SELECT_QUERY, new BeanPropertyRowMapper<>(ReconsentInsert.class), 
				subjectId, 
				eventNum);
	}
				

}
