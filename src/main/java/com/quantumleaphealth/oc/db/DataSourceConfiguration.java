package com.quantumleaphealth.oc.db;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;



@Configuration
public class DataSourceConfiguration {
	
	/*
	 @Value("${spring.datasource.driverClassName}")
	 private String driverClassName;
	
	@Value("${spring.datasource.password}")
    private String password;
	
	@Value("${spring.datasource.username}")
    private String username;
	
	@Value("${spring.datasource.url}")
    private String url;
	*/
	
	 @Bean
	    public DataSource dataSource(){
		 	DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
	        dataSourceBuilder.driverClassName("org.postgresql.Driver");
	        dataSourceBuilder.url("jdbc:postgresql://ec2-52-5-166-49.compute-1.amazonaws.com:5432/dac1bidvktmrds?ssl=true&sslmode=require&sslcert=src/main/resources/templates/certs/bundle-certs-test/postgresql-2022-test.crt&sslkey=src/main/resources/templates/certs/bundle-certs-test/postgresql-2022-test.der&sslrootcert=src/main/resources/templates/certs/bundle-certs-test/root-2022-test.crt");
	        dataSourceBuilder.username("kposa");
	        dataSourceBuilder.password("p06158b8f705e333669fa8f3bf281df22232238631e35cb6352911636f9d24bb7");
	        return dataSourceBuilder.build();
	        
	        //jdbc:postgresql://ec2-52-5-166-49.compute-1.amazonaws.com:5432/dac1bidvktmrds?user=kposa&password=p06158b8f705e333669fa8f3bf281df22232238631e35cb6352911636f9d24bb7&ssl=true&sslmode=require&sslcert=bundle-certs-test/postgresql-2022-test.crt&sslkey=bundle-certs-test/postgresql-2022-test.der&sslrootcert=bundle-certs-test/root-2022-test.crt
	    }

	  

}
