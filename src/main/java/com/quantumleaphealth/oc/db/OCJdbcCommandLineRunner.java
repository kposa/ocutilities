package com.quantumleaphealth.oc.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

import com.quantumleaphealth.oc.utils.SdvUtilApplication;

@SpringBootApplication
public class OCJdbcCommandLineRunner implements CommandLineRunner{

	@Autowired
	private OCJdbcRepository ocJdbcRepository;
	
	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		ReconsentInsert re = ocJdbcRepository.findByIdAndEventNum("Ran64455", "1");
		System.out.println(re);
	}
	
	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(OCJdbcCommandLineRunner.class);
		//application.setAddCommandLineProperties(false);
		application.run(args);
	}

}
