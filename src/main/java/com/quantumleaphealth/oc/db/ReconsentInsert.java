package com.quantumleaphealth.oc.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "reconsentinserts")
public class ReconsentInsert {

	@Id
	@Column(name = "subjectid")
		private String subjectId;
	
	@Column(name = "subjectkey")
		private String subjectKey;
	
		private String studyEventRepeatKey;
		
		@Column(name = "siteoid")
		private String siteOid;
		
		private String icfVerDate;
		private String irbApprovalProtoDate;
		private String irbApprovalIcfDate;
		private String protoVerDate;
		private String createDate;
		private String status;

    public ReconsentInsert() {
    }

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public String getSubjectKey() {
		return subjectKey;
	}

	public void setSubjectKey(String subjectKey) {
		this.subjectKey = subjectKey;
	}

	public String getStudyEventRepeatKey() {
		return studyEventRepeatKey;
	}

	public void setStudyEventRepeatKey(String studyEventRepeatKey) {
		this.studyEventRepeatKey = studyEventRepeatKey;
	}

	public String getSiteOid() {
		return siteOid;
	}

	public void setSiteOid(String siteOid) {
		this.siteOid = siteOid;
	}

	public String getIcfVerDate() {
		return icfVerDate;
	}

	public void setIcfVerDate(String icfVerDate) {
		this.icfVerDate = icfVerDate;
	}

	public String getIrbApprovalProtoDate() {
		return irbApprovalProtoDate;
	}

	public void setIrbApprovalProtoDate(String irbApprovalProtoDate) {
		this.irbApprovalProtoDate = irbApprovalProtoDate;
	}

	public String getIrbApprovalIcfDate() {
		return irbApprovalIcfDate;
	}

	public void setIrbApprovalIcfDate(String irbApprovalIcfDate) {
		this.irbApprovalIcfDate = irbApprovalIcfDate;
	}

	public String getProtoVerDate() {
		return protoVerDate;
	}

	public void setProtoVerDate(String protoVerDate) {
		this.protoVerDate = protoVerDate;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((createDate == null) ? 0 : createDate.hashCode());
		result = prime * result + ((icfVerDate == null) ? 0 : icfVerDate.hashCode());
		result = prime * result + ((irbApprovalIcfDate == null) ? 0 : irbApprovalIcfDate.hashCode());
		result = prime * result + ((irbApprovalProtoDate == null) ? 0 : irbApprovalProtoDate.hashCode());
		result = prime * result + ((protoVerDate == null) ? 0 : protoVerDate.hashCode());
		result = prime * result + ((siteOid == null) ? 0 : siteOid.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((studyEventRepeatKey == null) ? 0 : studyEventRepeatKey.hashCode());
		result = prime * result + ((subjectId == null) ? 0 : subjectId.hashCode());
		result = prime * result + ((subjectKey == null) ? 0 : subjectKey.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReconsentInsert other = (ReconsentInsert) obj;
		if (createDate == null) {
			if (other.createDate != null)
				return false;
		} else if (!createDate.equals(other.createDate))
			return false;
		if (icfVerDate == null) {
			if (other.icfVerDate != null)
				return false;
		} else if (!icfVerDate.equals(other.icfVerDate))
			return false;
		if (irbApprovalIcfDate == null) {
			if (other.irbApprovalIcfDate != null)
				return false;
		} else if (!irbApprovalIcfDate.equals(other.irbApprovalIcfDate))
			return false;
		if (irbApprovalProtoDate == null) {
			if (other.irbApprovalProtoDate != null)
				return false;
		} else if (!irbApprovalProtoDate.equals(other.irbApprovalProtoDate))
			return false;
		if (protoVerDate == null) {
			if (other.protoVerDate != null)
				return false;
		} else if (!protoVerDate.equals(other.protoVerDate))
			return false;
		if (siteOid == null) {
			if (other.siteOid != null)
				return false;
		} else if (!siteOid.equals(other.siteOid))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (studyEventRepeatKey == null) {
			if (other.studyEventRepeatKey != null)
				return false;
		} else if (!studyEventRepeatKey.equals(other.studyEventRepeatKey))
			return false;
		if (subjectId == null) {
			if (other.subjectId != null)
				return false;
		} else if (!subjectId.equals(other.subjectId))
			return false;
		if (subjectKey == null) {
			if (other.subjectKey != null)
				return false;
		} else if (!subjectKey.equals(other.subjectKey))
			return false;
		return true;
	}

  
}
