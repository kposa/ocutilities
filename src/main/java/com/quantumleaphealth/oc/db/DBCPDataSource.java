package com.quantumleaphealth.oc.db;

import java.io.FileInputStream;
import java.sql.Connection;
import java.util.Properties;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;

public class DBCPDataSource 
{


    
    private static BasicDataSource _ds;
    private static final String _lock = "lockDBCPDataSource";
    
    private DBCPDataSource() {
        
    }

    public static Connection getConnection() throws Exception
    {
        if (_ds == null)
        {
            String dirCerts = System.getProperty("user.dir") + "/src/main/resources/templates/certs/";
            
            Properties properties = new Properties();
            properties.load(new FileInputStream(System.getProperty("user.dir") + "/src/main/resources/templates/database.properties"));
    
            String url = String.format("%s?user=%s&password=%s&ssl=true&sslmode=require&sslcert=%s&sslkey=%s&sslrootcert=%s",
            		properties.getProperty("postgresurl"),
            		 properties.getProperty("user"),
                     properties.getProperty("password"),
                     dirCerts + properties.getProperty("sslcert"),
                     dirCerts + properties.getProperty("sslkey"),
                     dirCerts + properties.getProperty("sslrootcert"));


            _ds = new BasicDataSource();
           // _ds.setDriverClassName("org.postgresql.Driver");
            _ds.setUrl(url);
            _ds.setMinIdle(5);
            _ds.setMaxIdle(10);
            _ds.setMaxActive(40);
            _ds.setMaxOpenPreparedStatements(100);    
        }

        synchronized (_lock)
        {
               // System.out.println(String.format("thread %d getConnection begin (%d/%d active)", Thread.currentThread().getId(), _ds.getNumActive(), _ds.getMaxActive()));
                Connection connection = _ds.getConnection();
              //  System.out.println(String.format("thread %d getConnection end (%d/%d active)", Thread.currentThread().getId(), _ds.getNumActive(), _ds.getMaxActive()));
                return connection;
        }
    }


}
