package com.quantumleaphealth.os.utils;


import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.quantumleaphealth.os.model.Specimen;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.util.Properties;

@Component
//@PropertySource({ "classpath:application.properties" })
public class OpenSpecimenUtil implements EnvironmentAware 
{
	
	@Autowired
	public static  Environment env;
	
	
    @Override
    public void setEnvironment(Environment arg0) {
        if(env==null){
        	env=arg0;
        }

    }
    
    private Properties appproperties = null;
	
	public OpenSpecimenUtil() {
        appproperties = new Properties();
        try {
        	appproperties.load(new FileInputStream(System.getProperty("user.dir") + "/src/main/resources/templates/application.properties"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

    public Properties getAppProperties() {
    	return appproperties;
    }
    
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }
    
    public static  String getConfigProp(String key) {
        return env.getProperty(key);
    }
	
	

	
	public static JsonNode getJson(String jsonString) throws Exception
	{
		JsonNode rootNode = null;
		if (jsonString != null)
		{
			ObjectMapper mapper = new ObjectMapper();
			rootNode = mapper.readTree(jsonString);
		}
		return rootNode;
	}

	public static String httpGet(String urlString, String accessToken)
	{
		HttpResponse<String> response  = null;
		try {
			HttpClient client = HttpClient.newBuilder()
					.version(Version.HTTP_1_1)
					.connectTimeout(Duration.ofSeconds(20))
					.build();
			
			HttpRequest request = HttpRequest.newBuilder()
					  .uri(new URI(urlString))
					  .header("X-OS-API-TOKEN", accessToken)
					  .GET()
					  .build();
			
			response = client.send(request, HttpResponse.BodyHandlers.ofString());
			
			System.out.println(response.body());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return accessToken;
	}
	public static String httpPost(String urlString, String accessToken, String jsonRequest)
	{
		HttpResponse<String> response  = null;
		try {
			
			HttpClient client = HttpClient.newBuilder()
					.version(Version.HTTP_1_1)
					.connectTimeout(Duration.ofSeconds(20))
					.build();
			
			HttpRequest request = HttpRequest.newBuilder()
			  .uri(new URI(urlString))
			  .headers("Content-Type", "application/json;charset=UTF-8")
			  .header("X-OS-API-TOKEN", accessToken)
			  .POST(HttpRequest.BodyPublishers.ofString((jsonRequest)))
			  .build();
			
			response = client.send(request, HttpResponse.BodyHandlers.ofString());
			
			System.out.println(response.body());
			
		} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		return "";
	}

	public static String getToken(String urlString, String loginName, String password)
	{
		HttpResponse<String> response  = null;
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode rootNode = mapper.createObjectNode();
		rootNode.put("loginName", loginName);
		rootNode.put("password", password);
		rootNode.put("domainName", "openspecimen");
		
		String token = null;
		
		try {
			
			String jsonString = mapper.writerWithDefaultPrettyPrinter()
					.writeValueAsString(rootNode);

			System.out.println(jsonString);
			
			HttpClient client = HttpClient.newBuilder()
					.version(Version.HTTP_1_1)
					.connectTimeout(Duration.ofSeconds(20))
					.build();
			
			HttpRequest request = HttpRequest.newBuilder()
			  .uri(new URI(urlString))
			  .headers("Content-Type", "application/json;charset=UTF-8")
			  .POST(HttpRequest.BodyPublishers.ofString((jsonString)))
			  .build();
			
			response = client.send(request, HttpResponse.BodyHandlers.ofString());
			JsonNode node = getJson(response.body());
			token = node.get("token").asText();
			System.out.println(token);
			
		} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		return token;
	}
	
	public static Object jsonToObject(String jsonString, Class<?> cls) {
       
		Object specimen = null;
	 	
		try {
	        ObjectMapper mapper = new ObjectMapper();
	        specimen =  mapper.readValue(jsonString, cls);
	        System.out.println(specimen);

		} catch (Exception  e) {

			e.printStackTrace();
		}
		return specimen;

	}

	
}
