package com.quantumleaphealth.os.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

import com.quantumleaphealth.os.model.Specimen;

@SpringBootApplication
public class OSMain {
	
	public static void main(String[] args) {
		SpringApplication.run(OSMain.class, args);
		
	}
	
	@Autowired
	private Environment env;
	
	
	
	
	
	public static void testOpenSpecimen() {
		String token = OpenSpecimenUtil.getToken("https://app.openspecimen.org/qlh-prod/rest/ng/sessions", 
				"Krishna.Posa", "p0saQlos");
		System.out.println(token);
		
	}
	
	public static void jsontoSpecimen() {
		String json = "{\"id\":\"1\", \"lineage\":\"testlineage\"}";
		Specimen sp = (Specimen)OpenSpecimenUtil.jsonToObject(json, Specimen.class);
		System.out.println(sp);
		
	}
	
	
	
	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
			jsontoSpecimen();

		};
	}

}
