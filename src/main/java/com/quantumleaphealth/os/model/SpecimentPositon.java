package com.quantumleaphealth.os.model;

public class SpecimentPositon {
	String containerName;
	String positionDimensionTwoString;
	String positionDimensionOneString;
	public String getContainerName() {
		return containerName;
	}
	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}
	public String getPositionDimensionTwoString() {
		return positionDimensionTwoString;
	}
	public void setPositionDimensionTwoString(String positionDimensionTwoString) {
		this.positionDimensionTwoString = positionDimensionTwoString;
	}
	public String getPositionDimensionOneString() {
		return positionDimensionOneString;
	}
	public void setPositionDimensionOneString(String positionDimensionOneString) {
		this.positionDimensionOneString = positionDimensionOneString;
	}
}
