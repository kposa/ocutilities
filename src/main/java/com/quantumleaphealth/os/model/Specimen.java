package com.quantumleaphealth.os.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Specimen {
	String id;
	String lineage;
	String barcode;
	String initialQty;
	String concentration;
	String collectionStatus;
	String activityStatus;
	String comments;
	String freezeThawCycles;
	String creator;
	String updateTime;
	String updater;
	String creationTime;
	String label;
	
	@JsonProperty("class")
	String classStr;
	
	String type;
	String tissueSite;
	String pathologicalStatus;
	String createdOn;
	String availableQty;
	List<SpecimentPositon> specimentPositionList;
	List<Extension> extensionList;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLineage() {
		return lineage;
	}
	public void setLineage(String lineage) {
		this.lineage = lineage;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public String getInitialQty() {
		return initialQty;
	}
	public void setInitialQty(String initialQty) {
		this.initialQty = initialQty;
	}
	public String getConcentration() {
		return concentration;
	}
	public void setConcentration(String concentration) {
		this.concentration = concentration;
	}
	public String getCollectionStatus() {
		return collectionStatus;
	}
	public void setCollectionStatus(String collectionStatus) {
		this.collectionStatus = collectionStatus;
	}
	public String getActivityStatus() {
		return activityStatus;
	}
	public void setActivityStatus(String activityStatus) {
		this.activityStatus = activityStatus;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getFreezeThawCycles() {
		return freezeThawCycles;
	}
	public void setFreezeThawCycles(String freezeThawCycles) {
		this.freezeThawCycles = freezeThawCycles;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getUpdater() {
		return updater;
	}
	public void setUpdater(String updater) {
		this.updater = updater;
	}
	public String getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getClassStr() {
		return classStr;
	}
	public void setClassStr(String classStr) {
		this.classStr = classStr;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTissueSite() {
		return tissueSite;
	}
	public void setTissueSite(String tissueSite) {
		this.tissueSite = tissueSite;
	}
	public String getPathologicalStatus() {
		return pathologicalStatus;
	}
	public void setPathologicalStatus(String pathologicalStatus) {
		this.pathologicalStatus = pathologicalStatus;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public String getAvailableQty() {
		return availableQty;
	}
	public void setAvailableQty(String availableQty) {
		this.availableQty = availableQty;
	}
	public List<SpecimentPositon> getSpecimentPositionList() {
		return specimentPositionList;
	}
	public void setSpecimentPositionList(List<SpecimentPositon> specimentPositionList) {
		this.specimentPositionList = specimentPositionList;
	}
	public List<Extension> getExtensionList() {
		return extensionList;
	}
	public void setExtensionList(List<Extension> extensionList) {
		this.extensionList = extensionList;
	}
	@Override
	public String toString() {
		/*return "Specimen [id=" + id + ", lineage=" + lineage + ", barcode=" + barcode + ", initialQty=" + initialQty
				+ ", concentration=" + concentration + ", collectionStatus=" + collectionStatus + ", activityStatus="
				+ activityStatus + ", comments=" + comments + ", freezeThawCycles=" + freezeThawCycles + ", creator="
				+ creator + ", updateTime=" + updateTime + ", updater=" + updater + ", creationTime=" + creationTime
				+ ", label=" + label + ", classStr=" + classStr + ", type=" + type + ", tissueSite=" + tissueSite
				+ ", pathologicalStatus=" + pathologicalStatus + ", createdOn=" + createdOn + ", availableQty="
				+ availableQty + ", specimentPositionList=" + specimentPositionList + ", extensionList=" + extensionList
				+ "]";
				
				*/
		String str = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			str =  mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		return str;
	}
	

}
