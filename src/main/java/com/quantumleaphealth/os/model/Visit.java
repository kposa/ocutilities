package com.quantumleaphealth.os.model;

import java.util.List;

public class Visit {
	
	String visitName;
	List<Specimen> specimenList;
	public String getVisitName() {
		return visitName;
	}
	public void setVisitName(String visitName) {
		this.visitName = visitName;
	}
	public List<Specimen> getSpecimenList() {
		return specimenList;
	}
	public void setSpecimenList(List<Specimen> specimenList) {
		this.specimenList = specimenList;
	}
}
